# STL summary

## Introduction

## Contents

## Prerequisites

# Base notions

## Complexity

## Copy

# Containers

## Sequence

## Associative

## Other

## Algorithms

# in/out puts

## File

## String

## Buffers

## Standard in/out

# Memory management & multithreading

## shared_ptr

## unique_ptr

## make_...

## pointer casting

## atomic

# Type abstraction

## Any

## Variant

## Tuple