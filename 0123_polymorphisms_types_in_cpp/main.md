# Polymorphisms types in C++

## Introduction

## Contents

## Prerequisites

# Static polymorphism

## Templates

## Preformances

## Genericity

## Static limits

# Virtual polymorphism

## Virtual methods & inheritance

## Extensibility

## Performances drawbacks

# Composition polymorphism

## Composition

## Extensibility

## Performances

## Workload increase

## Performance drawbacks