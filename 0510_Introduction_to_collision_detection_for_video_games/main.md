# Introduction to collision detection for video games

## Introduction

## Contents

## Prerequisites

# Introduction

## Collision detection in a game

## Tools used in this course

## Vectors and quaternions

# Bounding boxes

## Point

## AABB

## OOB

## Convex hull/polygon

## Any mesh/polygon

## Terrain

# 2D simple collisions

## AABB - point

## AABB - AABB

## Checking with Geogebra

# Introduction to SAT

## Principle

## Projection

## 1D overlap

# 2D SAT

## Pre computing normals

## Solid transformations

## Example

# 3D SAT

## 3D solid representation

## Trivial 2D to 3D change

# In video games

## Bounding AABB

## AABB trees

## Octrees

## Per-collision optimizations

## High speed objects

# Opening

## Other spheres

## Predictive approach