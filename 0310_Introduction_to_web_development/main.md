# Introduction to web development

## Introduction

## Contents

## Prerequisites

# Getting Started

## Web technologies

## Typical project structure

## Git

## Information sources

# Page content

## Document structure

## HTML syntax

## Main HTML tags

# Page style

## CSS Syntax and rules

## Layout properties

## Element properties

# Ergonomy

## Responsive Web Design

## General interfaces ergonomy

## Web-specific ergonomy

# Dynamic page

## Javascript syntax

## Console

## Document manipulation

## Event callbacks

## Frameworks

# Page-server interactions

## HTTP

## Forms

## AJAX

## Security concerns

# Server-side programming

## NodeJS & npm

## Getting started

## Answering server request

## Files, libraries and databases