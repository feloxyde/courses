# Useful C++ tools and tricks

## Introduction

## Contents

## Prerequisites

# Tools

## Cmake & co

## GDB

## GCOV

## Valgrind

## clang-format

## editors & IDE

# Tricks

## C macros

## constexpr

## inheritance

## templates