from unittest import TestCase
from unittest import main
from formatting.default import Default


class MockProcessor:
    def __init__(self):
        self.name = "MockProcessor"


class TestDefault(TestCase):

    def setUp(self):
        self.vlist = [1, -2, 3, 7, 4]
        self.res = {"rkey1": "rval1", "rkey2": "rval2"}
        self.proc = MockProcessor()
        self.df = Default()

    def tearDown(self):
        del self.vlist
        del self.res
        del self.proc
        del self.df

    def test_format(self):
        r = self.df.format(self.res, self.vlist, self.proc)S
        expected = """----
Results for MockProcessor
rkey1 : rval1
rkey2 : rval2
----"""
        self.assertEqual(r, expected)


if __name__ == '__main__':
    unittest.main()
