class Default:
    def __init__(self):
        pass

    def format(self, res, vlist, processor):
        fullstr = "----\n"
        fullstr += "Results for " + processor.name + "\n"
        for r in res:
            fullstr += str(r) + " : " + str(res[r]) + "\n"
        fullstr += "----"
        return fullstr
