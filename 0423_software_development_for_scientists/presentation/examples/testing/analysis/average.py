class Average:
    def __init__(self):
        pass

    def process(self, vlist):
        if (len(vlist) == 0):
            return {"avg": "unkwnown"}
        else:
            s = 0
            for val in vlist:
                s += val
            return {"avg": s/len(vlist)}
