#!/usr/bin/python3

import sys
from core.app import App
from commands.controls import default_controls
from commands.processor import Processor
from ui.stdinout import StdInOut

from analysis.average import Average
from formatting.default import Default as DefaultFormat

app = App("process", StdInOut())

app.addCmds(default_controls())

app.addCmd(Processor("avg", Average(), DefaultFormat(),
                     "computes average of registered values"))

app.start(sys.argv)
