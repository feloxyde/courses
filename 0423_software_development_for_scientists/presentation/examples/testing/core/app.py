import sys


class App:
    def __init__(self, name, ui):
        self.name = name
        self.cmd = {}
        self.vlist = []
        self.ui = ui

    def addCmd(self, command):
        self.cmd[command.name] = command

    def addCmds(self, commands):
        for command in commands:
            self.addCmd(command)

    def addVal(self, val):
        self.vlist.append(val)

    def isCmd(self, cmd_name):
        return cmd_name in self.cmd

    def runCmd(self, cmd_name):
        return self.cmd[cmd_name].invoke(self)

    def direct(self, cmd_name, val_args):
        if not self.isCmd(cmd_name):
            self.ui.error(
                "Cmd should be a valid command name. Use [" + self.name + " help] command to display help.")
            sys.exit(1)
        for valarg in val_args:
            try:
                self.vlist.append(float(valarg))
            except ValueError:
                self.ui.error(
                    "All args after a cmd should be a float formatted number. Use [" + self.name + " help] command to display help.")
                sys.exit()
        self.runCmd(cmd_name)

    def parse_number_list(self, params):
        ignored = []
        for param in params:
            try:
                self.vlist.append(float(param))
            except ValueError:
                ignored.append(param)
        if len(ignored) > 0:
            self.ui.output("ignored invalid float values : " + str(ignored))

    def interactive(self):
        while True:
            inpt = self.ui.input(">>")
            params = list(filter(lambda a: a != "", inpt.split(" ")))
            if len(params) > 1:
                self.parse_number_list(params)
            elif len(params) == 1:
                if self.isCmd(params[0]):
                    self.runCmd(params[0])
                else:
                    try:
                        self.vlist.append(float(params[0]))
                    except ValueError:
                        self.ui.output(
                            "invalid input, should be command or float")

    def start(self, args):
        if len(args) > 1:
            self.direct(args[1], args[2:])
        else:
            self.interactive()
