from .command import Cmd


class Processor(Cmd):
    def __init__(self, name, analyzer, formatter, description):
        Cmd.__init__(self, name)
        self.ana = analyzer
        self.fmt = formatter
        self.desc = description

    def invoke(self, app):
        res = self.ana.process(app.vlist)
        fmtstr = self.fmt.format(res, app.vlist, self)
        app.ui.output(fmtstr)

    def description(self):
        return self.desc
