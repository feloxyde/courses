from .command import Cmd
import sys


class Show(Cmd):
    def __init__(self, name):
        Cmd.__init__(self, name)

    def invoke(self, app):
        strv = ""
        for val in app.vlist:
            strv += str(val) + " "
        app.ui.output(strv)

    def description(self):
        return "prints all registered values"


class Exit(Cmd):
    def __init__(self, name):
        Cmd.__init__(self, name)

    def invoke(self, app):
        sys.exit(0)

    def description(self):
        return "ends session"


class Help(Cmd):
    def __init__(self, name):
        Cmd.__init__(self, name)

    def invoke(self, app):
        app.ui.output(app.name + " help")
        app.ui.output("Call in direct mode with command")
        app.ui.output("\t" + app.name + " cmd [float]*")
        app.ui.output("Call in interactive mode with command")
        app.ui.output("\t" + app.name)
        app.ui.output("commands are :")
        for cmdn in app.cmd:
            app.ui.output("\t" + cmdn + " : " + app.cmd[cmdn].description())

    def description(self):
        return "prints help"


class ListCmd(Cmd):
    def __init__(self, name):
        Cmd.__init__(self, name)

    def invoke(self, app):
        for cmdn in app.cmd:
            app.ui.output(cmdn + " : " + app.cmd[cmdn].description())

    def description(self):
        return "show command list"


class Clear(Cmd):
    def __init__(self, name):
        Cmd.__init__(self, name)

    def invoke(self, app):
        app.vlist.clear()

    def description(self):
        return "remove all registered values"


def default_controls():
    ctrls = [Show("show"), Exit("exit"), Help(
        "help"), ListCmd("lcmd"), Clear("clear")]
    return ctrls
