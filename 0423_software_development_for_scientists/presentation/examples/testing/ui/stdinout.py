class StdInOut:
    def __init__(self):
        pass

    def input(self, hint):
        return input(hint)

    def output(self, out):
        print(out)

    def error(self, err):
        print("Error: " + err)
