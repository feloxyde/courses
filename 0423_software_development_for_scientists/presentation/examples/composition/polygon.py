from shape import Rectangle, Triangle
from transform import RotationOnly, TranslationOnly, AllTransform
from vec2 import Vec2


class Polygon:
    def __init__(self, name_arg, shape_arg, transform_arg):
        self.name = name_arg
        self.shape = shape_arg
        self.transform = transform_arg

    def display(self):
        vertices = self.shape.vertices()
        for vert in vertices:
            vert.rotate(self.transform.rot)
            vert += self.transform.pos
        print("Polygon ", self.name,
              "p=(", self.transform.pos.x, ";", self.transform.pos.y, ") r=", self.transform.rot, " :")
        for vert in vertices:
            print("\t(", vert.x, ";", vert.y, ")")


# MAIN CODE STARTS HERE
axis_aligned_rec = Polygon("Axis Aligned Rectangle",
                           Rectangle(5, 4),
                           TranslationOnly(Vec2(0, 0), 0))

any_rec = Polygon("Any Rec",
                  Rectangle(7, 3),
                  AllTransform(Vec2(0, 0), 0))

triangle = Polygon("Triangle",
                   Triangle(Vec2(-2, -1), Vec2(0, 3), Vec2(2.5, -2)),
                   RotationOnly(Vec2(0, 0), 0))

polygons = [axis_aligned_rec, any_rec, triangle]

print("### BASE POLYGONS ###\n\n")
for polygon in polygons:
    polygon.display()


for polygon in polygons:
    polygon.transform.rotate(90)
    polygon.transform.translate(Vec2(10, 5))


print("\n\n### TRANSFORMED POLYGONS ###\n\n")
for polygon in polygons:
    polygon.display()
