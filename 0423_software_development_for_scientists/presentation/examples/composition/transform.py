from vec2 import Vec2


class Transform:
    def __init__(self, position, rotation):
        self.pos = position
        self.rot = rotation

    def rotate(self, angle):
        pass

    def translate(self, vec):
        pass


class Static(Transform):
    def __init__(self, position, rotation):
        Transform.__init__(self, position, rotation)


class TranslationOnly(Transform):
    def __init__(self, position, rotation):
        Transform.__init__(self, position, rotation)

    def translate(self, vec):
        self.pos += vec


class RotationOnly(Transform):
    def __init__(self, position, rotation):
        Transform.__init__(self, position, rotation)

    def rotate(self, angle):
        self.rot += angle


class AllTransform(Transform):
    def __init__(self, position, rotation):
        Transform.__init__(self, position, rotation)

    def translate(self, vec):
        self.pos += vec

    def rotate(self, angle):
        self.rot += angle
