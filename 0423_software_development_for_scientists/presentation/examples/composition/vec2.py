from math import cos, sin


class Vec2:
    def __init__(self, x_coord, y_coord):
        self.x = x_coord
        self.y = y_coord

    def __add__(self, other):
        return Vec2(self.x + other.x, self.y + other.y)

    def __iadd__(self, other):
        self.x += other.x
        self.y += other.y
        return self

    def rotate(self, angle):
        temp_x = self.x * cos(angle) - self.y * sin(angle)
        temp_y = self.y * sin(angle) + self.y * cos(angle)
        self.x = temp_x
        self.y = temp_y
