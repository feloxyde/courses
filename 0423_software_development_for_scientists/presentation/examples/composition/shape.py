from vec2 import Vec2
from copy import deepcopy as dcopy


class Shape:
    def vertices(self):
        pass


class Triangle(Shape):
    def __init__(self, point_a, point_b, point_c):
        self.a = point_a
        self.b = point_b
        self.c = point_c

    def vertices(self):
        return dcopy([self.a, self.b, self.c])


class Rectangle(Shape):
    def __init__(self, width, height):
        self.w = width
        self.h = height

    def vertices(self):
        a = Vec2(-self.w / 2, -self.h / 2)
        b = Vec2(-self.w / 2, self.h / 2)
        c = Vec2(self.w / 2, self.h / 2)
        d = Vec2(self.w / 2, -self.h / 2)
        return [a, b, c, d]
