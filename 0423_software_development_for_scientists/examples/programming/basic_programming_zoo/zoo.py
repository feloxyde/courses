# defining animals
def display_cat():
    print("This animal is a cat :")
    print("*meows*")
    print("*runs and jumps with agility*")
    print("*licks paws*")


def display_whale():
    print("This animal is a whale :")
    print("*inaudible ultrasonic noises*")
    print("*swims with ample and powerful movements*")
    print("*shoots water in the air*")


def display_tiger():
    print("This animal is a tiger :")
    print("*roars*")
    print("*walks with grace and confidence*")
    print("*licks paws*")


def display_alpine_chough():
    print("This animal is an alpine chough :")
    print("*craws loudly*")
    print("*shows his mastery of air flow along the cliff*")
    print("*shakes feathers*")


# listing animals
def list_animals():
    # simply listing their names
    animals = ["cat", "whale", "tiger", "alpinechough"]
    return animals


# route stuff
def ask_route(animals):  # note that now we pass the animal list as a parameter

    valid = False  # set to false to execute while loop once
    route = []

    while not valid:
        valid = True
        print("please list animals you want to see in order, separated by a comma")
        answer = input("your answer : ")
        answer = answer.replace(" ", "")
        route = answer.split(",")
        for animal in route:
            valid = valid and (animal in animals)
    # at this point, we know route is valid
    return route  # return route


# touring stuff
def take_tour(route):
    for animal in route:
        if(animal == "cat"):
            display_cat()
        elif(animal == "whale"):
            display_whale()
        elif(animal == "tiger"):
            display_tiger()
        elif(animal == "alpinechough"):
            display_alpine_chough()
        else:
            print("animal ", animal, " not found")


# main code
animals = list_animals()
route = ask_route(animals)
take_tour(route)
