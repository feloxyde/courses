class Meowing:
    def __init__(self):
        pass

    def name(self):
        return "meowing"

    def make_noise(self):
        print("*Meows*")


class Roaring:
    def __init__(self):
        pass

    def name(self):
        return "roaring"

    def make_noise(self):
        print("*Roars*")


class UltrasonicNoises:
    def __init__(self):
        pass

    def name(self):
        return "ultrasonic_noises"

    def make_noise(self):
        print("*ultrasonic noises*")


class Crawing:
    def __init__(self):
        pass

    def name(self):
        return "crawing"

    def make_noise(self):
        print("*Craws*")


def list_noises():
    noi = {}
    noi["crawing"] = Crawing()
    noi["ultrasonic_noises"] = UltrasonicNoises()
    noi["roaring"] = Roaring()
    noi["meowing"] = Meowing()
    return noi
