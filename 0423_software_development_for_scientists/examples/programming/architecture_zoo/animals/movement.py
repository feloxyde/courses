class AgileRun:
    def __init__(self):
        pass

    def name(self):
        return "agile_run"

    def move(self):
        print("*Runs and jumps with agility*")


class PowerfulRun:
    def __init__(self):
        pass

    def name(self):
        return "powerful_run"

    def move(self):
        print("*Runs with powerful movements*")


class PowerfulSwimming:
    def __init__(self):
        pass

    def name(self):
        return "powerful_swimming"

    def move(self):
        print("*Swims with powerful movements*")


class AgileFlight:
    def __init__(self):
        pass

    def name(self):
        return "agile_flight"

    def move(self):
        print("*Flies, wisely using air currents*")


def list_movements():
    mvt = {}
    mvt["agile_flight"] = AgileFlight()
    mvt["powerful_swimming"] = PowerfulSwimming()
    mvt["powerful_run"] = PowerfulRun()
    mvt["agile_run"] = AgileFlight()
    return mvt
