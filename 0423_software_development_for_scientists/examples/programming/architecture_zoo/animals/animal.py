class Animal:
    def __init__(self, name, noise, movement, cuteness):
        self.name = name
        self.cuteness = cuteness
        self.noise = noise
        self.movement = movement

    def introduce(self):
        print("This animal is a ", self.name)

    def display(self):
        self.introduce()
        self.noise.make_noise()
        self.movement.move()
        self.cuteness.be_cute()
