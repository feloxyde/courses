from route import ask_route
from touring import take_tour
from chimera import create_chimeras
from animals.list import list_animals


def list_animals_names(animals):
    names = []
    for key in animals:  # iteration over a dictionnary is done using keys
        names.append(key)  # simply listing keys, easy !
    return names


animals = list_animals()
create_chimeras(animals)
animals_names = list_animals_names(animals)
route = ask_route(animals)
take_tour(route, animals)
