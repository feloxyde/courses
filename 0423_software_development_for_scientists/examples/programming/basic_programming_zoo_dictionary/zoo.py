# in this version, animals are represented using a dictionnary where
# actions they perform are their keys

# animal stuff
def create_animal(animals, name, noise, movement, cute_action):
    # adding a value to the animals dict and returning it
    dict = animals
    animal = {"name": name, "noise": noise,
              "move": movement, "cute": cute_action}
    dict[name] = animal
    return dict


def list_animal_names(animals):
    list = []
    for key in animals:
        list.append(key)
    return list


def display_animal(animal):
    print("This animal is a", animal["name"])
    print("*", animal["noise"], "*")
    print("*", animal["move"], "*")
    print("*", animal["cute"], "*")


# route stuff
def ask_route(animaldict):  # note that now we pass the animal list as a parameter

    animals = list_animal_names(animaldict)
    sep = ", "  # for displaying list of animals
    animal_string = sep.join(animals)

    valid = False  # set to false to execute while loop once
    route = []

    while not valid:
        valid = True
        print("please list animals you want to see in order, separated by a comma")
        print("animals are : ", animal_string)
        answer = input("your answer : ")
        answer = answer.replace(" ", "")
        route = answer.split(",")
        for animal in route:
            valid = valid and (animal in animals)

    # at this point, we know route is valid
    return route  # return route

# touring stuff


def take_tour(route, animals):
    for animal_name in route:
        display_animal(animals[animal_name])


# main code
animals = {}
# creating animals, changing descriptions to keep lines short
animals = create_animal(animals, "cat", "meows", "agile run", "licks paws")
animals = create_animal(animals, "whale", "whale noise",
                        "powerful swimming", "water geyser")
animals = create_animal(animals, "tiger", "roar", "powerful run", "licks paws")
animals = create_animal(animals, "alpinechough", "craws",
                        "agile fly", "shakes feathers")
route = ask_route(animals)
take_tour(route, animals)
