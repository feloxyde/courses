# animals
class Animal():
    def __init__(self):
        pass  # nothing to do

    def display(self):
        self.introduce()
        self.make_noise()
        self.move()
        self.be_cute()

    def introduce(self):
        print("This animal is a ", self.name())

    def name(self):
        return "unknown specie"

    def be_cute(self):
        print("*does nothing*")

    def move(self):
        print("*does nothing*")

    def make_noise(self):
        print("*stays silent*")

# feline class : feline licks their paws and that's cute


class Feline(Animal):
    def __init__(self):
        pass  # nothing to

    def be_cute(self):
        print("*licks paws*")

# specific feline


class Cat(Feline):
    def __init__(self):
        pass  # nothing to do

    def name(self):  # redefining name method to change name of the class
        return "cat"

    def make_noise(self):
        print("*meows*")

    def move(self):
        print("*runs and jumps with agility*")


class Whale(Animal):
    def __init__(self):
        pass  # nothing to do

    def name(self):  # redefining name method to change name of the class
        return "whale"

    def make_noise(self):
        print("*ultrasonic noises*")

    def move(self):
        print("*swimms with powerful noises*")

    def be_cute(self):
        print("*expels water from its back*")


class Tiger(Feline):
    def __init__(self):
        pass  # nothing to do

    def name(self):
        return "tiger"

    def make_noise(self):
        print("*roars*")

    def move(self):
        print("*runs with powerful jumps*")


class AlpineChough(Animal):
    def __init__(self):
        pass  # nothing to do

    def name(self):
        return "alpinechough"

    def make_noise(self):
        print("*craws*")

    def move(self):
        print("*flies with agility following airflow*")

    def be_cute(self):
        print("*shakes feathers*")


def list_animals():
    animals = {}  # dictionnary
    # simply creating the class corresponding to the animal we want
    animals["cat"] = Cat()
    animals["whale"] = Whale()
    animals["tiger"] = Tiger()
    animals["alpinechough"] = AlpineChough()
    return animals

# extracting animals names


def list_animals_names(animals):
    names = []
    for key in animals:  # iteration over a dictionnary is done using keys
        names.append(key)  # simply listing keys, easy !
    return names

# route stuff


def ask_route(animaldict):  # note that now we pass the animal list as a parameter

    sep = ", "  # for displaying list of animals
    animal_string = sep.join(animals)

    valid = False  # set to false to execute while loop once
    route = []

    while not valid:
        valid = True
        print("please list animals you want to see in order, separated by a comma")
        print("animals are : ", animal_string)
        answer = input("your answer : ")
        answer = answer.replace(" ", "")
        route = answer.split(",")
        for animal in route:
            valid = valid and (animal in animals)

    # at this point, we know route is valid
    return route  # return route


# touring
def take_tour(route, animals):
    for animal in route:
        # accessing the animal by its name and displaying it
        animals[animal].display()


# main code
animals = list_animals()
animals_names = list_animals_names(animals)
route = ask_route(animals_names)
take_tour(route, animals)
