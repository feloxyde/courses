# file strop.py
from format import concat
from format import between
from sys import argv
from sys import exit

# main code
# if not enough args, we return error and explain why
if len(argv) < 3:
    print(
        "too few arguments, invocation shall be done with: strop format str1 [... strn] ")
    exit(1)  # here retuning failure

# othewise, separate format from other str args
fmt = argv[1]
strs = argv[2:]

if fmt == "concat":
    print(concat(strs))
elif fmt == "between":
    print(between(strs))
else:
    print("invalid format, should be either concat or between")
    exit(1)

exit(0)
