# file test_format.py
# let's test concat and between methods
from unittest import TestCase
from unittest import main
from format import between
from format import concat

# we define test case as a concat


class TestConcat(TestCase):
    def setUp(self):  # this method is used to setup test case
        print("setting up concat")  # no setup needed, this is only for show

    def tearDown(self):  # this method is used to teardown test case
        print("tearing down concat")

    # starting a method name by "test" tells the framework that it
    # is a test case execution
    def test_concat_single(self):
        # assertEqual allows to verify that two values are equal, and report otherwise
        self.assertEqual(concat(["aaa"]), "aaa")

    # the third parameter of assertion allows to display a custom message in case of failure
    def test_concat_multiple(self):
        self.assertEqual(concat(["aaa", "bbb", "ccc"]),
                         "aaabbbccc", "custom failure message !")


class TestBetween(TestCase):
    def setUp(self):
        print("setting up between")  # no setup needed, this is only for show

    def tearDown(self):
        print("tearing down between")

    def test_between_two(self):
        self.assertEqual(between(["a", "b"]), "b")

    def test_between_more(self):
        self.assertEqual(between(["a", "b", "c", "d"]), "bacad")


class TestBetween(TestCase):
    # test failing just for show
    def setUp(self):
        # no setup needed, this is only for show
        print("setting up between fail")

    def tearDown(self):
        print("tearing down between fail")

    def test_between_two_fail(self):
        self.assertEqual(between(["a", "b"]), "bb")

    def test_between_more_fail(self):
        self.assertEqual(between(["a", "b", "c", "d"]), "bacadd")


# if the file isn't included from another
# (eg file is run with python command directly)
# then run tests
if __name__ == '__main__':
    unittest.main()
