#!/bin/bash
#file test_no_argument.sh
#we start by creating a file with expected result, this is our context setup :
printf "too few arguments, invocation shall be done with: strop format str1 [... strn] \n" > res.txt
#then we run the program without args and dump in a file, for a wide expected result, 
#or with a lot of tests like that, one can use external files instead
python strop.py > exp.txt
#then run diff on both files, diff returns 0 if file are the same, another value otherwise. 
diff exp.txt res.txt 
#we store return value of the diff command, represented by variable $?
res=$?
#we finally delete res and exp, this our context cleaning
rm res.txt
rm exp.txt 
#and we return result for diff 
exit $res
