#!/usr/bin/bash
#$1 is name of script/expected couple
src="./${1}.exp" #name of the script (software call)
res="${1}.res" #name of the result 
exp="${1}.expr" #name of expected

#creating commands
run_cmd="${src} > ${res}" #dump script into result
check_cmd="diff ${res} ${exp}" #run diff on result vs expected
clean_cmd="rm ${res}" #remove res

#actually calling commands
eval $run_cmd 
eval $check_cmd
r=$? #storing result for later, remember, 0 if diff found no difference
eval $clean_cmd 
exit $r #returning result

