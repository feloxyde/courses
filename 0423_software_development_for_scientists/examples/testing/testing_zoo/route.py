def ask_route(animals, ui):  # note that now we pass the animal list as a parameter

    sep = ", "  # for displaying list of animals
    animal_string = sep.join(animals)

    valid = False  # set to false to execute while loop once
    route = []

    while not valid:
        valid = True
        ui.print("please list animals you want to see in order, separated by a comma")
        ui.print("animals are : " + animal_string)
        answer = ui.input("your answer : ")
        answer = answer.replace(" ", "")
        route = answer.split(",")
        for animal in route:
            valid = valid and (animal in animals)

    # at this point, we know route is valid
    return route  # return route
