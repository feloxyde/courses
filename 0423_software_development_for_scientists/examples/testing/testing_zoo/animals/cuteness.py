class PawsLicking:
    def __init__(self):
        pass

    def name(self):
        return "paws_licking"

    def be_cute(self, ui):
        ui.print("*licks paws*")


class Geysering:
    def __init__(self):
        pass

    def name(self):
        return "geysering"

    def be_cute(self, ui):
        ui.print("*shoots water in the air")


class FeatherShaking:
    def __init__(self):
        pass

    def name(self):
        return "feather_shaking"

    def be_cute(self, ui):
        ui.print("*shakes feathers*")


def list_cutenesses():
    ctns = {}
    ctns["paws_licking"] = PawsLicking()
    ctns["geysering"] = Geysering()
    ctns["feather_shaking"] = FeatherShaking()
    return ctns
