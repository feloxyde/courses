class Animal:
    def __init__(self, name, noise, movement, cuteness):
        self.name = name
        self.cuteness = cuteness
        self.noise = noise
        self.movement = movement

    def introduce(self, ui):
        ui.print("This animal is a " + self.name)

    def display(self, ui):
        self.introduce(ui)
        self.noise.make_noise(ui)
        self.movement.move(ui)
        self.cuteness.be_cute(ui)
