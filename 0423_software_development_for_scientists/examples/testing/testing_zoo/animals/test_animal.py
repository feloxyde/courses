from unittest import TestCase
from unittest import main
from animals.animal import Animal
from animals.cuteness import PawsLicking
from animals.movement import AgileRun
from animals.noise import Meowing


class MockUI:
    # Mock class that will register output
    def __init__(self):
        self.out = []

    def print(self, message):
        self.out.append(message)


class MockCuteness:
    def be_cute(self, ui):
        ui.print("cuteness")


class MockMovement:
    def move(self, ui):
        ui.print("movement")


class MockNoise:
    def make_noise(self, ui):
        ui.print("noise")

# base class for all part tests that will setup and teardown UI


class TestAnimal(TestCase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.ui = None
        self.animal = None

    def setUp(self):
        self.ui = MockUI()
        self.animal = Animal("anim", MockNoise(),
                             MockMovement(), MockCuteness())

    def tearDown(self):
        del self.ui
        del self.animal

    def test_introduce(self):
        self.animal.introduce(self.ui)
        self.assertListEqual(["This animal is a anim"], self.ui.out)

    def test_display(self):
        self.animal.display(self.ui)
        self.assertListEqual(
            ["This animal is a anim", "noise", "movement", "cuteness"], self.ui.out)


class TestAnimalIntegrationAsCat(TestCase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.ui = None
        self.animal = None

    def setUp(self):
        self.ui = MockUI()
        self.animal = Animal("Cat", Meowing(), AgileRun(), PawsLicking())

    def tearDown(self):
        del self.ui
        del self.animal

    def test_introduce(self):
        self.animal.introduce(self.ui)
        self.assertListEqual(["This animal is a Cat"], self.ui.out)

    def test_display(self):
        self.animal.display(self.ui)
        self.assertListEqual(
            ["This animal is a Cat", "*Meows*", "*Runs and jumps with agility*", "*licks paws*"], self.ui.out)


if __name__ == '__main__':
    unittest.main()
