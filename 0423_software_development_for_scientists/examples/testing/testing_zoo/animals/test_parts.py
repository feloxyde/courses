from unittest import TestCase
from unittest import main
from animals.cuteness import *


class MockUI:
    # Mock class that will register output
    def __init__(self):
        self.out = []

    def print(self, message):
        self.out.append(message)


class TestPart(TestCase):
    # base class for all part tests that will setup and teardown UI
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.ui = None

    def setUp(self):
        self.ui = MockUI()

    def tearDown(self):
        self.ui = None


class TestCutenessBeCute(TestPart):
    # starting a method name by "test" tells the framework that it
    # is a test case execution
    def test_paws_licking(self):
        pl = PawsLicking()
        pl.be_cute(self.ui)
        self.assertListEqual(["*licks paws*"], self.ui.out)


if __name__ == '__main__':
    unittest.main()
