from .noise import (Crawing, Roaring, Roaring, Meowing, UltrasonicNoises)
from .movement import (AgileFlight, PowerfulRun, PowerfulSwimming, AgileRun)
from .cuteness import (PawsLicking, Geysering, FeatherShaking)
from .animal import Animal


def list_animals():
    animals = {}
    animals["cat"] = Animal("cat", Meowing(), AgileRun(), PawsLicking())
    animals["tiger"] = Animal("tiger", Roaring(), PowerfulRun(), PawsLicking())
    animals["whale"] = Animal(
        "whale", UltrasonicNoises(), PowerfulSwimming(), Geysering())
    animals["alpinechough"] = Animal(
        "alpinechough", Crawing(), AgileFlight(), FeatherShaking())
    animals["fishingcat"] = Animal("fishingcat", Meowing(),
                                   PowerfulSwimming(), PawsLicking())
    return animals
