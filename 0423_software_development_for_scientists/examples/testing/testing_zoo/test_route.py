from unittest import TestCase
from unittest import main
from route import ask_route


class MockUI:
    # Mock class that will register output and provide input sequence
    def __init__(self):
        self.out = []
        self.inputs = []
        self.currentInput = 0

    def input(self, message):
        # each call, we advance in the sequence and return res
        self.out.append(message)
        res = self.inputs[self.currentInput]
        self.currentInput += 1
        return res

    def print(self, message):
        self.out.append(message)

    def set_inputs(self, inputs):
        self.inputs = inputs


class TestAskRoute(TestCase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.ui = None
        self.animals = None

    def setUp(self):
        self.ui = MockUI()
        self.animals = ["animal1", "animal2", "animal3"]

    def tearDown(self):
        del self.ui
        del self.animals

    def test_prompt(self):
        self.ui.set_inputs(["animal1, animal2"])
        route = ask_route(self.animals, self.ui)
        self.assertListEqual(
            ["please list animals you want to see in order, separated by a comma", "animals are : animal1, animal2, animal3", "your answer : "], self.ui.out)

    def test_result_with_space(self):
        self.ui.set_inputs(["animal1, animal2"])
        route = ask_route(self.animals, self.ui)
        self.assertListEqual(["animal1", "animal2"], route)

    def test_result_no_space(self):
        self.ui.set_inputs(["animal3,animal1"])
        route = ask_route(self.animals, self.ui)
        self.assertListEqual(["animal3", "animal1"], route)

    def test_prompt_repeat_on_failure(self):
        self.ui.set_inputs(["animal, animal2", "animal1, animal2"])
        route = ask_route(self.animals, self.ui)
        expected = [
            "please list animals you want to see in order, separated by a comma"]
        expected.append("animals are : animal1, animal2, animal3")
        expected.append("your answer : ")
        expected.append(
            "please list animals you want to see in order, separated by a comma")
        expected.append("animals are : animal1, animal2, animal3")
        expected.append("your answer : ")
        self.assertListEqual(expected, self.ui.out)

    def test_result_on_failure(self):
        self.ui.set_inputs(["animal2, animal", "animal3, animal2"])
        route = ask_route(self.animals, self.ui)
        self.assertListEqual(["animal3", "animal2"], route)


class TestRouteCorrection(TestCase):
    pass


if __name__ == '__main__':
    unittest.main()
