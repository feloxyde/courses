from animals.cuteness import list_cutenesses
from animals.movement import list_movements
from animals.noise import list_noises
from animals.animal import Animal


def list_keys_str(dict):
    list = []
    for key in dict:
        list.append(key)
    glu = ", "
    return glu.join(list)


def create_chimeras(animals, ui):
    noises = list_noises()
    movements = list_movements()
    cutenesses = list_cutenesses()

    noise_query = "Available noises : " + list_keys_str(noises)
    cuteness_query = "Available cutenesses : " + list_keys_str(cutenesses)
    movement_query = "Available movements : " + list_keys_str(movements)

    ui.print("animals : " + list_keys_str(animals))
    answer = ui.input("Do you want to create a chimera (y/n) ?")
    cont = (answer == "y")
    while cont:
        ui.print(noise_query)
        noise = ui.input("your choice : ")
        ui.print(movement_query)
        movement = ui.input("your choice : ")
        ui.print(cuteness_query)
        cuteness = ui.input("your choice : ")
        name = ui.input("please name you animal : ")
        animals[name] = Animal(name, noises[noise],
                               movements[movement], cutenesses[cuteness])
        answer = ui.input("Do you want to create another chimera (y,n) ?")
        cont = (answer == "y")
