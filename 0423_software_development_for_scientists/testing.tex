\documentclass[aspectratio=43]{beamer}

\usepackage[utf8]{inputenc}
\usepackage{listings}
\usepackage{pdfpcnotes}
\usepackage[ddmmyyyy]{datetime}
\renewcommand{\dateseparator}{.}
%SETTING UP LISTINGS 
\lstset{          % print whole listing small
keywordstyle=\color{blue}\bfseries,% underlined bold black 
commentstyle=\color{gray}, % white 
stringstyle=\ttfamily,      % typewriter type for strings
basicstyle=\footnotesize\ttfamily,
breaklines=true,
showstringspaces=false
%postbreak=\mbox{\textcolor{blue}{$\hookrightarrow$}\space}
}     % no special string spaces\textbf{}

%Information to be included in the title page:
\title{0423 Software development for scientists}
\subtitle{4 — Testing}
\author{Félix Bertoni}
\date{26.10.2020\\ (gen. \today)}

\usetheme{Boadilla}

\begin{document}

\frame{\titlepage}

\date{26.10.2020}

\input{presentation/opening.tex}

\begin{frame}
    \frametitle{Testing}
    \tableofcontents
\end{frame}


% ##############################################################################################
% ##############################################################################################
% ##############################################################################################
% ##############################################################################################
\section{Motivations}

\begin{frame}
    \centering
    \includegraphics[width=\textwidth]{presentation/resources/code_quality/title_motivations.png}
\end{frame}

\begin{frame}
    \frametitle{Motivations}
    \framesubtitle{Functional correctness}
    \begin{itemize}
        \item Ensuring Software fits its purpose
              \begin{itemize}
                  \item Functionalities : what it can do ?
                  \item Performances : how fast ?
                  \item Stability/reliability : Are we sure of that ?
                  \item Usability : and for user ?
              \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Motivations}
    \framesubtitle{Maintainability}
    \begin{itemize}
        \item Introducing changes $\rightarrow$ risk of breaking something
        \item called non regression testing
    \end{itemize}
\end{frame}

% ##############################################################################################
% ##############################################################################################
% ##############################################################################################
% ##############################################################################################

\section{Composition/structure of a test}
%#FIXME maybe add cleaner separators between different subsections

\begin{frame}
    \centering
    \includegraphics[width=\textwidth]{presentation/resources/testing/title_structure.png}
    \pnote{start by introducing sample software}
\end{frame}

\begin{frame}
    \frametitle{Structure of a test}
    \framesubtitle{Context and precondition}
    \centering
    \includegraphics[width=\textwidth]{resources/test_composition/context_precondition.png}
\end{frame}

\begin{frame}
    \frametitle{Structure of a test}
    \framesubtitle{Execution and Verification : behavioral}
    \centering
    \includegraphics[width=\textwidth]{resources/test_composition/behavioral_verification.png}
    \pnote{Advantage : not bound to structure (high abstraction)}
\end{frame}

\begin{frame}
    \frametitle{Structure of a test}
    \framesubtitle{Execution and Verification : state verification}
    \centering
    \includegraphics[width=\textwidth]{resources/test_composition/state_verification.png}
    \pnote{Advantage : usually easier and better for refined proving}
\end{frame}

\begin{frame}
    \frametitle{Structure of a test}
    \framesubtitle{Test case, single verification}
    \centering
    \includegraphics[width=0.7\textwidth]{resources/test_composition/test_case.png}
\end{frame}

\begin{frame}
    \frametitle{Structure of a test}
    \framesubtitle{Test scenario, multiple verifications}
    \centering
    \includegraphics[width=\textwidth]{resources/test_composition/test_scenario.png}
\end{frame}

\begin{frame}
    \frametitle{Structure of a test}
    \framesubtitle{Fixture : env and precondition factorization}
    \centering
    \includegraphics[width=0.9\textwidth]{resources/test_composition/fixture.png}
\end{frame}

\begin{frame}
    \frametitle{Structure of a test}
    \framesubtitle{Mock : test isolation (context and objects)}
    \centering
    \includegraphics[width=0.7\textwidth]{resources/test_composition/mock.png}
\end{frame}

%#FIXME add concerns ?

% ##############################################################################################
% ##############################################################################################
% ##############################################################################################
% ##############################################################################################

\section{Environment}

%#FIXME here do a demo straight away !

\begin{frame}
    \centering
    \includegraphics[width=\textwidth]{presentation/resources/testing/title_environment.png}
\end{frame}

\begin{frame}
    \frametitle{Environment}
    \framesubtitle{Automate as much as possible}
    \begin{itemize}
        \item Humans are limited
        \item Tools to help drive tests
              \begin{itemize}
                  \item More reliable
                  \item Complete reports
              \end{itemize}
        \item Tools to automate testing
              \begin{itemize}
                  \item Less effort to run test (MANDATORY)
                  \item Usually faster than manual testing
              \end{itemize}
        \item Some tools are both helper and automation
        \item Some manual tests can still be useful
    \end{itemize}
    \pnote{simple to run test means more will to test}
\end{frame}

\begin{frame}
    \frametitle{Environment}
    \framesubtitle{General automation}
    \begin{itemize}
        \item Python, Make, Bash, Expect, ...
        \item Useful for behavioral testing
        \item A bit inconvenient to use because not specialized
        \item Yet can be nice
    \end{itemize}
\end{frame}


\begin{frame}
    \frametitle{Environment}
    \framesubtitle{Step by step : testing script}
    \centering
    \includegraphics[width=0.85\textwidth]{presentation/resources/testing/testing_script.png}
\end{frame}

\begin{frame}[fragile]
    \frametitle{Environment}
    \framesubtitle{Step by step : test script}
    \begin{itemize}
        \item Let's test a single invocation of the command
        \item \lstinline{./process.py help}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Environment}
    \framesubtitle{Testing framework}
    \begin{itemize}
        \item Amazing for state verification
        \item Exist in every size, for every language
        \item Drastically increase test writing speed
        \item Produces nice and clean reports
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Environment}
    \framesubtitle{UI test automation}
    \begin{itemize}
        \item Testing CLI
              \begin{itemize}
                  \item Fairly hard but not impossible
                  \item Done with scripts as bash, Python or Expect
              \end{itemize}
        \item Testing GUI
              \begin{itemize}
                  \item Extremely hard in most case
                  \item Some GUI libraries, as Qt, have tools to help testing
              \end{itemize}
        \item $\rightarrow$ prefer CLI over GUI when possible
        \item $\rightarrow$ Decoupling interface ins and outs from rest of software is mandatory
    \end{itemize}
    \pnote{in general CLI helps automation when using your software}
\end{frame}


\begin{frame}
    \frametitle{Environment}
    \framesubtitle{Continuous testing}
    \begin{itemize}
        \item Included in some forge software, as GitLab
        \item Allow trigger tests automatically
              \begin{itemize}
                  \item On changes (merges, commits)
                  \item At fixed time (each week, ...)
              \end{itemize}
        \item Prevents merging bad code
        \item Warn about broken dependencies
    \end{itemize}
\end{frame}

%#FIXME talk about decoupling somewhere ?
%#FIXME talk about interface testing ?


% ##############################################################################################
% ##############################################################################################
% ##############################################################################################
% ##############################################################################################

\section{System and unit testing}

\begin{frame}
    \centering
    \includegraphics[width=\textwidth]{presentation/resources/testing/title_system_unit.png}
\end{frame}

\begin{frame}
    \frametitle{Testing goal : Cover all execution routes (or path)}
    \framesubtitle{The dream of any tester}
    \centering
    \includegraphics[width=0.7\textwidth]{resources/system_and_unit_testing/execution_route.png}
\end{frame}


\begin{frame}
    \frametitle{System Testing, the \textit{experimental proof}}
    \framesubtitle{Software seen as a black box}
    \centering
    \includegraphics[width=0.5\textwidth]{resources/system_and_unit_testing/system_testing.png}
\end{frame}

\begin{frame}
    \frametitle{System Testing}
    \framesubtitle{What for ?}
    \begin{itemize}
        \item Work required increases with inner software structure complexity
        \item Good for :
              \begin{itemize}
                  \item A software with extra limited features and simple structure. Example : script to extract color profile of an image
                  \item A software composed mostly of trivial to write code. Example : a web page with trivial scripting
                  \item A software with fairly low stability and reliability requirements. Example : Script to display fun things in console
              \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Unit testing, the \textit{math proof}}
    \framesubtitle{Software seen as an aggregate of components, and isolate them}
    \centering
    \includegraphics[width=\textwidth]{resources/system_and_unit_testing/unit_testing.png}
\end{frame}


\begin{frame}
    \frametitle{Unit testing}
    \framesubtitle{What for ?}
    \begin{itemize}
        \item Testing components isolated from each other
        \item Work required increases with number of components and their need for mocking
        \item Need for UT increase, for a component, with :
              \begin{itemize}
                  \item Complexity of its output (how hard to check). Example : physics collision detection
                  \item Use across the program. Example : core components
                  \item Relations with complexly outputting components. Example : physics collision response
                  \item Ease of testing. Example : a function computing average of an array
              \end{itemize}
    \end{itemize}
\end{frame}


\begin{frame}
    \frametitle{Integration testing}
    \framesubtitle{???????}
    \begin{itemize}
        \item Rarely possible to fully prove a component is perfectly functional.
        \item Therefore also test assembly of components
        \item Well unit tested components can also act kind of like mocks for others.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Unit and integration testing}
    \framesubtitle{Difference}
    \centering
    \includegraphics[width=\textwidth]{resources/system_and_unit_testing/integration_testing.png}
\end{frame}

\begin{frame}
    \frametitle{Integration testing}
    \framesubtitle{???????}
    \begin{itemize}
        \item Well unit tested components can also act kind of like mocks for others.
    \end{itemize}
\end{frame}


\begin{frame}
    \frametitle{Overpowered testing combo}
    \framesubtitle{Combine power of all methods !}
    \begin{itemize}
        \item All levels have advantage
        \item Try finding right balance (a lot of UT/IT in science !)
        \item Different levels passing means different conclusions on sw quality
    \end{itemize}
\end{frame}

% ##############################################################################################
% ##############################################################################################
% ##############################################################################################
% ##############################################################################################

\section{Reliable testing}

\begin{frame}
    \centering
    \includegraphics[width=\textwidth]{presentation/resources/testing/title_reliable_testing.png}
\end{frame}

\begin{frame}
    \frametitle{Reliable testing}
    \framesubtitle{An infinite problem}
    \centering
    Tests ensure software is reliable, but how to ensure tests are reliable ?
\end{frame}

\begin{frame}
    \frametitle{Reliable testing}
    \framesubtitle{Low editing}
    \begin{itemize}
        \item Editing and complexity increase risk of errors
        \item Tests shall be straightforward and as simple as possible
        \item Tests shall be edited as few as possible
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Reliable testing}
    \framesubtitle{Sensitivity over specificity}
    \begin{itemize}
        \item Better having a test failing wrongly than a test passing wrongly
        \item Better having an empty failing test than no test at all
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Reliable testing}
    \framesubtitle{External tester}
    \centering
    Have someone else write part of the tests to avoid biases.
\end{frame}

\begin{frame}
    \frametitle{Reliable testing}
    \framesubtitle{Coverage}
    \begin{itemize}
        \item Informs which code has been run during test phase
        \item Quick overview or non-tested code
        \item GIVES NO INFO ON TEST QUALITY
        \item Yet often sufficient and easy to setup
    \end{itemize}
    \pnote{Dont forget to mention reports}
\end{frame}


\begin{frame}
    \frametitle{Reliable testing}
    \framesubtitle{Mutation testing}
    \begin{itemize}
        \item Test the tests with flawed variants of your program (mutant)
        \item Good to evaluate the tests
        \item Hard to setup
        \item Keep it for extreme reliability need cases
    \end{itemize}
\end{frame}


%#FIXME demo on example


% ##############################################################################################
% ##############################################################################################
% ##############################################################################################
% ##############################################################################################

\section{Test Driven Development}

\begin{frame}
    \centering
    \includegraphics[width=\textwidth]{presentation/resources/testing/title_tdd.png}
\end{frame}


\begin{frame}
    \frametitle{Test Driven Development}
    \framesubtitle{What and why ?}
    \begin{itemize}
        \item Ensure tests are always written for any component ?
        \item Write tests first !
        \item Strict method but can be adapted and combined with others
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Test Driven Development}
    \centering
    \includegraphics[width=\textwidth]{resources/tdd/tdd_overview.png}
\end{frame}

\input{presentation/closure.tex}

\end{document}