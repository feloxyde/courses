\chapter{Programming}
\label{chap:programming}
%#FIXME maybe list in python actually are arrays ?
\section{Example-driven : Let's create animals !}

This chapter presents programming and software architecture elements that can be useful for scientists. Programming and
software architecture are both fields that may require years of learning and practice to be mastered, and what is shown
here is only the surface of these two fields. It is sufficient for most of the small scale projects, and will provide you
a basis for learning faster if you need specific skills to carry on larger projects. This chapter is conducted using an example :
making a small program about creating a terminal-based chimera zoo.

\subsection {Goal}
Let's create a tiny zoo in our terminal. It will have three functionalities : managing the zoo, planning a route and finally taking a tour. This
example will be done in Python since the language is simple to use and widely used in the scientific field.
%#FIXME offer an equivalent in C++, as well as the full project code at appendix ?

Animals in the zoo will have four characteristics : a name, a noise, a movement and a cute action.

The route planning will be about the user selecting animals he wants to see. First we ask him about his preferences.

\begin{lstlisting}[caption={Route planning UI},language=Bash]
Our zoo has a lot of animals : cat, tiger, albatros, whale
Which of them do you want to see ?
\end{lstlisting}

Then he answers.

\begin{lstlisting}[caption={Route planning UI with user wrong answer},language=Bash]
Our zoo has a lot of animals : cat, tiger, albatros, whale
Which of them do you want to see ? cat, tiger, dragon
\end{lstlisting}

We check his answer : all animals specified shall be in the list.

\begin{lstlisting}[caption={Route planning UI with correction},language=Bash]
Our zoo has a lot of animals : cat, tiger, albatros, whale
Which of them do you want to see ? cat, tiger, dragon
Answer incorrect, please choose animals from the list, separated by commas 
Our zoo has a lot of animals : cat, tiger, albatros, whale
Which of them do you want to see ? tiger, cat, whale
Let's go !
\end{lstlisting}

And then we take him to the touring step, which consists of each specified animal being shown\footnote{Well, as nicely as a terminal allows us to show animals.}
in the order specified by the user.

\begin{lstlisting}[caption={Touring UI},language=Bash, label={lst:touring_ui}]
This animal is a tiger :
*roars*
*walks with grace and confidence*
*licks paws*

This animal is a cat : 
*meows*
*runs and jumps with agility*
*licks paws*

This animal is a whale : 
*inaudible ultrasonic noises*
*swims with ample and powerful movements*
*shoots water in the air*
\end{lstlisting}

The zoo management will occur before the user is invited to see the zoo, and will allow the user to create animals by mixing their characteristics. With no limits.
Biology will be trampled here, hence the chimera zoo. We ask if user wants to create an animal, then offers to select each characteristic,
and when done, user can either create a new animal or start planning its route.

\begin{lstlisting}[caption={Animal creation UI}, language=Bash]
Our zoo has a lot of animals : cat, tiger, albatros, whale 
Do you want to add one more (y/n)? y
available noises are : meow, roar, ultrasonic, rifle, woof
your choice ? rifle 
available movements are : walk, fast_run, agile_run, powerful_swimming, fast_swimming, teleportation
your choice ? teleportation
available cuteness are : paws_licking, water_shooting, hide, sarcastic_laughter
your choice ? sarcastic_laughter 
please name your... thing : scary_boy

Our zoo has a lot of animals : cats, tiger, albatros, whale, scary_boy
Do you want to add one more (y/n) ? n
Let's go to route planning then !
\end{lstlisting}

You can note the \textcolor{blue}{$\hookrightarrow$}\space at the start of one line in previous listing. This not actually part of the code
shown in the listing and is used to denote a line wrapping. It means that the line was too long to be displayed in this document, and thus
has to be split in several likes. What follows the arrow is the continuity of the previous line.

\subsection{Order of implementation}

In order to keep things simple, we will proceed by iterations, as presented in the previous chapter.
There will be packed in three groups.

\begin{itemize}
    \item Simple visit and route planning. The goal is to have a fixed set of animals
          from which the visitor can set up a route and then visit it.
          \begin{itemize}
              \item Displaying one or two animals as a fixed route tour
              \item Adding more animals
              \item Getting route from the user
              \item Touring following user's route
          \end{itemize}
    \item Adding more animals in a simpler way. We want to be able to add more animals in a simple way.
          \begin{itemize}
              \item Transferring animals from functions to classes
              \item Grouping animals by their common behaviors using classes inheritance
          \end{itemize}
    \item Making it possible to create animals. We want to be able to build animals.
          \begin{itemize}
              \item Making our code easier to understand and splitting into specialized modules.
              \item Making our modules independents from each other.
              \item Allowing creating animals by combining characteristics.
          \end{itemize}
\end{itemize}

Let's get started.

\subsection{Getting started}

Setting up a python project is pretty simple. We need to install Python first. Please
note the rest of the course will be referring to Python 3\cite{python3_or_2}.

\begin{lstlisting}[language=Bash, caption={Manjaro python install command}]
sudo pacman -S python
\end{lstlisting}

Then we create a folder where our code will be, and we create a Python file, named zoo.py.
\begin{lstlisting}[language=Bash, caption={Creation of the project directory}]
mkdir zoo_project
cd zoo_project
touch zoo.py
\end{lstlisting}

We can quickly test if our setup is working by doing a \textit{hello world}, which
is kind of a convention for programmers when learning new languages. Thus, let's open
zoo.py file and write some code inside.

\begin{lstlisting}[language=Python, caption={Python hello world}]
#This is our first Python script/program !

print("Hello, world !") #printing Hello, world ! to the terminal
\end{lstlisting}

Then we can simply execute our program from terminal by invoking Python interpreter
and passing our file as an argument. It should output \lstinline{Hello, world !} and terminate.

\begin{lstlisting}[language=bash, caption={Running Python hello world}]
python zoo.py 
Hello, world !
\end{lstlisting}

You may have noticed the first line in our python script, starting by a \lstinline{#}. This is
a comment in python\cite{python_comments}. Everything after this character on a line will be ignored by the Python
interpreter, and thus we can use it to add notes in our program. Comments are really useful to
help understand one's code. Their proper use will be discussed later in chapter \ref{chap:code_quality},
\nameref{chap:code_quality}. In this chapter, they will be heavily used to explicit code. Please keep in
mind that this is not exactly how they should be used in actual software development.

\input{chapters/programming_secs/basic_programming}

\input{chapters/programming_secs/object_programming}

\input{chapters/programming_secs/types}

\input{chapters/programming_secs/architecture}

\input{chapters/programming_secs/algorithmics}
