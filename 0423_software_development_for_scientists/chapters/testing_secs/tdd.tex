\section{Test Driven Development}
\label{sec:testing_tdd}

We have seen in this chapter that tests can act as a support for specification, describing what the software does.
We know how important tests are, and how it can be difficult to resist the temptation to skip them and
move onto another feature. If we want to avoid untested code, how about writing tests \textit{before} we code ?

\subsection{The method}
\textit{Test Driven Development}, or \textit{TDD}, is a widely used\footnote{Remember, widely used isn't a proof that it is good !} development method and relies extensively on unit testing. Even if it is strict in its roots, it
can have a lot of flavors depending on other methods it is associated with. The main idea is to write code by tiny steps, also called \textit{iterations},
achieving each time a tiny goal set by a short test. Following is an example were we will be writing a mean function for an
array by using TDD method.

%#FIXME (all doc) is mean or average the right word ? if mean -> change everywhere average is.
%actually seems ok, as average is the same kind of thing a mean is
Let's specify our overall goal. We want to write a function \lstinline{mean} that takes an array as parameters and
returns its mean value. If the input array is empty, function shall return 0.

As we need to take tiny steps, we will address those points separately, starting by the exceptional case. Starting by exceptional
case isn't mandatory, but right now it is simpler as this case is trivial. We first write a test to express it.

\begin{lstlisting}[language=Python, caption={Python TDD writing mean function, step 1 test}]
class TestMean(TestCase) : 
    def test_empty(self):
        self.assertEqual(0, mean([]))
\end{lstlisting}

When we run the test, it shall not pass at first. A test failing, failing to compile or be interpreted is considered as not passing. This is
the case of our test, since we did not even write \lstinline{mean} function. Our test isn't passing, so we can start writing a minimal amount of
code to get our test pass.

\begin{lstlisting}[language=Python, caption={Python TDD writing mean function, step 1 code}]
def mean(array):
    if len(array) == 0:
        return 0    
\end{lstlisting}

Now we run our test, it passes. We then run all our test base, it passes as well, that's to be expected since we have only one test. We review our code, it is high quality. A step has been completed, time
to move on. Average of not empty array is not that trivial, there are a lot of possibilities to cover. We'll therefore try to have few tests that are representative of the mass,
and do one step per test. Why not start by an array with only one element ? First, add a test.

\begin{lstlisting}[language=Python, caption={Python TDD writing mean function, step 2 test}]
class TestMean(TestCase) : 
    #already existing test
    def test_empty(self):
        self.assertEqual(0, mean([]))

    #new test 
    def test_single_element(self):
        self.assertEqual(33, mean([33]))
        self.assertEqual(49, mean([49]))
\end{lstlisting}

We wrote a new test with two new assertions. TDD purists would probably want a single assertion by step. As long as tests are concise and precise, we can consider that multiple assertions are fine, given they validate
the same requirement. Test isn't passing, we can move onto writing our code to pass the test. Minimalistic as always.

\begin{lstlisting}[language=Python, caption={Python TDD writing mean function, step 2 code}]
def mean(array):
    if len(array) == 0:
        return 0    
    if len(array) == 1 :
        return array[0]
\end{lstlisting}

Now we run our test, it passes. Then we run all our test base, it passes as well. We review our code, it is high quality. We completed a step. Let's move onto the last one.
We finally want to check we can handle more than one element. Time for test writing.

\begin{lstlisting}[language=Python, caption={Python TDD writing mean function, step 3 test}]
class TestMean(TestCase) : 
    #already existing tests
    def test_empty(self):
        self.assertEqual(0, mean([]))
    def test_single_element(self):
        self.assertEqual(33, mean([33]))
        self.assertEqual(49, mean([49]))

    #new test 
    def test_multiple_elements(self):
        self.assertEqual(20, mean([10, 20, 30]))
        self.assertEqual(100, mean([100, 0, 200, 300, -100]))
        self.assertEqual(-3, mean([-3, -3, -3, -3]))
\end{lstlisting}

Test is not passing. We can implement, always with minimal effort.

\begin{lstlisting}[language=Python, caption={Python TDD writing mean function, step 3 code}]
def mean(array):
    if len(array) == 0:
        return 0    
    if len(array) == 1 :
        return array[0]
    sum = 0
    for(elem in array) :
        sum += array 
    return sum / len(array)
\end{lstlisting}

Now we run our test, it passes. Then we run all our test base, it passes as well. We review our code, and we find it could be better quality. The for loop handles the 1 element case as well, so
we can correct that with a refactoring.

\begin{lstlisting}[language=Python, caption={Python TDD writing mean function, step 3 refactoring}]
def mean(array):
    if len(array) == 0:
        return 0    
    for(elem in array) :
        sum += array 
    return sum / len(array)
\end{lstlisting}

We run our test, it passes. We run all our test base, it passes as well. After a review our code, we conclude it is high quality. One more step completed. Figure\footnote{Inspired from Xavier Pigeon, \url{https://en.wikipedia.org/wiki/Test-driven_development\#/media/File:TDD_Global_Lifecycle.png}} \ref{fig:tdd_overview} shows an overview of the TDD development cycle.

\begin{figure}
    \centering
    \includegraphics[width=0.9\linewidth]{resources/tdd/tdd_overview.png}
    \caption{\label{fig:tdd_overview}Overview of TDD development cycle}
\end{figure}


\subsection{Rationale and concerns}
Test driven development is, test wise, an extremely reliable development method. By writing tests first, you avoid the risk of forgetting to test what needs to be tested. By unit testing
extensively, you remove the subjective decision to choose what to test or not based on your feelings.

Unit testing everything encourages you to decouple components of your software.
It also emphases design and refactoring phases we have seen in section \ref{subsec:design_and_refactor}. Writing a test before coding is well fit to serve as a support for a design phase, where
you consider \textit{what} you want to achieve and not \textit{how}, and review and refactoring are already at the end of a TDD iteration. Small iterations and refactoring often also help prevent
technical debt.

If you are not extremely confident in your rigor to test your software, and if you plan to use your software in a way where you need reliability, then TDD is likely to
be a good choice.

TDD shows its limits when code structure is likely to change a lot, as for example in a prototyping context. A fast changing code structure may lead to intensive test editing, it increases the risk of introducing
mistakes in them. In the case of fast prototyping, only low amount of testing is wanted, and TDD therefore isn't fit. Once prototype is validated, actual solution can start back from zero using TDD.