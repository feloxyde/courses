
\section{Reliable testing}
\label{sec:reliable_testing}

Tests are like a protection against bugs. However, tests are imperfect and bugs can still exist, even in a
thoroughly tested program. The worst thing that can happen with any safety device is to overestimate its protective
power and expose oneself to danger even greater that if no safety device where here. Therefore, we need methods
to build tests as reliable as possible, as well as methods to evaluate how reliable our tests are.

A set of test can face two types of problems. First one is a lack of coverage, meaning a functionality or unit isn't tested.
Second one is a flaw in one of tests, leading to accepting a result with insufficient verifications.

\subsection{Low editing}
%test shall be as simple as possible
%we shall avoid edit them as well
Error probability in any code increases with its size, complexity, and number of editions. It includes tests. The first thing we want with tests is reliability.
Tests shall be short. Express directly what you want to verify, fixed values are usually bad in software code but perfectly fine in tests.

\begin{lstlisting}[language=Python, caption={Python test with and without fixed values}]
class Affine : 
    def __self__(self, a, b):
        self.a = a
        self.b = b

    def root(self):
        return -b/a  

#this test is so-so, because we rewrite formula, it can cause errors
class TestAffine(TestCase) : 
    def test_root(self):
        aff = Affine(2 , 4)
        a = aff.a
        b = aff.b
        self.assertEqual(-b/a, aff.root())

#this test is good, uses fixed value obtained by external means
class TestAffine(TestCase) : 
    def test_root(self):
        aff = Affine(2 , 6)
        self.assertEqual(-2, aff.root())
\end{lstlisting}

Avoid editing tests. Sometimes, it can be better to test later than to have to rewrite tests. This will be discussed in section \ref{sec:testing_tdd}.
Since testing user interfaces is generally hard, it is advised to decouple interfaces and inner logic. This will be demonstrated in section \ref{sec:testing_zoo}.

\subsection{Sensitivity over specificity}
% rename sensitivity over specificity ?
An empty failing test is better than no test at all. If you know something has to be tested, or can't decide yet, write
and empty failing test case for it. This way you will know something is left to test upon running tests. Some
testing frameworks have a way to express that clearly, and thus reporting the test as not implemented instead of failing.
Otherwise, it can easily be simulated.

\begin{lstlisting}[language=Python, caption={Python not implemented test}]
#assuming we have these classes
class A : 
    #some code here 
    
class B(A): 
    #some code here
    
class C :
    def __init__(self, A):
    #some more code here
    
#an integration test for would look like that with unittest:
    
class TestCWithB(TestCase) : 
    
    def test_construction(self):
        self.assert(False, "Test not implemented")
\end{lstlisting}

Such construction allows for experimental and fast prototyping. If unsure whether a solution is fitting the problem or not, why not simply trying it ?
In that case, developer wants to write code as few as possible to have an answer quickly, but also to have a prototype easy to convert into a definitive solution.
An empty test allows the developer to send a message into the future, stating that when writing some code he skipped testing. When accepting the solution,
he then simply needs to implement tests.

\subsection{External tester}

When implementing your own tests, you can be biased. Therefore, it is wise to have an external tester implementing additional tests for what you develop.
Please note that tests can be included in a code review process, as described section \ref{subsec:design_and_refactor}.

\subsection{Coverage}
\label{subsec:testing_coverage}

Code coverage measures how many execution routes are covered by tests. Route analysis is most of the time done by line or by branches. A branch somehow refers to machine code notions
that are out of the scope of this course, but it can be summarized as a conditional execution route\footnote{Or \textit{path}, as mentioned in a note in a previous
    section of this chapter.}.

\begin{lstlisting}[language=Python, caption={Python branches}]
# this conditional expression has four branches
if a and b : 
    #if body
#next statement

#they can be seen like that 
#1 if a is false, skip to next statement 
#2 if a is true, check b
#3 if b is true, execute body 
# if be is false, skip to next statement
\end{lstlisting}

Coverage gives limited information on the quality of the tests. It only points non-tested code that is \textit{actually written}. If for example you forgot to handle some
errors, coverage will not tell you anything. If such error handling added, without adding more tests, coverage will \textit{decrease}. It can be quite surprising if you
are not aware of this. As an example, the following function computes average of an array, without checking if it is empty or not.

\begin{lstlisting}[language=Python, caption={Python coverage of non written code}]
def average(array):
    sum = 0    
    for (elem in array) :
        sum += array 
    return sum / len(array)
\end{lstlisting}

Let's assume we only wrote one test for it.

\begin{lstlisting}[language=Python, caption={Python coverage of non written code test}]
class TestCWithB(TestCase) : 
    def test_construction(self):
        self.assertEqual(0, average([0, 1, 2, -1, -2]))
\end{lstlisting}

The line coverage for the above function will be 100\%, as all lines are executed. Coverage will not mirror the fact that we simply forgot to check if array is not empty.
We see that, and add the check, without writing a test for it.

\begin{lstlisting}[language=Python, caption={Python coverage of non written code, fixed}]
def average(array):
    if (len(array) == 0):
        return 0
    sum = 0    
    for (elem in array) :
        sum += array 
    return sum / len(array)
\end{lstlisting}

The line coverage now decreased, because we added the check and thus increasing line count, without testing it. Coverage can vary in a quite unexpected manner when editing code base, and give
the wrong software reliability feeling.
Therefore, it is important to remember : it is only showing which written lines are not run during tests, no less, no more.
Still, coverage tools are rather simple to set up and therefore having them at hand is always beneficial, as demonstrated later in section \ref{sec:testing_zoo}. Low code coverage is a warning that your code may not
be sufficiently tested. High code coverage is not a guarantee that your code is sufficiently tested.


\subsection{Mutation testing}

To get an idea of the quality of tests, we can test them using mutants. A mutant is a version of our software where the code has been altered, as for example swapping a
\lstinline{+} for a \lstinline{-}. A solid test base shall reject the vast majority of mutants of a program. If a mutant passes, then it means some components aren't fully tested\cite{mutation_testing_js}.
Such practice is called \textit{mutation testing}\cite{wiki_mutation_t}.
Mutants are clearly impossible for humans to generate in sufficient quantity to be useful, but there are tools that do that fairly well for nearly all languages. They can be much trickier
to set up than coverage tools, as they are less used. They are a bit overkill for most projects, as mutation testing tends to be extremely slow. This course does not demonstrate such setup, but keep in mind the existence of mutation
testing in case you need extremely high reliability on tricky to test code one day.

