
\section{Environment}
\label{sec:testing_environment}

This section describes several tools that can be used in order to test a software. The aim isn't to fully
explain these tools, only to make you aware of their existence and abilities.
As an example, we will set up environment to test a really simple software made of the following code, split between two files.

\lstinputlisting[language=Python, caption={Software sample for test environment, strop.py}, label={lst:test_environment_example_part1}]{examples/testing/environment/strop.py}


\lstinputlisting[language=Python, caption={Software sample for test environment, format.py}, label={lst:test_environment_example_part2}]{examples/testing/environment/format.py}

It takes two or more strings as arguments and formats them according to a selected format.

\begin{lstlisting}[language=Bash, caption={Software sample for test environment execution example}]
python strop concat ad b c d e f
adbcdef
python strop between ad b c d e f
badcaddadeadf
\end{lstlisting}

\lstinline{concat} concatenates all remaining strings together. \lstinline{between} takes first next string and uses it as concatenation insert for all remaining strings.

\subsection{Automation}
\label{sec:testing_automation}

Testing involves performing a lot of actions and a lot of verifications. For this reason,
manual testing is foolish in most cases. First, setting up and executing tests is boring and takes time.
Second, when the number of tests grow, there is a high probability that you will simply not be attentive
enough and miss something out verifying result, letting a bug slip away. Somehow, computers are much
more reliable than humans when it comes to apply a test protocol, given the protocol can be translated
as a program. And fortunately, we most of the time develop software using a computer.
The goal of automation is to make testing as pleasant as possible. This way we are encouraged to write a lot
of tests and run them often. In the ideal case, we only have to perform one single action to know
the result of our tests.

\begin{lstlisting}[language=Bash, caption={What automated testing should look like from the outside}]
test
passing !
\end{lstlisting}

Automation may look like a bother at first, because it requires a bit of setup\cite{quora_when_use_auto_test}. It can also make tests a bit costly to write, because you
have to register them in automation.
Let's do the math. Imagine you really struggle, it takes you three days to get your automated test environment ready. 21 hours
of work. Then, when writing a test, you spend one more minute writing it in comparison to a manual test. Let's say you have 120 tests, making the total to 23 hours
spent on automation. Now, consider each test you wrote to take an average of 30 seconds to execute by hand, while it takes less than 10 seconds
to run them all with automation.
In development phase, we can you will probably run your tests three times per hour. That's
average : in the beginning of a feature, you will probably not run them, when you want the feature to match tests, you will want to run them \textit{a lot}, like
once every two minutes or so.
You will not always need to run all of them, even if it would be better. We'll therefore consider you run 10 tests each time. By hand, every 20 minutes you code, you spend
5 more minutes running tests. In a day of 7 hours of work, you literally spent 1h and 30 minutes running your tests, and you only ran few of the suite. With automated testing,
after 7 hours, you spent a bit more than 3 minutes running your tests, and you ran the full set each time. After roughly 15 days, any test you run manually makes you loose
an incredible amount of time.

One may argue that as a scientist, you will tend to have a lot of tiny scripting projects instead of wide software development tasks, and that setting environment and
tests each time isn't worth it. That would be forgetting that an estimation of three days for setting up environment is extremely pessimistic. You will quickly get used
to the task. \textit{For example, when setting up a C++ project, C++ being my main language, it rarely takes me more than 15 minutes to get a decent testing environment ready.
  And C++ doesn't come with a simple to set up tool set.}

\subsubsection{General automation}

General purpose automation can be done simply using a combination of scripts, as bash, and build tools, as \textit{Make}, presented section \ref{subsec:programming_tools_automation}.
Some tests may be written in plain code, in the same language as our software is. Those are usually state verification related tests, and
can be done either from scratch or with the help of a library or framework, that we will discuss in section \ref{subsec:testing_libraries_and_frameworks}.
Behavioral verification oriented tests will tend to rely more on external automation tools, would it be simple scripts or interface interaction
automation tools.
General automation tool are good for scheduling execution of tests and then aggregating the results if needed. Here is an example using Make.

\lstinputlisting[language=Bash, caption={Test automation with make simple example}]{examples/testing/environment/Makefile}

We can then write a simple test script using bash for testing the error message of our program if no arguments are passed.

\lstinputlisting[language=Bash, caption={Testing with no argument, test\_no\_argument.sh}]{examples/testing/environment/test_no_argument.sh}

Bash tests are not the best and there are many other ways to write tests, as Python for example. Some build tools\footnote{Non-exhaustive list can be found at \url{https://en.wikipedia.org/wiki/List_of_build_automation_software}} are specialized in building a particular
language or set of languages, and can handle test automation tasks.

\subsubsection{Command line interface automation}

Doing things with bash-like scripts can become tedious when needing to test an interactive command line interface. Fortunately, there are
tools to help us with this. One of them is \textit{Expect}\cite{expect_page}. Expect allows you to run a program and send characters on its standard input
based on what it writes on its standard output, as if someone were interacting with its CLI. Our program earlier doesn't feature such interactivity, and thus we will demonstrate it by writing a test script for the following program.

\begin{lstlisting}[language=Python, caption={Expect script example hello world}]
user = input("who are you ?")
print("hello, ", user, " !")
\end{lstlisting}

\begin{lstlisting}[language=Bash, caption={Expect script example}]
#!/usr/bin/expect -f
#line above tells to use expect interpreter instead of bash

#running program we want to interact with
spawn python script_example.py

#now we wait for the prompt
expect "who are you ?"
#sending an input
send "expect interpreter"
#waiting for answer
expect "!"
#waiting for session to close
expect eof
\end{lstlisting}

We will see more of it in section \ref{sec:testing_zoo}, \nameref{sec:testing_zoo}.

\subsubsection{Graphical interface automation}

We won't do any graphical user interface testing demonstration in this course. Except when using web, or web-like technologies as \textit{Electron}, automating GUI testing
is fairly hard. Please note that some large GUI frameworks and libraries, as \textit{Qt}\cite{qt_page}\cite{qt_github}, offer their own
way to automate testing their graphical interfaces. In case you need to build a software with a complex graphical interface that has to be highly tested, it would be wise to use one of those. Qt and alike
have ports in a lot of languages. The counterpart of Qt in Python it \textit{pyQt}.

\subsection{Libraries and frameworks}
\label{subsec:testing_libraries_and_frameworks}

Testing libraries and frameworks can greatly help writing tests that are in the same languages as your software. They provide tools to help you write tests, including fixtures and mocks,
quicker and with an increased reliability. When referring to either a library or framework, we will use \textit{framework} word for simplicity, and also because a lot of in-language
testing tools are frameworks.
If you choose the \textit{unit approach} presented later in section \ref{subsec:unit_testing},
you will probably end up writing dozens, hundreds or even thousands of state verification tests, or at least behavioral tests that will act like state verification ones. In that case, not using
at least a tiny test library is dangerous.
Test frameworks\footnote{A non-exhaustive list of such tools can be found at \url{https://en.wikipedia.org/wiki/List_of_unit_testing_frameworks}} don't only provide you a nice way to write your tests, they can also ease running them, by helping you have a clear output of what is passing, what is failing, and by allowing to
regroup several tests in a single program.

Python has several of those frameworks. \textit{unittest}\cite{unittest_documentation} is present in the standard library.
We will therefore use it to write more test cases for our example code listings \ref{lst:test_environment_example_part1} and \ref{lst:test_environment_example_part2}.
Note that following example doesn't show the full potential of unittest, only a minimal example.

\lstinputlisting[language=Python, caption={Python unittest example, test\_format.py}]{examples/testing/environment/test_format.py}

Then we can simply invoke python with unittest module command in the strop root directory, \lstinline{python -m unittest} and it will give a report. We can notice setup and teardown at each test run.

\begin{lstlisting}[language=Bash, caption={Python unittest example output}]
python -m unittest
setting up between fail
tearing down between fail
Fsetting up between fail
tearing down between fail
Fsetting up concat
tearing down concat
.setting up concat
tearing down concat
.
======================================================================
FAIL: test_between_more_fail (test_format.TestBetween)
----------------------------------------------------------------------
Traceback (most recent call last):
  File "/home/felix/courses_ressources/0423_software_development_methods_for_scientists/examples/testing/environment/test_format.py", line 57, in test_between_more_fail
    self.assertEqual(between(["a", "b", "c", "d"]), "bacadd")
AssertionError: 'bacad' != 'bacadd'
- bacad
+ bacadd
?      +


======================================================================
FAIL: test_between_two_fail (test_format.TestBetween)
----------------------------------------------------------------------
Traceback (most recent call last):
  File "/home/felix/courses_ressources/0423_software_development_methods_for_scientists/examples/testing/environment/test_format.py", line 54, in test_between_two_fail
    self.assertEqual(between(["a", "b"]), "bb")
AssertionError: 'b' != 'bb'
- b
+ bb
? +


----------------------------------------------------------------------
Ran 4 tests in 0.000s

FAILED (failures=2)
\end{lstlisting}

Report indicates, for each assertion, file and line where the assertion is, as well as value tested and expected result.

\subsection{Continuous testing}

Automated testing is good, but it has some limits. If a developer pushes work he forgot to run tests on, or forgets to
test a merge, it can introduce bugs. Also, if your code is at rest for some time, changes in the dependencies, like libraries used,
can introduce bugs as well. Fortunately, we can automate that as well. It is called \textit{continuous testing}\cite{wiki_continuous_t}\cite{continuous_integration}.
We can choose to trigger testing at two moments. First, when changes are introduced in the code base, on commits and merges. Second, at an arbitrary date or recurrent occurrence,
as for example once per week, on Monday morning. Most forge software support such features a way or another. It can be set up directly in version control software as well, as for example by using \textit{Git Hooks}\cite{githook_doc}.
Following example will be based on GitLab, since it is an open core software. To enable such feature, we need to add a \textit{.gitlab-ci.yml} yaml file in our repository\cite{gitlab_doc_ci}.


\begin{lstlisting}[language=Bash, caption={Gitlab CI file for continuous testing}]
#this is a comment in yaml

#we select a docker image, to host our run
image: carterjones/manjaro

#then we create a pipeline stage 
test:
  #we set it to run only when commit on main, dev and develop branches
  only:
     - main
     - dev
     - develop
  #setting name of the stage
  stage: test 
  #setting setup script, here for building a C++ project
  before_script: 
     #installing some libs
     - sudo pacman -Syu --noconfirm && sudo pacman --noconfirm -S gcc cmake make unittestpp glm
     #building
     - mkdir build
     - cd build
     - cmake ..
     - make 
  #running tests 
  script: 
     #it only needs to show run test command, as long as the command returns 0 if pass and another value if fails 
     - make test 
\end{lstlisting}

With this, each time we push a commit on either main, dev or develop branches, tests will be executed for that commit. Recurrent occurrences of tests in GitLab
can be done through the GitLab interface directly and is described in GitLab documentation\cite{gitlab_doc_ci_schedule}, as well as detailed setup. Results will be displayed in a merge
request's thread if the target branch is concerned by the run. Email and other alerts can be configured in case of failure of a run. Of course,
similar features can be found in nearly any forge software.
