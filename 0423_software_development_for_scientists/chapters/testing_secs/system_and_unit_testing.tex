\section{System and unit testing}


When it comes to choose what to test in a software, there is roughly two schools. The first one wants test only the behavior of the software as a whole.
It is often referred to as \textit{functional correctness testing} or as \textit{System testing}. It's opposite school, called \textit{code correctness testing} or \textit{unit testing},
sees a software as an assembly of components.
The main goal of both these approaches is to ensure that the software is functional. Doing so involves covering all executions routes\footnote{Execution path would be a better term in most cases,
      but execution path usually refers to path created by control structures, and may exclude other elements. For example, attempting to divide by zero a floating point number will not result in
      an error but in producing a Not A Number value, and thus will be considered as being one \textit{path}. In the case of testing, we may want to consider it as two possible paths, \textit{division
            by zero} and \textit{OK division}, division
      by zero often being an error. For this reason, we will use the name \textit{route}. Keep in mind that in literature, you probably want to look for \textit{path} and not \textit{route}, as the latter
      isn't a correct term.}
that the software can take, or, said another way, testing the behavior of the software for each possible input. See figure \ref{fig:execution_route} for an
illustration. Of course, proving a software is fully
functional is nearly impossible except in few really simplistic cases, and we can only try to cover as much as executions routes as possible. The number of routes
in a program can be measured as for example with \textit{cyclomatic complexity}\cite{wiki_cyclo_complexity}.

\begin{figure}
      \centering
      \includegraphics[width=0.6\linewidth]{resources/system_and_unit_testing/execution_route.png}
      \caption{\label{fig:execution_route}Illustration of execution route}
\end{figure}

\subsection{System testing}
%#FIXME add figures in all section

System testing considers the program as an opaque box, shown figure \ref{fig:system_testing_view} receiving inputs and producing outputs according to inputs and context. It can be seen somehow as an experimental proof : to show that
the program works, show that it doesn't fail in as many cases as possible. The amount of work of this
approach scales with the complexity of its interface and inner structure. The more complex they are, the more tests we will need to write and the more complicated
the tests will become. System testing can be extremely efficient for :

\begin{figure}
      \centering
      \includegraphics[width=0.4\linewidth]{resources/system_and_unit_testing/system_testing.png}
      \caption{\label{fig:system_testing_view}A software seen through system testing}
\end{figure}

\begin{itemize}
      \item A software that has a really simple structure and a limited
            amount of functionalities. As it will have only a few pairs of inputs and outputs, covering them all isn't really hard. An example can be a script to extract color profile of an image.
      \item A software that is mostly composed of trivial to write code. Since such code will rarely have a lot of errors, not covered
            routes will probably not fail anyway. It can for example be a static web page featuring some trivial scripting.
      \item A software that has fairly low reliability and stability requirements. Since some errors are allowed, not removing them all is fine. A good example for this can be physics of a video game : it doesn't
            need more accuracy than the minimum a human, the player, can perceive.
\end{itemize}

If none of those criterions are met, then unit testing, discussed below, is probably a better choice. System testing is made of nearly only behavioral tests,
thus a lot of interface tests, so covering all cases can become costly really quickly.
System testing has a double-edged property : it does not take in account internal errors if they don't affect behavior of the software. This means that we may save time
by not fixing non-critical errors, but it also means that we may not see growth of technical debt we talked about in section \ref{subsec:design_and_refactor}. Another downside
is that when tests fails, we don't get much information about where the error comes from. Of course, using a debugger will ease the search process, but it still takes some effort.

%#FIXME add examples for all this section

%can be seen as experimental proof

%behavioral
%good for non regression
%inner default correction

\subsection{Unit testing}
\label{subsec:unit_testing}
%#FIXME add a figure to illustrate the view
Unit testing considers the program as an assembly of components, shown figure \ref{fig:unit_testing_view}, and aims to check that all components are functioning properly. This vision is recursive : a component is made of components
as well. It can be seen somehow as a mathematical proof in which to show that the program works, we show that all components of the program work. Components, referred to as units, will usually be functions, methods and classes.
Unit testing requires to isolate each component and to test it with mocks. Testing components separately allows us to drastically reduce the number of execution routes to check in order
to cover every possible routes, and thus drastically eases writing test cases.

\begin{figure}
      \centering
      \includegraphics[width=0.8\linewidth]{resources/system_and_unit_testing/unit_testing.png}
      \caption{\label{fig:unit_testing_view}A software seen through unit testing}
\end{figure}

\begin{lstlisting}[language=Python, caption={Python sample for route simplification}]
def guard(number):
      if number == 0 :
            return 1
      return number

def foo(divisor):
      return 10/divisor

def transform(input):
      #returns a float value according to some rules
      #and the "input" string

#main code 
in = input("enter a string")
tr = transform(input)
gd = guard(tr)

print(foo(gd)) 
\end{lstlisting}

In the previous listing, the critical part is the \lstinline{guard} function. This function ensures that \lstinline{foo} will never receive a divisor equal to 0, which would
result in an error. When testing this program, we will want to test that \lstinline{guard} returns 1 when receiving zero and the original value otherwise.

In a system testing approach,
we would know that one or our two cases aren't covered by using coverage tools introduced later in section \ref{subsec:testing_coverage}. Such tools would tell us
that \lstinline{return 1} statement in \lstinline{guard} has never been executed by any of the tests. However, it does not tell us \textit{how} to cover this case. In order to write
a relevant test, we have to look into \lstinline{transform} code to know which input will produce the right value.

%#FIXME add figure here ?

With unit approach, everything is much easier, as we directly write test cases to verify \textit{guard} requirements, without caring about other functions. Additionally, if at some point
we change the code of \lstinline{guard} and break it, we will be informed directly that this specific component broke. The cost of writing unit tests scales with the number of components in
the software, as well as their need for mocking. For this reason, unit testing can consume a lot of time, and therefore we want to avoid testing everything all the time.
The need for unit testing increases, for each component, with :
\begin{itemize}
      \item Complexity of the output. The harder it is to for a human to validate a pair input-output for a component, the harder it will be to live debug it with a debugger. For example,
            physics collision detection code is hard to debug.
      \item Use across the program. The more a component is used, the more testing it becomes worth it, since it will avoid later errors and reduce risk of technical debt. A core component
            is a good example of widely used component. Obviously, all components of a library are good candidate for unit testing.
      \item Relations with complexly outputting components. A component depending on or using hard to verify components will be difficult to discriminate against error. As an example, a code processing
            the result of collision detection and performing physics response may be trivial, however if it fails it is hard to know if collision data were valid or not.
      \item Ease of testing. If a component requires only few mocking and test cases, why not simply test it ?
\end{itemize}

We can rarely fully prove that a component if working. Because of this, sole unit testing is most of the time insufficient. To compensate this, we do integration testing as well.

\subsection{Integration testing}

%#FIXME add a figure here to explain integration testing

Unit testing tests isolated components. Integration testing tests isolated groups of components together. This difference is pictured in figure \ref{fig:unit_testing_view}. This definition is recursive, thus system testing can somehow be considered as
top level integration testing, and unit testing as bottom level integration testing.

\begin{figure}
      \centering
      \includegraphics[width=0.7\linewidth]{resources/system_and_unit_testing/integration_testing.png}
      \caption{\label{fig:unit_vs_integration_testing}Difference between unit and integration testing.}
\end{figure}

\begin{lstlisting}[language=Python, caption={Python integration testing}]
#assuming we have these classes
class A : 
      #some code here 

class B(A): 
      #some code here

class C :
      def __init__(self, A):
      #some more code here

#an integration test for would look like that with unittest:

class TestCWithB(TestCase) : 

      def test_construction(self):
            sample = C(B())
            #verify stuff here
\end{lstlisting}


The line between a unit test and an integration test isn't always clear.
Is testing a component composed of a fixed set of components considered as unit testing or integrations testing ? On one side, the component is composed of others, so it looks
like integration testing. On the other side, its components are fixed and thus cannot be mocked, making it a unit.

\begin{lstlisting}[language=Python, caption={Python. Is this unit or integration testing?}]
#a component
class Point : 
      #some code here

#another component, higher level
class Rectangle : 
      def __init__(self, x, y, h, l):
            #point here is fixed, Rectangle cannot be tested without the actual component Point.
            self.origin = Point(x, y)
            self.height = h
            self.length = l
\end{lstlisting}

Integration testing can also be seen as a form of unit testing if all components but one are heavily tested. In that case, tested components act like mocks to test the remaining component.

\begin{lstlisting}[language=Python, caption={Python. Integration testing as unit testing}]
class A : 
      #some code here 

#assuming that B is already heavily tested
class B(A): 
      #some code here

class C :
      def __init__(self, A):
      #some more code here

#a unit test using B as a mock for C
class TestC(TestCase) : 

      def test_construction(self):
            #B is used as a mock to test C
            sample = C(B())
            #verify stuff here
\end{lstlisting}

Doing such construction can drastically speed up tests writing. However, if we introduce untested behavior in components acting as mocks later, there is a risk to create some undetected error on unit-like tested
component.

\subsection{Synergy}

All test approaches, often called \textit{testing levels} are important to cover. Used wisely in cohabitation, they can drastically improve the overall quality of your software. They
also can help you take decisions, as fixing a bug or not. Tables \ref{tab:test_synergy_system} and \ref{tab:test_synergy_inner} show a summary of conclusion we can draw regarding which combination of test levels are passing.

\begin{table}
      \centering
      \begin{tabular}{c|c|p{0.6\textwidth}}
            \textbf{system} & \textbf{unit and int.} & \textbf{conclusion}                                                                                                                                                               \\
            \hline
            pass            & pass                   & Everything seems right.                                                                                                                                                           \\
            \hline
            pass            & fail                   & Error does not seem to affect functional correctness, fixing isn't a priority. Beware of technological debt.                                                                      \\
            \hline
            fail            & pass                   & Error seems to affect functional correctness, fixing it is high priority. It has not been detected by unit and integration testing, they are probably insufficient.               \\
            \hline
            fail            & fail                   & Error seems to affect functional correctness, fixing it is high priority. It has been detected by unit and integration testing, thus some component is wrong. Fix should be easy.
      \end{tabular}
      \caption{\label{tab:test_synergy_system}Information in regard of system testing status}
\end{table}

\begin{table}
      \centering
      \begin{tabular}{c|c|p{0.6\textwidth}}
            \textbf{integration} & \textbf{unit} & \textbf{conclusion}                                                                                    \\
            \hline
            pass                 & pass          & Everything seems right.                                                                                \\
            \hline
            pass                 & fail          & A component has an error, fix should be easy.                                                          \\
            \hline
            fail                 & pass          & Components may have a wrong behavior tested or are insufficiently tested. Improving tests is required. \\
            \hline
            fail                 & fail          & A component has an error, fix should be easy.
      \end{tabular}
      \caption{\label{tab:test_synergy_inner}Information in regard of inner testing levels status}
\end{table}

%#FIXME I feel like there is more to add here


