\section{Example : testing the Zoo application}
\label{sec:testing_zoo}

As a larger example, we will set up a test suite for the zoo
program we wrote in chapter \ref{chap:programming}, section \ref{sec:architecture}, \nameref{sec:architecture}.

\subsection{Preparing for the tests : decoupling}

In order to ease testing, we want to decouple our code more than it was before, especially,
we want to decouple user interface from inner logic. We will create a class responsible
for all inputs and outputs. Let's call it \lstinline{UserInterface}. Its implementation is trivial.

\begin{lstlisting}[language=Python, caption={Python Zoo UserInterface}]
class UserInterface :
    def __init__(self):
        pass 

    def input(self, message) :
        return input(message)
    
    def print(self, message):
        print(message)
\end{lstlisting}

It simply forwards calls to \lstinline{input} and \lstinline{print}, and will act as an abstraction allowing us to mock user interface.
All functions and methods requiring an interaction with user will then receive a \lstinline{UserInterface} instance. With these changes, an animal part
will look as follows.

\begin{lstlisting}[language=Python, caption={Python animal part using UserInterface}]
class PawsLicking:
    def __init__(self):
        pass

    def name(self):
        return "paws_licking"

    #ui refers to user interface class
    def be_cute(self, ui): 
        ui.print("*licks paws*")
\end{lstlisting}

Please note that this is not an absolute solution, and not the best solution in a lot of case. It would have been at least as nice to have only a few classes
handling interfaces, and have other methods instead returning and gathering values, display being done at a single place at once.

\begin{lstlisting}[language=Python, caption={Python animals returning and gathering values}]
#please method names are now much less relevant with those changes
class PawsLicking:
    #...
    def be_cute(self): 
        return "*licks paws*"

class Animal :   
    #...    
    def display(self):
        str = ""
        str += self.cuteness.be_cute()
        return str

#somewhere else in code, actual ui code
for animal in animals :
    print(animal.display())
\end{lstlisting}

The first one is more fit in our case since it will have us perform much less transformation to existing code.
You can find full transformed zoo code and all test files in appendix \ref{appendix:testing_the_zoo_python_code}.

\subsection{Unit testing animal parts}

Animal parts are really simple to test, therefore we will start with them. Create a file \lstinline{test_parts.py} in \lstinline{animals/} directory.
As part name is trivial to test, we will only demonstrate with the behavior the part carries on. For \lstinline{cuteness} parts, it is the method
\lstinline{be_cute}. As this method requires an \lstinline{UserInterface} object, we will mock one by relying on Python ducktyping.

\begin{lstlisting}[language=Python, caption={Python UserInterface mock for be\_cute test}]
class MockUI:
# Mock class that will register output
    def __init__(self):
        self.out = []

    def print(self, message):
        self.out.append(message)
\end{lstlisting}

This mock simply stores all messages sent to it through its \lstinline{print} method.

All test will need it, and so we will use the mock as a fixture, by creating a new test class.

\begin{lstlisting}[language=Python, caption={Python UserInterface as a fixture}]
class TestPart(TestCase):
    # base class for all part tests that will setup and teardown UI
    
    def __init__(self, *args, **kwargs):
        #this strange * ** syntax is to simply adapt to any call made on the constructor
        #and transfer arguments to TestCase constructor
        super().__init__(*args, **kwargs)
        self.ui = None #we reserve a slot for ui mock

    def setUp(self):
        self.ui = MockUI() #we create a new UI at setup

    def tearDown(self):
        del self.ui #we delete ui at teardown
\end{lstlisting}

We will inherit from this class to build our tests.

\begin{lstlisting}[language=Python, caption={Python testing a part}]
class TestCutenessBeCute(TestPart):
    def test_paws_licking(self):
        pl = PawsLicking()
        pl.be_cute(self.ui) #ui inherited from TestPart 
        self.assertListEqual(["*licks paws*"], self.ui.out)
    #more tests here
\end{lstlisting}

After writing all our tests, we can run them using \lstinline{python -m unittest} command. Please note we have to use \lstinline{from animals.cuteness import *}
and not \lstinline{from cuteness import *}. It is mandatory to allow tests to be run from root directory of the project, otherwise we would get a \lstinline{ModuleNotFoundError}.

\subsection{Coverage and other setups}

Since we have some tests, it will be simpler for us to set coverage tool up, \lstinline{Coverage.py}\cite{coverage_doc}. We can install it really quickly with \lstinline{pip install coverage}. To invoke coverage,
we run \lstinline{coverage run -m unittest}. It should have created a \lstinline{.coverage} file. This file contains data on the coverage. It can be used to produce
a quick report, using \lstinline{coverage report} command.

\begin{lstlisting}[language=Python, caption={Python zoo coverage report}]
coverage report 
Name                    Stmts   Miss  Cover
-------------------------------------------
animals/__init__.py         0      0   100%
animals/cuteness.py        27     12    56%
animals/test_parts.py      23      1    96%
-------------------------------------------
TOTAL                      50     13    74%
\end{lstlisting}

We can also produce a more detailed HTML report with \lstinline{coverage html} command. It will create a \lstinline{htmlcov} directory containing all report files. Report can be
read using any navigator : \lstinline{chromium htmlcov/index.html}. Result is seen on figures \ref{fig:htmlreportmain} and \ref{fig:htmlreportnoise}.
To erase \lstinline{.coverage} files, we can call \lstinline{coverage erase} command. Please note it does not remove HTML report files.

\begin{figure}
    \centering
    \includegraphics[width=0.8\linewidth]{resources/zoo_testing/htmlreportmain.png}
    \caption{\label{fig:htmlreportmain}Example of a html coverage report, front page}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=0.7\linewidth]{resources/zoo_testing/htmlreportnoise.png}
    \caption{\label{fig:htmlreportnoise}Example of a html coverage report, file details}
\end{figure}

We can also notice that coverage actually counts tests as a source file, while non run files are considered
as not in the software. To select files to include or exclude, we can write a configuration file for coverage, \lstinline{.coveragerc} at the root of the project.

\lstinputlisting[language=Bash, caption={Zoo .coveragerc}]{examples/testing/testing_zoo/.coveragerc}

\lstinline{source = .} tells coverage tool to include any files in the directory, recursively. \lstinline{omit = ...} specifies to exclude all files matching specified patterns.

\begin{lstlisting}[language=Python, caption={Python zoo coverage report with all files}]
Name                  Stmts   Miss  Cover
-----------------------------------------
animals/animal.py        13     13     0%
animals/cuteness.py      27     12    56%
animals/list.py          12     12     0%
animals/movement.py      35     35     0%
animals/noise.py         35     35     0%
chimera.py               31     31     0%
route.py                 15     15     0%
touring.py                3      3     0%
user_interface.py         7      7     0%
zoo.py                   14     14     0%
-----------------------------------------
TOTAL                   192    177     8%    
\end{lstlisting}

To simplify the process of testing and coverage, we can use make tool.

\begin{lstlisting}[language=Bash, caption={Zoo testing makefile}]{examples/testing/testing_zoo/Makefile}
default :
#nothing to do actually

#running tests
test : 
	python -m unittest

#coverage
cov : 
	coverage run -m unittest 

#generating coverage report
covr : cov
	coverage report

#generating coverage html report
covhtml : cov 
	coverage html 

#removing extra files
#|| true allows to bypass error if html cov isn't here, while still informing
clean : 
	coverage erase
	rm -r htmlcov || true
\end{lstlisting}

This way, we can access all we need with simple commands, as \lstinline{make test}.

\subsection{Unit and integration testing Animal}

Unit testing \lstinline{Animal} class isn't much harder than testing a part, it only requires a bit more mocking.
We have to write a mock for each part type. But before that, let's create a \lstinline{test_animal.py} file in \lstinline{animals} package.

\lstinputlisting[language=Python, caption={Python animal unit testing mocks}, linerange={18-30}]{examples/testing/testing_zoo/animals/test_animal.py}

We will also use the same \lstinline{MockUI} class we used for unit testing parts. We can then write two simple tests.

\lstinputlisting[language=Python, caption={Python unit testing Animal}, linerange={35-57}]{examples/testing/testing_zoo/animals/test_animal.py}

As we use the same \lstinline{animal} for both tests, it can be considered a fixture, manipulated through \lstinline{setUp} and \lstinline{tearDown}.
To further check, why don't we implement a simple integration test ? It isn't much different from unit one, simply does not use mocks but parts instead.

\lstinputlisting[language=Python, caption={Python integration testing Animal}, linerange={60-81}]{examples/testing/testing_zoo/animals/test_animal.py}

Even if in this example we have put both integration and unit test in the same file, it would probably be better to separate them in a real development context.

\subsection{Unit testing route planning}

Unit testing route planning, e.g. function \lstinline{ask_route} in lstinline{route.py} file is much harder to test. It is in these
cases that a good decoupling and abstraction of the user interface is primordial. We need to use a much more complicated mock interface than what we did before
to simulate sequential inputs from the user. But first, we want to create a \lstinline{test_route.py} file alongside \lstinline{route.py} file.

\lstinputlisting[language=Python, caption={Python zoo route mock ui}, linerange={6-24}]{examples/testing/testing_zoo/test_route.py}

After that, we will set up a fixed list of animals for all our tests, animals in our case being only string representing their names.

\lstinputlisting[language=Python, caption={Python zoo route test setup}, linerange={27-39}]{examples/testing/testing_zoo/test_route.py}

When creating a test, we can then specify several inputs that will be sent one after another in each \lstinline{ui.input} call.

\lstinputlisting[language=Python, caption={Python zoo route test example}, linerange={57-73}]{examples/testing/testing_zoo/test_route.py}

In these tests, we send two string consecutively, the first one isn't correctly formatted while the second is.

\subsection{System testing zoo}

System testing zoo will not be made using Python, even if it would be possible,
as the main idea is to show how to test any command line interface. It can be done using a bit of \lstinline{Bash} and \lstinline{TCL/Expect}\cite{expect_page} scripting.
We will use Bash to run the overall structure of our tests, setup and teardown, while Expect will allow us to send interactive inputs to our zoo. Let's
place these tests into a \lstinline{tests} directory at the root of our project. We will first create a \lstinline{compare_output.sh} script.

\lstinputlisting[language=Bash, caption={Bash script for system testing zoo}]{examples/testing/testing_zoo/tests/compare_output.sh}

This shall be called with \lstinline{compare_output.sh testname}, where testname refers to the name of a couple of a \lstinline{testname.exp} Expect script and a \lstinline{testname.expr}
expected result file. Let's start by Expect script. We need to install Expect. On Manjaro, it can be done using \lstinline{pacman -S expect} command. Then, we can
write an Expect script in a \lstinline{create_chimera.exp} file.

\lstinputlisting[language=Bash, caption={Expect script for system testing zoo}]{examples/testing/testing_zoo/tests/create_chimera.exp}

We can now run the Expect script and check it is producing the right output manually.

\begin{lstlisting}[language=Bash, caption={Expect script for system testing zoo output}]
spawn python ../zoo.py
animals : cat, tiger, whale, alpinechough, fishingcat
Do you want to create a chimera (y/n) ?y
Available noises : crawing, ultrasonic_noises, roaring, meowing
your choice : crawing
Available movements : agile_flight, powerful_swimming, powerful_run, agile_run
your choice : agile_flight
Available cutenesses : paws_licking, geysering, feather_shaking
your choice : paws_licking
please name you animal : tigercraw
Do you want to create another chimera (y,n) ?n
please list animals you want to see in order, separated by a comma
animals are : cat, tiger, whale, alpinechough, fishingcat, tigercraw
your answer : tigercraw, cat
This animal is a tigercraw
*Craws*
*Flies, wisely using air currents*
*licks paws*
This animal is a cat
*Meows*
*Runs and jumps with agility*
*licks paws*
\end{lstlisting}

We will then create a text file called \lstinline{create_chimera.expr} by dumping Expect script result in a file \lstinline{./create_chimera.exp > create_chimera.expr}
Finally, we can update our Makefile by adding a \lstinline{systest} target.

\begin{lstlisting}[language=Bash, caption={Makefile update for zoo system testing}]
test : utest systest

#was test target previously
utest :
    python -m unittest

systest :
    #calling compare_output.sh on relevant pairs
    cd tests && ./compare_output.sh create_chimera 
\end{lstlisting}

We manually checked the result of our software and
then used it as a test. This method, has to be used wisely as it is prone to create erroneous tests. However, if user with care,
it can drastically increase writing speed of certain tests,
as is sometime much simpler to verify a result rather than anticipating it, especially when dealing with user interfaces.