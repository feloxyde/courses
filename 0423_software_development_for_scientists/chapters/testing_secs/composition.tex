%FIXME make width of figures (scale) more even !
\section{Composition of a test}

The notion carried by the word \textit{test} is vague. This sections aims to clarify a bit what
can be included in the \textit{test} concept. To summarize, a test aims to check that a program produces
the right output, given set context, precondition and input.

\subsection{Context and precondition}
A test will always be executed in a certain context, which consists of everything external to the
program that can have influence on it. For example, it can be the operating system, or a distant
web server with which our program will interact. The context can be set explicitly, which would
probably be the case for a server, or implicitly, as it will be most of the case for the operating system.

Precondition is the state in which the program is right before executing the test. For example,
if we want to verify that user can save a file, we could want as precondition for the test that
a file is already loaded in the software and ready for saving. This is illustrated in figure \ref{fig:test_context_and_precondition}.

\begin{figure}
    \centering
    \includegraphics[width=0.9\linewidth]{resources/test_composition/context_precondition.png}
    \caption{\label{fig:test_context_and_precondition}Test context and precondition}
\end{figure}

\subsection{Execution and verification}

Running a test involves both sending inputs to the program and checking that the program reacts correctly to it.

If verification only checks for the effect the program has on the environment, its output, as shown in figure \ref{fig:behavioral_verification}, we will call it a \textit{behavioral verification}, or \textit{black box test}. It has
the advantage to offer a really high level of abstraction, and therefore to be fairly independent of the program's structure, allowing to change the program
without needing to edit the tests.

\begin{figure}
    \centering
    \includegraphics[width=0.8\linewidth]{resources/test_composition/behavioral_verification.png}
    \caption{\label{fig:behavioral_verification}Behavioral verification}
\end{figure}

If verification checks whether the program is in a certain state after or during execution of the test, as in figure \ref{fig:state_verification}. We will call it a \textit{state verification}, or \textit{white box test}. It offers
more possibilities than behavioral verification, as sometimes checking directly the state of a program is much simpler than verifying its behavior. However,
it shall be used with care because changes in the program structure may require to rewrite the state tests. This will be discussed later in section \ref{sec:reliable_testing}.

\begin{figure}
    \centering
    \includegraphics[width=0.8\linewidth]{resources/test_composition/state_verification.png}
    \caption{\label{fig:state_verification}State verification}
\end{figure}

\subsection{Test case}
A test case is one way to formulate a test that aims to be really concise and precise. It focuses on a single of few inputs and performs the verification after
all inputs has been sent, as show figure \ref{fig:test_case}. It is well suitable for both behavioral and state verifications. When several test cases address the same set of requirements or are
related, they are usually grouped in a \textit{test suite}.
A test case has to be as minimalistic as possible, with only required context and precondition, and fewer possible inputs. Because of this, verifications across
a test suite are independent of each other, allowing testing precisely requirements and avoiding falsely marking some requirements as failed simply because test case failed.
Test case usually are meant to be fully deterministic : they give fixed inputs and expects fixed outputs, excluding any randomness, in order to be fully reproducible, since
they are meant to run often during development or edition of the code.

\begin{figure}
    \centering
    \includegraphics[width=0.6\linewidth]{resources/test_composition/test_case.png}
    \caption{\label{fig:test_case}Test case}
\end{figure}

%#FIXME show examples here

\subsection{Test scenario}
A \textit{test scenario} is the other way to formulate a test. Instead of aiming to be concise, it will try to verify requirements in a more natural way, kind of as a user would use software.
After setting the context and precondition, it sends inputs and verify output right after it, repeating the process as much as needed to verify target requirements, as it can be seen in figure \ref{fig:test_scenario}.
Those tests are compatible with state verifications but are much better for behavioral verifications. During development or edition, they act as a guidance, informing the developer
what the user will do with the program. After the development or edition, they permit validating software requirements in more realistic conditions than test cases do.
Comparing to test cases, they are much easier to write when it comes to verify loosely a wide set of requirements. As they are not forcibly run often during development or edition,
they can be fairly large and imprecise, as well as featuring a fair amount of random inputs. Test scenario can be split into test case and vice versa, depending on the needs. A scenario is,
somehow, a collection of consecutive test cases.

\begin{figure}
    \centering
    \includegraphics[width=0.9\linewidth]{resources/test_composition/test_scenario.png}
    \caption{\label{fig:test_scenario}Test scenario}
\end{figure}

\subsection{Fixtures and Mocks}

Contexts and preconditions may be fairly heavy to write or setup while being required across different test cases or scenarios. In order to keep the code clean and avoiding redundancy, we factorize
contexts and preconditions of tests into \textit{fixtures}\cite{se_fixture_meaning}\cite{so_fixture_programming}\cite{se_what_fixture}, as figure \ref{fig:test_fixture} displays. Fixtures are no more than a setup for context and precondition. Fixtures help tests cases or scenario to be
independent one from another by allowing to setup and tear down context and precondition for each case or scenario run.

\begin{figure}
    \centering
    \includegraphics[width=0.8\linewidth]{resources/test_composition/fixture.png}
    \caption{\label{fig:test_fixture}Fixtures}
\end{figure}

As for a scientific experiment, we want to execute our tests in a controlled environment. We want to avoid contextual errors that would make test fail.
For example, if testing a web page, we want to ensure that test will be immune to unrelated server failures. Therefore, when setting up the context, we may want to
substitute actual context and input elements by artificial mimics that behaves the same way but are much simpler and thus less prone to errors. Those are called
\textit{mocks}\cite{so_what_mocking}\cite{so_what_use_mock}\cite{mocking_code_smell}\cite{mocks_arent_stubs} and are shown in figure \ref{fig:test_mock}. A mock for a large web server could for example be a tiny web server serving a static web page instead of dynamically creating it.

\begin{figure}
    \centering
    \includegraphics[width=0.7\linewidth]{resources/test_composition/mock.png}
    \caption{\label{fig:test_mock}Mocks}
\end{figure}

\subsection{Non-trivial verifications}

Some requirements involving randomness or user interface, or being heavily subject to
not easily controllable context elements. They may prove to be fairly complex to verify, even more to make such verifications deterministic.
This section tries to address such issues.

\subsubsection{Randomness}

%how to check for a random production ?
Randomness is the enemy of tests, and you want to avoid it as much as possible. The best way for this is to have a well-defined separation between
random and deterministic code, and have as much as possible deterministic code\cite{se_test_randomness}.

%#FIXME add figure here
\begin{lstlisting}[language=Python, caption={Decoupling random code, base example}]
#this function is random 
def random_based_behavior() :
    val = dice_throw() #dice_throw generates a random value
    #... here treatments based on random value
\end{lstlisting}

\begin{lstlisting}[language=Python, caption={Decoupling random code, value as param}]
#we can make function deterministic by taking out
#dice call and instead pass it as a value 
def random_based_behavior(val) :
    #... here treatments based on -eventually- random value passed as param through val

#calling such function would look like this : 
random_based_behavior( dice_throw() ) #random 
random_based_behavior( 1 ) #deterministic
\end{lstlisting}

\begin{lstlisting}[language=Python, caption={Decoupling random code, generator abstraction}]
#we can also make it deterministic by abstracting dice as a value generator
def random_based_behavior(value_generator) :
    val = value_generator.get_value()
    #... here treatments based on val

#this way we can define either a dice or a predetermined sequence as value generator
class Dice : 
    def get_value(self):
        return throw_dice()

class Sequence : 
    def __init__(self, values): 
        self.vals = values 
        self.index = 0

    def get_value(self):
        val = self.vals[self.index]
        self.index += 1 #moving onto next value for last call 
        #should be error code here if we reach end of sequence 
        return val

#Calling such a function would look like this : 
random_based_behavior( Dice() ) #random 
random_based_behavior( Sequence([1, 2, 3]) ) #deterministic
\end{lstlisting}


Testing randomness is better done in a separate suite or scenario, with a lot of repetitions to allow statistical confidence in the result.
Please note that most random generators, as Python random module\cite{random_doc}, allow to provide a \textit{seed}, making random generation
deterministic. It may be useful in case decoupling is difficult.

\subsubsection{Performances}

Execution speed of a software is heavily dependent of the hardware it runs on, as well as compiler and version of libraries used.
If performances are always to be evaluated by a human, this is not a problem and issuing a report with raw speed is usually fine.
However, if tests are meant to \textit{ensure} speed criterions are met, one probably wants to change from report to \textit{fail or pass} type of result.
In case of time critical applications, as robotics, where the system has to guarantee to react in a given time, taking a fixed value as reference
is the right solution.

\begin{lstlisting}[language=Python, caption={Python fixed time performance test example}]
#we will consider chrono to be a way to measure time

#main code 
chrono.start()
#code to test here
chrono.end()

if chrono.time() > 1000 : #using a fixed time requirement value
    return False #test failure
else
    return True #test success
\end{lstlisting}

However, if tests are meant to measure progression of the software's performances, and that those variations shall be evaluated independently of the context,
it can be better to use some trivial computing or a context-dependent value as reference.

\begin{lstlisting}[language=Python, caption={Python relative time performance test example}]
#we will consider chrono to be a way to measure time
    
#main code 
chrono.start()
#code to test here
chrono.end()

runtime = chrono.time()
#computing a reference time value that will scale with computer power
chrono.start()
sum = 0
for(i in range(1..100000))
    sum += i
chrono.stop()
reftime = chrono.time()

if runtime > reftime : #using a relative time requirement value
    return False #test failure
else
    return True #test success
\end{lstlisting}

Software performance can vary depending on the execution environment, as for example other processes running on the same computer. It is therefore advised to test
it as if it involved randomness.

\subsubsection{Floating-point}

Floating-point numbers are subject to lack of precision, due to the fact that we represent an interval of real numbers with a limited number of values.
For this reason, unless you are looking for the exact value, use intervals to compare actual value to expected one.

%#FIXME correct this example
\begin{lstlisting}[language=Python, caption={Python float imprecision example}]

val = 1.343
c = cos(val)
a = acos(c)

#this is unlikely to happen because of float imprecision
if a == val :
    print("equals")

#this will likely happen
epsilon = 0.00001
if val - epsilon < a and a < val + epsilon :
    print("in between")
\end{lstlisting}

\subsubsection{UI}

User interface is by far the hardest thing to test in a program, as they usually allow complex workflows and actions, and that isolating components
isn't always possible and can even be unwanted. Even if both graphical and command lines interfaces testing tools will be discussed later in section \ref{sec:testing_automation},
CLI tends to be easier to test and thus shall be preferred if software requirements allow it.
