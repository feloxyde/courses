\section{Basic programming : simply visiting zoo and route planning}

\subsection{Main code : fixed route}

Let's erase all content of the zoo.py file first, and start off again. Our goal is simply to display one or two
animals, as specified in listing \ref{lst:touring_ui}. Our implementation will be really straightforward,
and everything will be done in the \textit{main code} of our program. In python, any instruction, or \textit{statement}, written directly
in a file is part of the main code\cite{python_main_wikibook}\cite{python3_main_overflow}.
This code is the one being first executed by the interpreter when passing it a program.
Therefore, we can simply write our tour straight in the file :

\begin{lstlisting}[language=Python, caption={Fixed route tour}]
print("This animal is a cat :") 
print("*meows*")
print("*runs and jumps with agility*")
print("*licks paws*")
print("") #printing nothing to get simply a new line 
print("This animal is a whale :")
print("*inaudible ultrasonic noises*")
print("*swims with ample and powerful movements*")
print("*shoots water in the air*")
\end{lstlisting}

%#FIXME remove following or change it ?
Running this code should produce a satisfying output. However, doing so is often considered a bad practice\cite{python3_main_overflow}, since it
allows spreading code everywhere in the file, and it can get messy later, when our code is larger. Therefore, we will
split this code in \textit{functions}.

\subsection{Functions : several animals}

In programming field, a function is a set of instructions that can be used more than once, sometime called \textit{procedures} or \textit{subroutines}. They also are used to make code clearer and easier to maintain.
Functions work with three steps. They need to be \textit{declared} then \textit{defined}, and finally \textit{called}. Declaration is
telling the interpreter that the function exists, it usually simply is giving it a name. Definition tells the interpreter what the function is, what are
the instructions it relates to.
In Python, declaration
is done at the same time as definition, using the \textit{keyword} \lstinline{def}. You can see defining a function as teaching
something to someone : you tell him how to do the thing, not to do it. We can express visiting an animal as a function. Let's erase our file and
replace it by :

\begin{lstlisting}[language=Python, caption={Animal as a function, function definition}]
def display_cat() :  #this is how we declare a function 
    #and below is it's definition. It's called "function body"
    print("This animal is a cat :") 
    print("*meows*")
    print("*runs and jumps with agility*")
    print("*licks paws*")

def display_whale() : #we declare another function
    print("This animal is a whale :")
    print("*inaudible ultrasonic noises*")
    print("*swims with ample and powerful movements*")
    print("*shoots water in the air*")
\end{lstlisting}

You can notice how the body of the function is indented using spaces. Python uses that to know when the body of a function stops.

\begin{lstlisting}[language=Python, caption={Function body limitation}]
def a_function() :  
    print("something")  #this is part of the function body

#indentation isn't anymore
print("something else") #this is not part of the body
\end{lstlisting}

Indented code pieces are called \textit{code blocks}. In a lot of other languages, as C, they are delimited using braces \lstinline|{body}|.
We will talk more about them later in this chapter.

We successfully defined our functions. But if you run the file right now, nothing will happen. That's where the third step for using a function
comes : we need to call them. Calling a function is the way we have to tell the interpreter to actually execute its body. At the bottom of our file,
after the functions definitions, we can add the code for calling them :

\begin{lstlisting}[language=Python, caption={Function call}]
display_cat() #displaying a cat

print("") #printing nothing to get a new line, 
          #"print" is actually a function too !

display_whale() #displaying a whale

print("") #new line again 

display_cat() #displaying a cat again
              #function can be called more than once !
\end{lstlisting}

Our code is now a bit clearer, because it is easier to see what it is made of :

\begin{lstlisting}[language=Python, caption={Full animal code}]
#how we display a cat
def display_cat() :   
    #and below is it's definition. It's called "function body"
    print("This animal is a cat :") 
    print("*meows*")
    print("*runs and jumps with agility*")
    print("*licks paws*")

#how we display a whale
def display_whale() : 
    print("This animal is a whale :")
    print("*inaudible ultrasonic noises*")
    print("*swims with ample and powerful movements*")
    print("*shoots water in the air*")

#what we do in our program ("main code")
display_cat()  #displaying a cat
    
print("")  #newline
    
display_whale() #diplaying a whale
    
print("")  #newline
    
display_cat() #displaying a cat again
\end{lstlisting}

Keep in mind that a function has to be declared first and then called. Therefore, its first call shall be lower in the file than its declaration.
That's not true for every language, but it is for most. A function can also be called from a function, as long as it has been declared before.

\begin{lstlisting}[language=Python, caption={Python function calling function}]
def hello() :  #first function
    print("Hello,")

def world() :  #second function
    print("world !")

def hello_world() : #third function
    hello()
    world()

#main code
hello_world()

#output should be :
#Hello,
#world !
\end{lstlisting}

\subsection{Variables : getting user input}
\label{subsec:variables}
Now we want to allow visitor to set up a route. For that, we need to remember its answer somewhere. That's what variables are used for
in a program. A variable is a box where we can put a value to access it later. Usually, a variable has a \textit{type}. A type refers to
the kind of value the variable holds, it can be a number, a character sequence, a boolean, and a lot more, as user defined types. We
will see more about types a later section. A variable has to be declared and then have its value set before using it. In Python, we
declare a variable by... assigning it a value. The type of the variable will be the type of the value we set, and it can change if we
assign a value of different type.

\begin{lstlisting}[language=Python, caption={Python variable declaration and initialization}]
a = 1 #we create a variable a and set it's value to 1
#a is now of type integer
a = 2 #we change the value of a to 2
#a is still an integer 
a = "something" #we change the value of a to a string
#a is now a string (character sequence)
b = 1.0 #we create another variable b and set it to 1.0
#b is now a floating point number (float), e.g decimal  
\end{lstlisting}

The type of variables defines which operations we can do on them. For example, we can add an integer to another integer or to a float,
and we can split a string to form a \textit{list}\cite{python_data_structures}. Lists are variables holding 0 to N values. In Python, a list can hold values
of different types. We usually access each value independently using an index, an integer ranging from 0 to N-1. There are other ways of storing
several values in a variable, as for example by using \textit{dictionaries}\cite{python_data_structures}, also called \textit{map}, which allow indexing values with types
different from integers, as for example a string. In this case, indexes are called \textit{keys}.

\begin{lstlisting}[language=Python, caption={Python lists and dictionnaries}]
a = [1, 2, 3] #creating a list 
# a values are 1,2,3
a.append("22") #adding a value at the end of the list
# a values are 1,2,3,"22" (note how last value is a string and not integer)
a.pop(1) #removing second element in the list 
# a values are 1,3,"22" 
print(a[2]) #printing third value of the list, "22"
a[0] = 10 #changing value of the first element of the list 
# a values are 10,3,"22"

d = {"key1":1, "key2" : "astring"} #creating a dictionnary
# d values are "key1":1,"key2":"astring"
d["key1"] = 2 #changing value of element 
# d values are "key1":2,"key2":"astring"
print(d["key2"]) #printing "astring"
d["key3"] = True #adding a new element to dict, type is boolean
# d values are "key1":2,"key2":"astring","key3":True
\end{lstlisting}

We can also give variables to a function, and a function can produce variables as well. Variables produced by a function
are called \textit{return values}. A function needs to tell how many variables we can pass to it. Those declarations are
called \textit{parameters}. When passing variables to a function, those variables are called \textit{arguments}. Some languages
only allow functions to produce one variable. Python functions can produce more than one.

\begin{lstlisting}[language=Python, caption={Passing variables to a function}]
def addsub(a, b): #declaring a function with two parameters, a and b
    add = a + b #new variable 
    sub = a - b #new variable 
    return add, sub #returning two values

#main code 
#declaring some variables 
d = 10
e = 5

#calling our function, passing d and e as argument,
#and assigning return values to f and g
f, g = addsub(d, e)
# now, 
# f has a value of 15 (d + e) 
# g has a value of (d - e)
\end{lstlisting}

We can notice that in the listing above, we created two variables in the body of the function. Those variables exist only in the function
body, and can not be accessed from outside. Their value is also specific to the call. If we call the function twice, those variables will
not be the same box each time\cite{python_scope}. We say those variables are \textit{local} to the function. Usually, same goes for the parameters : when passing
a variable as an argument, a new box is created and it is copied in it. However, this is not always the case, and this will be discussed later in this chapter.

\begin{lstlisting}[language=Python, caption={Local variables of function}]
def a_function(param):   
    ma_var = 10 #declaring local variable
    param = 5
    print(ma_var)
    print(param)

#main code 
ma_var = 1 #same name as local one
a = 10  
p = 20
#calling function 
a_function(p)
print(ma_var) #prints 1
print(p) #prints 20

#output is : 
#10
#5
#1
#20
\end{lstlisting}

For now, we will assume that our visitor will enter a valid route all the time, so we can add a new function in zoo.py.


\begin{lstlisting}[language=Python, caption={Querying route to the user}]
def ask_route() :
    print("please list animals you want to see in order, separated by a comma")
    #getting user answer using "input(hint)" function returning a string
    answer = input("your answer : ")
    #then, we remove every whitespaces in the answer
    answer = answer.replace(" ","") #actually replacing spaces with nothing
    #finally, converting string as a list using "," as a delimiter
    #"str1,str2,str3" -> ["str1", "str2", "str3"]
    route = answer.split(",")
    return route #returning value
\end{lstlisting}

And to test it, we can simply display the returned value.

\begin{lstlisting}[language=Python, caption={Displaying user's answer}]
#before this are function definitions : display_cat(), display_whale(), ask_route()
#main code 

answer = ask_route()
print(answer)

#touring code below
\end{lstlisting}

\subsection{Flow controls : applying route}

It is time to do real touring ! The solution presented here is NOT the best solution
possible from a programming point of view as it is kept as simple and as straightforward as possible.
But before that we need to add animals and list them. First, add a bunch of animals with the same
function construct we did.

\begin{lstlisting}[language=Python, caption={Adding function animals}]
#define some functions as we did before
def display_animal_name() : 
    print("This animal is an animal name")
    print("animal noises")
    print("animal movement")
    print("animal cute action")
\end{lstlisting}

Then, create a function that lists names of all animals you have. I added a tiger and an alpine chough\footnote{A bird living at high altitude in mountains, often flying in
    and acrobatic manner.}.

\begin{lstlisting}[language=Python, caption={Listing animal names}]
def list_animals() : 
    animals = ["cat", "whale", "tiger", "alpinechough"] #simply listing their names
    return animals
\end{lstlisting}

Let's modify the \lstinline{ask_route} function to ensure that the user gives a valid answer. In order to do this, we will use \textit{control structures}.
We will use two control structures called \textit{iterations}. The first one is a \textit{while loop}, that will run code as long as a condition is met, and the second
one is a \textit{foreach loop}, which will allow us to perform an operation on each element of a list.
Loops have a body as well as function do, delimited by indentation. There can be variables local to the loop's body. They will also be local to each time the loop's body is executed.

\begin{lstlisting}[language=Python, caption={Python while and foreach loop}]
#this is a while loop : 
a = 3
while a > 0 : #as long as a > 0,
    print(a) #we display a
    a = a -1 #we reduce a value by 1 #a is not local to the while

#this code produces an output as : 
#3
#2
#1

#this is a foreach loop :
lst = ["THIS", "IS", "FOREACH!"] 
for word in lst : 
    print(word) #word is a variable local to the foreach's loop body

#this code produces output as : 
#THIS
#IS
#FOREACH!
\end{lstlisting}

We will keep asking the user as long as he has not a valid route. A route is valid when every animal mentioned is listed by \lstinline{list_animals()}. We can
rewrite our \lstinline{ask_route} now.

\begin{lstlisting}[language=Python, caption={New ask\_route function}]
def ask_route(animals): #note that now we pass the animal list as a parameter

    valid = False #set to false to execute while loop once at least
    route = []

    #a boolean value acts as a condition, not negates it (not False <=> True)
    while not valid : 
        print("please list animals you want to see in order, separated by a comma")
        answer = input("your answer : ")
        answer = answer.replace(" ","")
        route = answer.split(",")
        #now let's check for validity
        valid = True #set to true because we assume that it is valid unless it fails
        for animal in route : #for each animal in route,
            #we check that it is present in animals,
            #and that it was present in each previous element of the route
            valid = valid and (animal in animals)

    #at this point, we know route is valid
    return route #return route
\end{lstlisting}

Conditions are expressed through boolean variables. They are values that can be converted to either \lstinline{True} or \lstinline{False}. There is roughly three
ways to produce a boolean.

\begin{lstlisting}[language=Python, caption={Python boolean production}]
#First way : assign boolean value directly to a variable 
bool_true = True 
bool_false = False     

#Second way : compare two values of other types 
i = #here assign a value
j = #here assign a value 
res = i == j # res is True if i equals j, false otherwise
res = i != j # res is True if i is different from j, fo
res = i < j # res is True if i is strictly inferior to j, fo
res = i <= j # res is True if i is inferior or equal to j, fo
res = i > j # res is True if i is strictly superior to j, fo
res = i >= j # res is True if i is superior or equal to j, fo

#third way : combine two boolean values
a = #boolean value assign here
b = #boolean value assign here
res = a and b #True if a and b are True, fo 
res = a or b #True if a or b or both is True, fo
res = not a #True if a is False, fo

#those three ways can be combined into really complex expressions
res = a and b and not (10 < c or c == 19)  
\end{lstlisting}

Now that we have a route correctly setup, we can finally take a real tour. It will be done in another function, \lstinline{take_tour}. To implement our tour,
we will use a foreach loop over the route and another type of control structure, a \textit{conditional structure}, often referred as
\textit{if else-if else}. It allows us to execute code depending on a condition.

\begin{lstlisting}[language=Python, caption={Python if elseif else}]
#this is a conditional structure in Python
a = ... #we don't tell value here, as it will be used to 
        #look at the contitional flow

if a == 0: #if a equals 0
    print("a == 0")
elif a < 10: #if a < 10 AND a != 0
    print("a < 10")
elif a < 30: #if a < 30 and a >= 10
    print("a < 30")
else: #otherwise
    print("else")

#note that elif are executed only if their conditions are met
#and if the previous if and elifs conditions weren't met.
\end{lstlisting}

The \lstinline{take_tour} function will therefore look like this.

\begin{lstlisting}[language=Python, caption={take\_tour function}]
def take_tour(route):
    for animal in route: 
        ifanimal == "cat": 
            display_cat()
        elif animal == "whale":
            display_whale()
        elif animal == "tiger":
            display_tiger()
        elif animal == "alpinechough":
            display_alpine_chough()
        else: #should never happen, case animal is not found
            print("animal ", animal, " not found")
\end{lstlisting}

Actually, such a construct of a lot of \lstinline{if} and \lstinline{elif} checking the value of a single variable could be done using a \textit{selection structure},
also called \textit{switch}\cite{wiki_switch}\cite{golang_switch}\cite{cpp_switch}, but as it isn't supported in Python, it will not be discussed here.

Finally, we can rewrite our main code for it to use our new wonderful functions !

\begin{lstlisting}[language=Python, caption={simple tour main code}]
#functions are defined before this 

#main code

animals = list_animals()
route = ask_route(animals)
take_tour(route)
\end{lstlisting}

The full code at this point can be found in appendix \ref{appendix:basic_full_code}. This implementation
can cause problems if we want to add more animals, because we need to change code at three places : add a new
function as well as change \lstinline{list_animals()} and \lstinline{take_tour()} functions.
A more concise and flexible version using dictionaries to store animals instead of functions can be
found in appendix \ref{appendix:basic_full_code_dictionnary}. It is not mandatory to look at
it, its principles will be explained in the next section of this chapter. It was not demonstrated this way
in this section to show conditional structures, and also since this construct is only valid with this exact
example, if we were doing more than simply printing text for animal actions, it would not work anymore.
