\section{Architecture : animal creation}
\label{sec:architecture}

Only one thing is missing in our chimera zoo : chimeras. Implementing chimeras will allow us to explore
the surface of software architecture. Programming is about writing software. Architecture is about conceiving them.
Most of the time, a software solution involves both programming and architecture. We already did a bit of architecture
without even knowing all along the project : algorithmics as well as designing structure of a program are both a matter of
architecture. Architecture is an extra wide field, probably wider than programming, and there is rarely perfect solution,
it all depends on the context, which means that learning architecture is more a matter of experience than of a theoretical course.
The goal of this section is not to teach architecture, only to give knowledge of some principles that can help design a software.

\subsection{Modules and packages : clearer code structure}

In this chapter, our code will eventually become bigger. \textit{Much bigger}. For this reason we want to split our code between \textit{modules}.
Modules are group of functions, classes and other code stuff having a common purpose. You may have noticed that when writing our Python zoo code,
some comments were acting as borders for implicit areas :

\begin{lstlisting}[language=Python, caption={Comment delimited areas}]
#animals 
#...

#touring 
#...

#main code 
#...
\end{lstlisting}

The idea stays the same, but we will do it in different files. We will then create three new files, alongside our \lstinline{zoo.py} :

\begin{itemize}
    \item \lstinline{animals.py}, in which we move all animal related classes and the function \lstinline{list_animals}.
    \item \lstinline{route.py}, in which we move \lstinline{ask_route} and \lstinline{list_animals_names} functions.
    \item \lstinline{touring.py}, in which we move \lstinline{take_tour} function.
\end{itemize}

\lstinline{zoo.py} file should now be left with only main code inside. Now we need to tell Python where to find all it needs to run main code.

\begin{lstlisting}[language=Python, caption={Importing modules}]
#syntax for importing module is 
#from module import names 
from animals import list_animals
#we don't need to import animals (Cat etc) because they are used only by list_animals
from route import ask_route, list_animal_names
from touring import take_tour
#we need to specify names otherwise we would need to call them by module :
#import animals 
#animals.list_animals()

#main code 
animals = list_animals()
animals_names = list_animals_names(animals)
route = ask_route(animals_name)
take_tour(route, animals)
\end{lstlisting}

In the future, when a file becomes too large, we can simply split it between several submodules. To do so we need to create \textit{packages}.
Packages in Python are a group of modules. However, in some other language package and module can be synonyms. We can see packages as libraries. Let's
split again our animals module into an animals package. First, we create a directory called \lstinline{animals}, alongside \lstinline{zoo.py}. Then
we add one file per animal class : \lstinline{animal.py}, \lstinline{cat.py}, \lstinline{whale.py}, \lstinline{tiger.py}, \lstinline{alpine_chough.py}.
Then we move classes in their respective files. Don't forget to add \lstinline{from animal import Animal} in all other four files. We can create
a last file, \lstinline{list.py}, importing all four animal classes and defining \lstinline{list_animals} function. To finalize our package creation,
we need to add a file \lstinline{__init__.py}\cite{python_modules}\cite{python_nspackages} in our directory to tell Python that this directory is containing a package, and then remove our \lstinline{animals.py} file.
We can now import \lstinline{list_animals} from our package in \lstinline{zoo.py}.

\begin{lstlisting}[language=Python, caption={Importing from package}]
#we simply state the route to the module 
#from package.module import names
from animals.list import list_animals 
from route import ask_route, list_animal_names
from touring import take_tour
   
#main code 
animals = list_animals()
animals_names = list_animals_names(animals)
route = ask_route(animals_name)
take_tour(route, animals)
\end{lstlisting}

Splitting like this can seem a bit overkill, considering our file was only a bit larger than 100 lines, but in the two next
steps, we will write a lot of code.

\subsection{Abstraction and interfaces}
\label{subsec:abstraction_and_interfaces}
%avoid global variables ?
%#FIXME this part needs to be rewritten
In section \ref{subsec:polymorphism}, \ref{subsec:polymorphism}, we saw that we can handle variables of different types as if they had all the same type,
by defining a common way of interacting with them. This process is a form of \textit{abstraction} and the \textit{how to interact} is called an
\textit{interface}. Interfaces can take various forms, and we already encountered several of them. For example, when we declare a function, we actually
create an interface to interact with the function, stating the number of parameters and eventually their types and the return type of the function. The \lstinline{Animal}
class we used as a base for concrete animal classes, as \lstinline{Cat}, also acts as an interface, by defining operations like \lstinline{introduce()} or \lstinline{display()}.
Types are interfaces to the data in a variable. Interfaces are useful from both a design and a programming point of view. They can either be explicit or implicit. We
will consider an interface explicit if it can be expressed solely in code and be known and understood by both the programmer and the compiler.

In programming, explicit interfaces are useful to state what needs to be used with a component, as classes or functions, without needing to look into the component's code. The
following example is in C++, because Python makes it hard to define explicit interfaces. Following code defines an interface but is not valid C++, it has been simplified
for understandability.

\begin{lstlisting}[language=C++, caption={An interface in C++ (invalid code)}]
//this is a way of defining an interface in C++ : a class without any field,
//having only methods. Please note methods are not implemented here.
//class TableInterface can NOT be instanciated into an object
class TableInterface {
    void putOn(Object o);
};

//Here, TableInterface is used as an interface : if you want to call 
//setTheTable, you need to pass it an object that 
//matches the TableInterface interface (eg inherits from it)
void setTheTable(TableInterface table){
    //do something with table
}

\end{lstlisting}

If we place ourselves in the point view of someone wanting to use \lstinline{setTheTable}, we know that we need to give an object
that allows to \lstinline{Object} to be \lstinline{putOn} it.

\begin{lstlisting}[language=C++, caption={A C++ class matching an interface (invalid code)}]
//By making WoodenTable inherit from TableInterface, we tell that
//WoodenTable matches the interface TableInterface
class WoodenTable : TableInterface {
    //...
    void putOn(Object o){
        //do something with o
    }
};
\end{lstlisting}

Defining interfaces has a heavy consequence on the way we design software : we can define components without even implementing components it will use or act on.
In the previous C++ example, we first defined the \lstinline{TableInterface}, then \lstinline{setTheTable}, and finally \lstinline{WoodenTable}.
If we didn't define an interface, we would have needed to first define WoodenTable, and then a function \lstinline{setTheTable(WoodenTable object)}.
Of course, it also allows \lstinline{setTheTable} to work on any possible table in a polymorphic way. This abstraction is extremely convenient when
developing libraries, where you can declare what your tools will need without having to define it, letting this responsibility to the user.
Dependency between components is called \textit{coupling},
and through interfaces we can reduce coupling, as shown figure \ref{fig:architecture_coupling}. Loosely coupled code is easier to test. This will be discussed in chapter \ref{chap:testing}, \nameref{chap:testing}.

\begin{figure}
    \centering
    \includegraphics[width=0.9\linewidth]{resources/architecture/coupling.png}
    \caption{\label{fig:architecture_coupling}Illustration of loose or tight coupling}
\end{figure}


Even if Python does not allow creating fully explicit interfaces, it still helps to reduce coupling. More than that, because of the combination of dynamic typing
and duck typing, Python ensures that all components we create are fairly loosely coupled. However, it makes harder to express requirements for the programmer, which has to be
done through documentation.

\begin{lstlisting}[language=Python, caption={Python interface through documentation}]
#a python documentation is done using specific comments,
#right after the first line of the code block of a component,
#in between """ """. It can be spread over multiple lines. These are called Docstrings
def set_the_table(table_like) : 
    """set the table sets the table, it requires table_like to have a putOn(object) method."""
    #this line shall be empty
    #here do something with table_like.
\end{lstlisting}

Documentation and the implication of not having explicit interfaces will be discussed further in chapter \ref{chap:code_quality}.

\subsection{Composition : animals better than with inheritance}
\label{subsec:composition}

It's time to put aside ethical concerns about mixing animals and focus on \textit{how} we can mix animals. I meant mix animals \textit{together}, which is as unethical as mixing
an animal alone, but is much harder to do. To keep this course and example relatively simple, we will continue to consider that animals have four characteristics.

First, we need to observe what happens to our class structure when we drastically diversify animals in our zoo. For now, there were only few animals, and creating
an ancestor tree to factorize their characteristics is fairly easy. But can we reach a moment where it will be impossible to factorize using inheritance ? Yes, and
it can come much faster than we think. Let's add one more animal, the \textit{fishing cat}, which is actually a real specie. Even if its name is fairly explicit,
here are its characteristics :

\begin{itemize}
    \item \textit{name} : fishing cat.
    \item \textit{movement} : powerful swimming.
    \item \textit{noise} : purring. Actually, I'm not sure they can purr, but let's say they can because it's cute.
    \item \textit{cute action} : paws licking.
\end{itemize}

Well, that's a cat who fishes. And now with inheritance we can't avoid redundancy anymore. As shown figure \ref{fig:architecture_fishingcat}, we can either make the fishing cat a feline,
sharing paws licking with others, but then we would have to write twice the powerful swimming code it has in common with whale,
or create a \textit{powerful swimmer} class that would be ancestor of both whale and fishing cat, and write twice the paws licking code.
From a biology point of view, it may make more sense to consider that fishing cat is a feline. However, since we intend to create chimeras,
biology does not make a lot of sense in our code. Stating fishing cat as a feline would only postpone the problem, and as we create
more chimeras we will have to write again code we already wrote in another branch of our inheritance tree.

\begin{figure}
    \centering
    \includegraphics[width=0.55\linewidth]{resources/architecture/fishingcat.png}
    \caption{\label{fig:architecture_fishingcat}Fishing cat ancestors problem}
\end{figure}


Multiple inheritance could be a solution : we would create one base class for each characteristic, and each concrete animal class would
inherit from three characteristics classes, name being common to all.

\begin{lstlisting}[language=Python, caption={Python animals with multiple inheritance, name field is ommitted}]
class PawLicker : 
    def be_cute(self):
        #... 

class PowerfulSwimmer :  
    def move(self) : 
        #... 

class PurringThing : 
    def make_noise(self) :
        #... 

class FishingCat(PawLicker, PowerfulSwimmer, PurringThing) : 
    def introduce(self) : 
        print("this animal is a fishing_cat)
        print("btw multiple inheritance is bad)
\end{lstlisting}

As we already said in section \ref{subsec:inheritance}, multiple inheritance is, in most of the cases, the wrong solution. A lot of languages do not
support multiple inheritance, and in most languages supporting multiple inheritance it is a mess to work with. Python is an exception, and
multiple inheritance is \textit{not so bad} with it. When someone feels an urge to use multiple inheritance, often have a misunderstood desire
to use \textit{composition}. We can model an animal as an assembly of parts. One part for movements, one part for noise and one part for cuteness, and that's
what we call composition. Name is always a string so it will be kept a string, field of \lstinline{Animal} class.

\begin{lstlisting}[language=Python, caption={Animal by composition}, label={lst:animal_by_composition}]
class Animal : 
    def __init__(self, name, noise, movement, cuteness):
        self.name = name 
        self.noise = noise
        self.movement = movement 
        self.cuteness = cuteness

    def introduce(self) :
        print("This animal is a ", self.name)

    def display(self) : 
        self.introduce()
        self.noise.make_noise()
        self.movement.move()
        self.cuteness.be_cute()

#main code 
#how we create a Cat, we suppose classes Meow, AgileRun and PawsLicking 
#to be defined
cat = Animal("cat", Meow(), AgileRun(), PawsLicking())
\end{lstlisting}

This construction makes it really easy to create animals with a lot of diversity, while never having to write the same code twice. It also makes it
possible to create animals at runtime, as composition of an \lstinline{Animal} object is chosen at runtime. However, it makes creation of a specific animal
a bit harder. We can compensate this deficit using either a class or a function to create an animal explicitly.

\begin{lstlisting}[language=Python, caption={Easy composition animal creation}]
#cat explicit creation by function 
def create_cat():
    return Animal("cat", Meow(), AgileRun(), PawsLicking()) 

#cat explicit creation by class, we pass components to base class constructor
#in constructor
class Cat : (Animal) : 
    def __init__(self):
        Animal.__init__(self, "cat", Meow(), AgileRun(), PawsLicking())
\end{lstlisting}

We can now create three files in the package \lstinline{animals}: \lstinline{movement.py}, \lstinline{noise.py} and \lstinline{cuteness.py}. They will
receive components. Each component has to define a name, unique
within components of a same file. The file shall also define a \lstinline{list_components} function that returns a dictionary of all components of the file,
indexed by their name. Here is an example for \lstinline{cuteness.py}.

\begin{lstlisting}[language=Python, caption={Cuteness sub package}]
#defining components
class PawsLicking : 
    def __init__(self) :
        pass 
    def name(self) :
        return "paws_licking"
    def be_cute(self) :
        print("*licks paws*")

class Geysering : 
    def __init__(self) :
        pass 
    def name(self) :
        return "geysering"
    def be_cute(self) :
        print("*licks paws*") 

#... other components 

#listing them 
def list_cutenesses() :
    ctns = {}
    ctns["paws_licking"] = PawsLicking()
    ctns["geysering"] = Geysering()
    #... and so on
    return ctns 
\end{lstlisting}

Let's then remove files \lstinline{cat.py}, \lstinline{whale.py}, \lstinline{tiger.py} and \lstinline{Alpine_chough.py}, and then update \lstinline{animal.py}
with code shown in listing \ref{lst:animal_by_composition}. We also can update function \lstinline{list_animals} in file \lstinline{list.py} to adapt to the
new way of creating animals.

Now, we will put visitor related code in a new file called \lstinline{chimera.py}, alongside \lstinline{zoo.py} at the root of our project, and implement a function
to create chimera there. Don't forget to import components and \lstinline{Animal}.

\begin{lstlisting}[language=Python, caption={Chimera creation}]
#an utility function for listing keys of a dictionnary
#and returns them as a string
def list_keys_str(dict) :
    list = []
    for key in dict : 
        list.append(key)
    glu = ", "
    return glu.join(list)

#this code assumes the visitor does no errors when writing part names
def create_chimeras(animals)
    noises = list_noises()
    movements = list_movements()
    cutenesses = list_cutenesses()

    noise_query = "Available noises" + list_keys_str(noises) 
    movement_query = "Available movements" + list_keys_str(movements)
    cutenes_query = "Available cuteness" + list_keys_str(cutenesses)
    print("animals : ", list_keys_str(animals))
    answer = input("Do you want to create a chimera (y/n)?")
    continue = (answer == y)
    while continue : 
        print(noise_query)
        noise = input("your choice : ")
        print(movement_query)
        movement = input("you choice : ")
        print(cuteness_query)
        cuteness = input("your choice : ")
        name = input("please name your animal")
        #animals will also get added outside the function since it is passed by ref
        animals[name] = Animal(name, noises[noise], movements[movement], cutenesses[cuteness])

        answer = input("Do you want to create another chimera (y/n)?")
        continue = (answer == y)
\end{lstlisting}

After few modifications to \lstinline{zoo.py}, to change imports and create chimera, our zoo will be ready.

\begin{lstlisting}[language=Python, caption={Chimera creation zoo.py}]
from animals.list import list_animals 
from route import ask_route, list_animal_names
from touring import take_tour
from chimera import create_chimeras
       
#main code 
animals = list_animals()
create_chimeras(animals)
animals_names = list_animals_names(animals)
route = ask_route(animals_name)
take_tour(route, animals)   
\end{lstlisting}

You can find the complete code of the zoo in appendix \ref{appendix:architecture_full_code}, as well as a C++ version in appendix \ref{appendix:architecture_full_code_cpp}.
When using composition instead of inheritance, we somehow move behavior from method to a field, and a field is much simpler to change at both compile time and runtime.
It helps to develop flexible software, but isn't the solutions for all cases where genericity is needed. Inheritance usually leads to writing less code, even if this
difference might be small sometimes. When choosing between the two solutions isn't easy to decide, it can be advised to choose the one preferred by the programmer,
and to eventually change later, as it will be discussed in chapter \ref{chap:code_quality}, \nameref{chap:code_quality}, section \ref{subsec:design_and_refactor}.

\subsection{Design patterns : pre-made solutions}

Software development often requires to solve architectural problems. Fortunately for us most problems archetypes have already been encountered and solved
by other developers or architects. Therefore, when encountering a problem that is non-trivial the best reflex to have is to look in literature or on the internet if
it hasn't already been solved. This can be bothersome if we don't know specific names of structure we want to achieve. For this matter, one name can be remembered :
\textit{design patterns}. They are a group of solutions that address nearly any architectural problem a developer can encounter, and by remembering this name, it
is easy to find a list of them. Design patterns are only archetypes of solutions and have to be adapted to perfectly fit one's need. Design patterns are
often expressed in terms of object-oriented programming but can easily be translated to other programming paradigms. Using them provides an already battle tested solution,
that can be named for better design and documentation clarity. Let's observe some of them.


The composite pattern allows representing tree-like structures, as for example a file and directory structure.

\begin{lstlisting}[language=Python, caption={Composite pattern}, label={lst:composite_pattern}]
#we can use this pattern to represent a file structure
#defining a base class component, it is an interface
class Component : 
    def display(self) : #declaring operation
        pass 

#defining a composite class directory, which a component as well
class Directory (Component) :
    def __init__(self, name) :
        self.components = [] #holds a list of components
        self.name = name
    def display(self) :  #operation usually involves performing operation on components
        print("directory ", name, " :")
        for comp in self.components : 
            comp.display()
    def add(self, component) : #obviously, component is supposed derived from Component
        self.components.append(component)

#defining another component class 
class File (Component) :
    def __init__(self, name) :
        self.name = name 
    def display(self) : 
        print(self.name)
\end{lstlisting}


The observer pattern enables an object to be notified by another object. It can be useful for simulations,
where objects need to react to a change of a specific object.


\begin{lstlisting}[language=Python, caption={Observer pattern}]
#defining an Observable class, that is a rigid body
class RigidBody : 
    def __init__(self) :
        self.observers = [] #remembers the list of observers
    def add(self, observer) :
        self.observers.append(observer) #registers observer to the list 
    def on_collision(self) :
        #do some complicated physics stuffs
        #notify observers 
        for obs in self.observers : 
            obs.notify_collision(self)

#defining an observer that can be notified, it is an interface
class RigidBodyObserver() : 
    def observe(self, body) :
        body.watch(self)
    def notify_collision(self, body) :
        pass 

an observer could be for example an object counting collisions
class CollisionCounter(RigidBodyObserver) : 
    def __init__(self) : 
        self.count = 0
    def notify_collision(self, body) :
        self.count += 1 #increasing by 1 
\end{lstlisting}

The iterator pattern abstracts a group of data to allow performing operations on it regardless of the structure of the group.

\begin{lstlisting}[language=Python, caption={Iterator pattern}]
#interface declaring an object providing iterators
class Iterable : 
    def begin(self) :
        pass

#inteface declaring an iterator
class Iterator() :   
    def is_end(self) :  #are we done iterating ?
        pass 
    def next(self) :  #go to next item
        pass
    def value(self) :  #get value of current item
        pass

#defining a list class with iterator
class ListIterator(Iterator) : 
    def __init__(self, list, index) : 
        self.list = list
        self.index = index
    def is_end(self) : 
        return self.index == len(list.inner_list)
    def next(self) : 
        self.index += 1
    def value(self) : 
        return self.list.inner_list[self.index]

class List(Iterable) : 
    def __init__(self)  :
        self.inner_list = [] #we use a list in our list, yep, that's cheating !
    def begin(self) :  
        return ListIterator(self, 0)
    def append(elem) : 

#defining a vector3D with class iterator 
class Vector3DIterator(Iterator) : 
    def __init__(self, vector, field) : 
        self.vec = vector
        self.field = field
    def is_end(self) : 
        return self.field == "end" #not at the 3rd field yet
    def next(self):  
        if(self.field == "x") : 
            self.field == "y"
        elsif(self.field == "y") :
            self.field == "z"
        else :
            self.field == "end"

    def value(self) : 
        if(self.field == "x") : 
            return self.vec.x
        elsif(self.field == "y") :
            return self.vec.y
        else :
            return self.vec.z

class Vector3D(Iterable) : 
    def __init__(self, x, y, z) : 
        self.x = x
        self.y = y
        self.z = z
    def begin(self) : 
        return Vector3DIterator(self, "x")

# main code for iterating 
vec = Vector3D(...)
iter = vec.begin()
while not iter.is_end() : 
    print(iter.value())
    iter.next()

lst = List()
#... filling up list
#iteration code is absolutely the same !
iter = list.begin
while not iter.is_end() : 
    print(iter.value())
    iter.next()
\end{lstlisting}


\subsection{Justice for multiple inheritance}

We said several times in this chapter that multiple inheritance is bad. Indeed, if used without care, multiple inheritance
can later cause heavy and painful headaches, bringing confusion and despair on your project. However, if knowing its common
caveats, and used with care, multiple inheritance can save a lot of work.

The first main problem of multiple inheritance occurs when redundant names appear in inheritance tree.

\begin{lstlisting}[language=C++, caption={C++ multiple inheritance name issue}]
class A {
    void operation(){
        //...
    }
}

class B {
    void operation(){
        //...
    }
}

class C : A, B { //C inherits from both A and B
    //...
    //does not redefines operation
}

//main code 
C c = C();
c.operation(); //is this operation method from A or B ?
\end{lstlisting}

In that case, C++ will force you either to reimplement \lstinline{operation} in \lstinline{C} definition, or to precise which base class
you want to call \lstinline{operation} from : \lstinline{c.A::operation()}.

The second problem is a consequence of the first one : in case a class is inherited several times, shall the derived class have several
copies of it in its definition ? This is referred as the \textit{Diamond inheritance problem}.

\begin{lstlisting}[language=C++, caption={C++ Diamond inheritance problem}]
class A {};

class B : A {}; //B is made of A

class C : A {}; //C is made of A

class D : B, C {}; //D is made of both B and C
//Does it has two A part or one ?

\end{lstlisting}

This may look like a trivial problem, but \lstinline{B} or \lstinline{C} can eventually require to maintain their base class \lstinline{A} in
a certain state, and having only one shared \lstinline{A} part could cause a problem. On the other hand, not sharing \lstinline{A} can lead to strange behavior
is \lstinline{A} is meant to be shared, as for example if it holds a counter that \lstinline{C}, \lstinline{B} and \lstinline{D} will use.

This looks painful, so why would we use multiple inheritance ? If we need to express interfaces as we saw in section \ref{subsec:abstraction_and_interfaces},
multiple inheritance may be the solution.

\begin{lstlisting}[language=C++, caption={C++ interfaces multiple inheritance, simplified code}]
class DoSomething {//this is an interface
    void something(); 
};

class DoSomethingElse { //another one
    void somethingElse();
}

//this is a concrete class matching both DoSomethingElse
// and DoSomething interfaces, and this is perfectly fine,
//this way, DoAll can be used in a polymorphic way as being 
//both of them
class DoAll : DoSomethingElse, DoSomething { 
    void something(){
        //...
    }
    void somethingElse(){
        //...
    }
}
\end{lstlisting}

Fortunately, C++ has specific rules for interfaces-like classes, containing only methods declarations and no fields nor method implementations,
avoiding multiple inheritance related problems for interfaces\cite{cpp_inheritance}. Other languages
may have other solutions, for example Java differentiates to entities, \textit{interface} and \textit{class}\cite{java_abstraction}\cite{java_interface}. A class can only inherit from one class but
implement multiple interface, allowing for flexible and yet simple polymorphism.

The other case where multiple inheritance shows its interest, is when we have a composition, as presented section \ref{subsec:composition}, \nameref{subsec:composition},
that is recursive. In such a case, the composite class also wants to match component's interface. This can easily be demonstrated through Python ducktyping.

\begin{lstlisting}[language=Python, caption={Python recursive composition}]
Class Component1 : 
    #... 
    def method_one(self):
        #...
Class Component2 : 
    #... 
    def method_two(self):
        #... 

class Composite : 
    #... 
    def __init__(self, component1, component2):
        self.component1 = component1
        self.component2 = component2

    def method_one(self):
        self.component1.method_one()
    
    def method_two(self):
        self.component2.method_two()
\end{lstlisting}

We can see that the composite implementation of methods simply calls for corresponding component's method. This is called \textit{method forwarding}.
This is additional work that would not have occurred with multiple inheritance.

\begin{lstlisting}[language=Python, caption={Python recursive composition using multiple inheritance}]

Class Component1 : 
    #... 
    def method_one(self):
        #...
Class Component2 : 
    #... 
    def method_two(self):
        #... 
    
class Composite(Component1, Component2) : 
    #... 
    # interfaces are already supported
\end{lstlisting}

To conclude on multiple inheritance, I'll give you my \textit{opinion} on when it is always worth to consider.

When you want to have polymorphism through
interfaces, multiple inheritance is worth, and is usually not a choice. Be careful in this case to have as few as possible \textit{implementing} classes
in the inheritance tree : only non-interface classes should be never multiply inherited.

When you have a need for composition, and you know that you will
not have to increase the number of components nor often modify their inheritance tree, then multiple inheritance is maybe a better choice than composition.

Finally, when creating parts of your software that are not meant to change or be subject to any kind of flexibility, as the very core of your software,
multiple inheritance can be good as well.