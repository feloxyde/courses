\section{Object programming : more animals}

%#FIXME explain that it is not exactly object oriented but conceptual and philosophy ?

\subsection{Classes : more intuitive animals}
You may have felt like representing animals with functions was a bit strange. And you definitely
would be right. Because, through \lstinline{display_xxx()} functions, animals were seen only as
the effect they had on the environment : their show. Even if it was sufficient for our first steps,
it would be inconvenient in case we need to define a lot of animals. A solution for that would be
to simply do as in example found in appendix \ref{appendix:basic_full_code_dictionnary}, using dictionaries.
In this example, animals characteristics are represented by data, and a function \lstinline{display_animal()}
has the responsibility of displaying an animal regarding its data.

\begin{lstlisting}[language=Python, caption={Animals represented as dictionnaries}]
def create_animal(animals, name, noise, movement, cute_action):
    #adding a value to the animals dict and returning it
    dct = animals 
    animal = {"name":name, "noise":noise, "move":movement, "cute":cute_action} 
    dct[name] = animal
    return dct

def display_animal(animal): #accessing value using keys and displaying them
    print("This animal is a ", animal["name"])
    print("*",animal["noise"],"*")
    print("*",animal["move"],"*")
    print("*",animal["cute"],"*")
\end{lstlisting}

This allows us to easily create animals, however, if we suppose we want to give animals a more complex behavior, as for
example for ecosystem simulation, representing all their characteristics as data can be hard. We also want to give them
\textit{actions}. Another problem of this dictionary representation is that it is a bit hard to see what data of an animal
means, because we have to look at the code of a function.

To overcome this, we can create classes and objects. A class describes a \textit{concept}. For example, the concept of an
animal, in our case, is an entity having a name, a noise, a movement, and a cute action. An object, also called \textit{instance} of a class,
is a concrete thing corresponding to the class. For example, a cat is an object of class animal. Classes can hold data, called \textit{fields},
but also functions, called \textit{methods}. Usually, classes feature a special method, the \textit{constructor}, which is used to create objects.


\begin{lstlisting}[language=Python, caption={Python class}]
#this is how we define a class in python
class MyClass :
#note that class has a body indented as function and controle structures do 
    static_field = 10 #defining static/class field
    
    @staticmethod  #defining static class method 
    def static_method() :
        print("static_field=", MyClass.static_field)

    #below is an alternative way of defining a static method 
    #the difference is that class is passed as the first parameter 
    #which can be cool class name is long and you have to use it 
    #a lot in the body
    @classmethod
    def static_method2(cls) :
        print("static_field=", cls.static_field)

    def member_method(self): #a method is member if it first argument is called "self"
        #self refers to the object
        print("member_field=", self.member_field, ", static_field=", MyClass.static_field)

    def __init__(self, value) : #defining constructor, named __init__, with an aditional "value" parameter
        self.member_field = value
\end{lstlisting}

You can note that we defined two different types of fields and methods : \textit{class} or \textit{static}, and \textit{object} or \textit{member}. Class
fields are the same for the entire class, meaning that their values are shared between instances. Class methods can be called without an instance. On the other hand,
member fields are local to each instance : their values differs from an object to another. Member methods need an object to be called. To access members or methods of a class
or an instance, most languages, including python, uses the dot operator \lstinline{MyClass.mymember}.

\begin{lstlisting}[language=Python, caption={Python class usage}]
#instancing a class into an object
my_object = MyClass(20) #note that self does not need to be passed as a parameter

#calling a class method 
MyClass.static_method() #prints static_field=10

MyClass.static_method2() #prints static_field=10

#accessing a class field 
print(MyClass.static_field) #prints 10

#accessing a member field
print(my_object.member_field) #prints 20

#calling a member method 
my_object.member_method() #again, self does not need to be passed as an argument,
                          #the dot operator is actually doing that for us
                          #prints member_field=20, static_field=10
\end{lstlisting}

At this point, we can change the way we represent animals in our code to use a class. It will heavily resemble the dictionary representations and yet
will be a lot clearer.

\begin{lstlisting}[language=Python, caption={Animals as a class}]
class Animal : 
    #how we create an animal
    def __init__(self, name, noise, movement, cuteness) :
        self.name = name
        self.noise = noise 
        self.movement = movement 
        self.cuteness = cuteness

    #actions an animal can perform
    def introduce(self) : 
        print("This animal is a ", self.name)

    def make_noise(self) :
        print("*", self.noise,"*")

    def move(self) :
        print("*", self.movement, "*")

    def be_cute(self) :
        print("*", self.cuteness, "*")
        
    #also, how we display an animal 
    def display(self) : 
        self.introduce()
        self.make_noise()
        self.move()
        self.be_cute()
\end{lstlisting}

We now will list animals into a dictionary, as the example in appendix \ref{appendix:basic_full_code_dictionnary}, and adapt
our main code. We also need an intermediate step to extract animal names from the dictionary in order to display them to the
visitor when planning route.

\begin{lstlisting}[language=Python, caption={listing animals using classes}, label={lst:listing_animals_using_classes}]
#listing animals
def list_animals() :
    animals = {}
    animals["cat"] = Animal("cat", "meows", "runs with agility", "licks paws")
    animals["whale"] = Animal("whale", "ultrasonic noising", "swims", "geysering")
    animals["tiger"] = Animal("tiger", "roars", "heavy walking", "licks paws")
    animals["alpinechough"] = Animal("alpinechough", "craws", "flies with elegance", "shakes feathers")
    #adding an animal simply means adding one line here now
    return animals


#extracting animals names 
def list_animals_names(animals) :
    names = []
    for key in animals : #iteration over a dictionnary is done using keys
        names.append(key) #simply listing keys, easy !
    return names

#touring 
def take_tour(route, animals) : 
    for animal  in route : 
        animals[animal].display() #accessing the animal by its name and displaying it

#main code
animals = list_animals()
animals_names = list_animals_names(animals)
route = ask_route(animals_names) #this function did not change
take_tour(route, animals)
\end{lstlisting}

It surely feels a bit nicer, however, it can still be enhanced. When we look at listing \ref{lst:listing_animals_using_classes}, we can see two things :
All animals have a name, and some animals share common behavior : tiger and cat shares their paws licking as a cute action. We can improve our representation
using inheritance.

\subsection{Inheritance : animal factorization}
\label{subsec:inheritance}

Before getting into inheritance, we need to imagine how it would be in a much, much larger software. Let's say for example an ecosystem simulation. Animals
would have kind of an artificial intelligence, and some would even have complicated behavior, \textit{hard to model simply with data}. Even if all animals
would more or less share the same action set (getting food, mating, resting, protecting territory), it would become hard to describe such behavior with data:
we need to write code for that. We already have done it when writing method for actions of our animals. We will still be simply printing stuff in the
rest of this chapter, but please keep in mind that these printing are now representing potentially complex processing.

\begin{lstlisting}[language=Python, caption={Animal complex behavior}]
class Animal : 
    # ... (these ... show that the class has some non mentioned code)
    #actions an animal can perform
    def introduce(self) : 
        #was print("This animal is a ", self.name)
        #but could be extremely complicated behavior, taking 
        #environment and internal parameters as hunger in account
        #to make decisions
    # ...
\end{lstlisting}

Doing so, we may end up wanting to have a different method body for the same action of two animals, as cat and dog, and therefore declaring each animal type by a class on its own,
yet we want to avoid writing the same code twice. Looking at the following listing, we can see that cat and tiger have one behavior in common.

\begin{lstlisting}[language=Python, caption={One animal, one class}]
class Cat : 
    # ... 
    def introduce(self) : 
        #behavior of a cat
    def be_cute(self) :
        #complex paws licking
    # ...

class Tiger : 
    # ... 
    def introduce(self) : 
        #behavior of a tiger
    def be_cute(self) :
        #complex paws licking, same as cat
    # ...
\end{lstlisting}

We can either write complex paws licking code twice, once in Tiger, once in Cat, but if we want to change it later, we may forget to modify both of them. Inheritance
is here to solve this problem. Inheritance involves two classes : a \textit{base} class, also called \textit{mother} or \textit{parent} class, and a \textit{derived} class, also called
\textit{daughter} class. The derived class \textit{inherits} from the base class : it has all its fields and methods. In some languages, including Python, a class
can inherit from more than one class, but we will only talk about it later, in \ref{sec:architecture}. For now, consider it is not a good practice, since most problems
solved by multiple inheritance can be solved by composition, that we will introduce in section \ref{subsec:composition}. Using inheritance, we would have cat and tiger
share behavior :


\begin{lstlisting}[language=Python, caption={Inheritance to share behavior}]
#defining a base Feline class
class Feline : 
    # ...    
    def be_cute(self) :
        print("complex paws licking")
    
#this is the syntax to create a class Cat that derives (inherits) from Feline class
class Cat(Feline) : 
    # ...
    def introduce(self) :    
        print("This animal is a cat")

#this is the syntax to create a class Tiger that derives (inherits) from Feline class
class Tiger(Feline) : 
    # ...    
    def introduce(self) :    
        print("This animal is a tiger")

# main code 
cat = Cat()
tiger = Tiger()

tiger.be_cute() #prints complex paws licking
cat.be_cute() #prints complex paws licking

tiger.introduce() #prints This animal is a tiger
cat.introduce() #prints This animal is a cat
\end{lstlisting}

A derived class can redefine a method of its base class. This is useful to create default behavior in the base class and specific behavior in derived class.

\begin{lstlisting}[language=Python, caption={Redefining inherited behavior}]
#defining a base Feline class
class Feline : 
    # ...    
    def be_cute(self) :
        print("complex paws licking")
        
#this is the syntax to create a class Cat that derives (inherits) from Feline class
class Cat(Feline) : 
    # ...
    def introduce(self) :    
        print("This animal is a cat")

#lets create a special cat class that does not licks paws
class AngryCat(Cat) : #it inherits from Cat, which inherits from Feline
    # ...    
    #redefining cuteness 
    def be_cute(self) : 
        print("Angry paws licking")

#main code
angry_cat = AngryCat()
angry_cat.introduce() #prints this animal is a cat
angry_cat.be_cute() #prints Angry paws licking
\end{lstlisting}

When constructing a derived class, we may want to construct it's base class first. We can do that by calling the constructor of the base
class withing the body of the constructor of the derived class.

\begin{lstlisting}[language=Python, caption={Constructing base class from derived}]
#base class want's two parameters in its constructor
class Base : 
    def __init__(self, param1, param2): 
        p1 = param1;
        p2 = param2;
    def print(self):
        print(self.p1, ",", self.p2)

#derived class will take only one parameter to be constructed and 
#will use it to create its base part : 
class Derived(base)
    def __init__(self, param1) : 
        #calling the constructor of the base class, do not forget to pass self !
        Base.__init__(self, param1/2, param1*2)

#main code 
derived = Derived(10)
derived.print() #prints 5,20
\end{lstlisting}

It's time to rewrite animals.

\begin{lstlisting}[language=Python, caption={Rewriting animals with inheritance}]
#defining a base class Animal, with default behaviors
class Animal() : 
    def __init__(self) :
        pass #nothing to do
    def display(self) :
        self.introduce()
        self.make_noise()
        self.move()
        self.be_cute()
    def introduce(self) :
        print("This animal is a ", self.name) 
    def name(self) : 
        return "unknown specie"
    def be_cute(self) : 
        print("*does nothing*")
    def move(self) : 
        print("*does nothing*")
    def make_noise(self) :
        print("*stays silent*")

#feline class : feline licks their paws and that's cute
class Feline(Animal) : 
    def __init__(self) :
        pass #nothing to
    def be_cute(self) : 
        print("*licks paws*")

#specific feline
class Cat(Feline) : 
    def __init__(self) :   
        pass #nothing to do
    def name(self): #redefining name method to change name of the class
        return "cat" 
    def make_noise(self) : 
        print("*meows*")
    def move(self) : 
        print("*Runs and jumps with agility*")

class Whale(Animal) :
    def __init__(self) :   
        pass #nothing to do
    def name(self): #redefining name method to change name of the class
        return "whale" 
    def make_noise(self) : 
        print("*ultrasonic noises*")
    def move(self) : 
        print("*Swimms with powerful noises*")
    def be_cute(self) :
        print("*expels water from its back*")

# ... (other classes)
\end{lstlisting}

We now only have one last adaptation to make, which is the way we build the list of animals

\begin{lstlisting}[language=Python, caption={Inheritance animal listing}]
def list_animals() : 
    animals = {} #dictionnary
    animals["cat"] = Cat() #simply creating the object corresponding to the animal we want
    animals["whale"] = Whale()
    animals["tiger"] = Tiger()
    animals["alpinechough"] = AlpineChough()
    return animals
\end{lstlisting}

The full code of our zoo at this point can be found in appendix \ref{appendix:object_programming_full_code}.

You may have noticed that before inheriting, our dictionary of animals was full of only one type, \lstinline{Animal}, and
after implementing animals with inheritance, we ended up with a dictionary filled with different animal types, \lstinline{Cat},
\lstinline{Whale}, \lstinline{Tiger}, \lstinline{AlpineChough}. Yet we are able to display them using method \lstinline{display()}
as if they were all \lstinline{Animal}. This is because of Python's duck typing, which we will explain later in section \ref{subsec:polymorphism}, \nameref{subsec:polymorphism},
at the same time we discuss types.
