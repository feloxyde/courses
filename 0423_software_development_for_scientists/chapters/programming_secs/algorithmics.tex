\section{Algorithmics and performances}

Desktop computers have enough processing power to allow developers to not focus too much on performances
of the software and instead improve functionalities. However, some applications can require
extreme processing capabilities, or some hardware may be really limited. Both cases are frequent in science field,
and being aware of some notions of both low level programming and algorithmics can help improve
performances of software.

\subsection{Cost and complexity of algorithms}
When manipulating a large amount of data, we want to use algorithms as efficient as possible. An algorithm is a
sequence of instructions to solve a problem. To evaluate how efficient an algorithm is, we look at the number
of operations it performs, \textit{time} and how much memory \textit{space} it needs to solve a problem regarding the size of the input.
Algorithm \ref{alg:matrix_mult} is performing multiplication of two 2D square matrices of size $N$. $N$ is our input size here.

\begin{algorithm}
    \KwData{$A$, $B$, $C$ : 2D square matrices of same size $N$}
    \KwResult{$C$ is now equal to $A*B$}
    \For{$i$ \KwFrom $0$ \KwTo $N-1$}{
    \For{$j$ \KwFrom $0$ \KwTo $N-1$}{
    $s = 0$ \;
    \For{$k$ \KwFrom $0$ \KwTo $N-1$}{
        $s = s + A[k][i] * B[j][k]$ \;
    }
    $C[i][j] = {s}$ \;
    }
    }
    \caption{Naive 2D square matrix multiplication}
    \label{alg:matrix_mult}
\end{algorithm}

This algorithm does 3 for loops of $N$ iterations nested in each other, and therefore performs around $N^3$ operations before terminating. This is the complexity of the
algorithm, usually expressed through the big O notation $O(n^3)$, as an asymptotic upper bound, which means it is only relevant with large values of $N$. Also, it only
gives the magnitude of the growth of resource need regarding data, as all constants, even multiplicative, are neglected.
This algorithm doesn't have additional memory needs, so its space complexity is $O(1)$ : it is independent of the size of the input.

Understanding complexity is useful when selecting an algorithm from a library, as documentation usually specifies the complexity of each algorithm used, and it can give
us a fairly easy way to compare their performances, as for example $O(n^3)$ is nearly guaranteed to be slower than $O(n^2)$ when input is large. Complexity of an
algorithm may vary depending on the arrangement of the input.
Because of this, complexity is often expressed as \textit{best case}, \textit{average} and \textit{worst case}, which gives more precision on how the algorithm performs
in certain conditions.
Table \ref{tab:sorting_complexity} presents the time complexity of few sorting algorithms, reorganizing an array so elements are ordered.

\begin{table}
    \centering
    \begin{tabular}{l|c|c|c}
        \textbf{Name}                  & \textbf{Best case} & \textbf{Average} & \textbf{Worst case} \\
        \hline
        \textbf{Quicksort}             & $O(nlogn)$         & $O(nlogn)$       & $O(n^2)$            \\
        \textbf{Merge sort}            & $O(nlogn)$         & $O(nlogn)$       & $O(nlogn)$          \\
        \textbf{Radix sort}            & $O(n)$             & $O(n)$           & $O(n)$              \\
        \textbf{Optimized Bubble sort} & $O(n)$             & $O(n^2)$         & $O(n^2)$            \\
    \end{tabular}
    \caption{\label{tab:sorting_complexity}Sorting algorithms time complexity}
\end{table}

In most cases, optimized bubble sort will be slower than any other sorting algorithm presented here. However, it is trivial to implement,
and if the array is nearly sorted, as if elements are close to their real position, it can beat nearly any other sorting
algorithm. Radix sort is extremely fast, but it can only operate on elements that can be converted into integers to be sorted. Merge
sort and quicksort are well-balanced sort, as they have no constraining requirements and perform well in most cases.

It shows us that all there can be a lot of differently performing algorithms for a given task, and that there is no perfect algorithm :
it all depends on context. For example, quicksort is most of the time the default sorting algorithm found in libraries, as it
performs fairly fast and does not consume additional space. However, if we can guarantee that the array or list will always be nearly sorted,
bubble sort is probably a much better choice. Such situation can occur in physics simulation with slowly moving objects, using a \textit{sweep and prune}
approach\cite{sweep_prune_algorithm_github_wiki}.

\subsection{Data collection}
\label{subsec:Data_collections}
As we may want to work on large amount of data, we usually want to store them in \textit{collections}.
Collections are abstract concepts of data properties and what operation we want to perform on them. They include :
\begin{itemize}
    \item \textbf{List} : a sequence of elements keeping trace of their insertion order.
    \item \textbf{Map} : a collection of key-value pair where keys are unique.
    \item \textbf{Set} : a collection of unique elements, they can be seen as a map where values are key as well.
    \item \textbf{Stack} : a special list where we retrieve elements in the reverse order we added them.
    \item \textbf{Queue} : a special list where we retrieve elements in the same order we added them.
    \item \textbf{Priority queue} : a special list where we want to have access to max and min elements.
\end{itemize}

%#FIXME add figure for the difference
Their implementation are data \textit{structures}. We already have seen two structures in Python in this chapter :
list, which implements a list, and dictionary, implementing a map. Different methods can be chosen to implement a collection,
leading to different performances regarding operations we can perform on them. For example, a list can be either represented
as an array, which stores elements side by side in memory, as a forward linked list, where each element has a pointer to the next, or as
a doubly linked list, where each element has a pointer to the next and the previous.
Table \ref{tab:list_complexity} shows expected complexity of some common list operations regarding list implementations. We can see that
linked implementations are overall better performing in insertion and removal, while array implementation is overall better for accessing
elements. It comes from the fact that arrays, storing elements side by side in memory, allows to access an element : as we know the address
of the first element, to access second element we simply add 1 to the address of the first element. In our shelf metaphor from section \ref{subsec:ref_and_pointers},
it would mean that having an array starting at address A2, to access next element we need to access element at address A3.

\begin{table}
    \centering
    \begin{tabular}{l |c |c|c}
        \textbf{Operation}           & \textbf{array}     & \textbf{forward linked } & \textbf{doubly linked} \\
        \hline
        insertion at end             & $O(n)$, am. $O(1)$ & $O(n)$                   & $O(1)$                 \\
        insertion at beginning       & $O(n)$             & $O(1)$                   & $O(1)$                 \\
        insertion at known position  & $O(n)$             & $O(1)$                   & $O(1)$                 \\
        finding element              & $O(n)$             & $O(n)$                   & $O(n)$                 \\
        finding element, list sorted & $O(logn)$          & $O(n)$                   & $O(n)$                 \\
        random access                & $O(1)$             & $O(n)$                   & $O(n)$                 \\
        removing last                & $O(n)$, am. $O(1)$ & $O(n)$                   & $O(1)$                 \\
        removing first               & $O(n)$             & $O(1)$                   & $O(1)$                 \\
        removing at known position   & $O(n)$             & $O(1)$                   & $O(1)$                 \\
    \end{tabular}
    \caption{\label{tab:list_complexity}List operations complexity regarding implementation, \textit{n} is the number of elements, \textit{known} means we have a pointer to the element.  \textit{am.}, for \textit{amortized}, means that when performing a lot of the same operation, it will look like they have this complexity}
\end{table}

Knowing the existence of different data structures in the languages and libraries/modules/packages you work with is important, as it helps to select the one fitting your need
the most. It can usually be found in documentation.

\subsection{Recursion vs Iteration}
\label{subsec:recursion}

Most programming languages allows some entities as classes or functions to refer to themselves,
directly or indirectly, in their definition. This is called \textit{recursion}. Recursion can sometimes be really intuitive to use,
as seen in listing \ref{lst:composite_pattern} in which composite pattern allowed us to recursively define a folder architecture.
A lot of data structures and algorithms can be expressed recursively, as for example a list and a search algorithm.

%#FIXME need to check this listing
\begin{lstlisting}[language=Python, caption={Recursive list and search}]
#a list is an element and a sublist. The sublist is a list as well.
class List : 
    def __init__(self) :
        self.value = 0
        self.sublist = None #none allows us to tell sublist has "no value"
    #appending is recursive as well, it self calls 
    def append(self, value) :  
        if self.sublist == None : #if no sublist (we are on last element)
            lst = List()
            lst.value = value
            self.sublist = lst #we create the new element 
        else : 
            self.sublist.append(value) #we append to sublist
    #search algorithm, recursive, returns List corresponding to element, starting from the sublist element
    #it starts from sublist element so we can use first element as an empty one, as if it had no value
    def find(self, value) :
        if self.sublist == None : 
            return None #element has not been found
        else : 
            if self.sublist.value == value : #found
                return self.sublist 
            else :
                return self.sublist.find(value) #recursion occurs here

#Main code 
#creating a list : 
lst = List() #list is empty []
#appending element 
lst.append(1) #list has value [1]
lst.append(2) #list has value [1, 2]
lst.append(3) #list has value [1, 2, 3]
lst.append(4) #list has value [1, 2, 3, 4]

res = lst.find(3) #res is a list containing [3, 4]
priny(res.value) #prints 3
\end{lstlisting}

Recursion usually leads to lower performance than iteration, because of call stack. In a computer, there are two types\footnote{Actually, only one type used in two different manners.} of memory :
\textit{heap} and \textit{call stack}. Stack is, as its name indicate, a stack data structure. Call stack is necessary to allow recursion. When
we call a function, its parameters and local variable are piled on the stack, and when we return, they are unpiled, as seen on figure \ref{fig:callstack}. It allows
functions to be called multiple times recursively while guaranteeing that their variables are local.
A recursive algorithm can rapidly consume all memory of the stack since it eventually makes thousands or millions function calls.
This has to be kept in mind when using recursive approach.
Some recursive algorithms can be optimized by the compiler or interpreter to not use stack, with methods as \textit{tail call optimization}.
Whether compilers or interpreters performs such optimizations is usually well documented for major languages.

\begin{figure}
    \centering
    \includegraphics[width=0.9\linewidth]{resources/algorithmics/callstack.png}
    \caption{\label{fig:callstack}Changes on call stack during function call}
\end{figure}

\subsection{Programming related performance issues}
Some performance issues in software can come from how computers works, and from the fact that languages hides those underlying mechanisms.
First, not all operations in a language are equal, and same operations can have different cost from one language to another.
A function call is part of costly operations. It comes from the fact that when calling a function we have to move a lot of variables
on the stack, as we have seen in previous section, \nameref{subsec:recursion}. Even more costly operations are \textit{system calls}.
System calls are an abstraction made by the operating system to allow programs to interact with hardware and other programs within
the system. Opening a file is a system call. Printing something on terminal is a system call. Using a networking library to send
data over internet leads to system calls.
System calls can be really expensive because of a lot of security mechanisms, as well as the need for operating system to allow
programs to access those functionalities at the same time.
In order to improve performances, it is advised, \textit{but not always true}, to reduce the number of system calls we perform.
When reading from a file, it can mean reading the file once to load it in memory and then processing its data instead of
loading data during processing.

\begin{lstlisting}[language=Python, caption={Reducing system calls (invalid code)}]
#this syntax is pseudocode, not real python
#this function does a lot of system calls 
def lot_of_system_calls() : 
    file = #...
    file.open() #one syscall
    while(file.notFinished()) : 
        line = file.getline() #one syscall per iteration
        process_line(line)
    file.close() #one more syscall   
    
#this function does only few system calls
def lot_of_system_calls() : 
    file = #...
    file.open() #one syscall
    data = file.read() #two syscalls
    file.close() #three syscalls
    while(file.notFinished()) : 
        line = getline(data)
        process_line(line)  
\end{lstlisting}

%#FIXME check that.
Memory allocation can also lead to system calls depending on language used. A memory allocation is done when
reserving memory for use. It we reuse the shelf metaphor from section \ref{subsec:ref_and_pointers}, we
can picture the memory of the computer being an empty shelf, and allocation would be creation of a named box.
Allocation can be done by two different ways : on stack and on \textit{heap}. Allocations on stack are done
by functions, for local variables, arguments and return values, they are usually really fast. Heap allocations are done when creating variables
whose size can not be known at compile time. The size of a variable is the number of bytes it needs to encode its data. For example, an array of 7 integers will usually
use 28 bytes, as integers typically use 4 bytes each.
Languages offer either explicit or implicit choice between stack and heap allocations. C++ is explicit. Python is implicit, objects are usually allocated on heap, as well
as lists and dictionaries, but some primitives types as integers may be allocated on stack.

\begin{lstlisting}[language=C++, caption={C++ heap vs stack allocation}]
int function(int b) { //b will be allocated on stack, returned int as well
    int a; //a is allocated on stack 
    int* c; //the * means that c is a pointer to an int type value.
            // c, the pointer, is allocated on stack
    c = new int(0); //creating a int type value on heap, using "new" operator. the returned value 
                    //is a pointer to the value, which we store in c.
    delete c; //this is for memory management, see below in this section
    return 0; //return value is allocated on stack
}
\end{lstlisting}

Heap allocations are in most cases much slower than stack allocation, as a system call has to be performed. However, not using heap
can prove to be really hard, as it is rare to know the size of all variables we need at compile time. Therefore, reusing already
allocated objects instead of allocating new ones can be a wise choice, as for example by using the \textit{object pool pattern}.

\begin{lstlisting}[language=Python, caption={Python reuse versus reallocation}]
class Vector2D : 
    def __init__(self, x, y) : 
        self.x = x
        self.y = y

def realloc_object() : 
    vec = Vector2D(1, 2)
    #using vector 
    vec = Vector2D(2, 3) #by constructing object again, we perform a second allocation

def reuse_object() :
    vec = Vector2D(1, 2)
    #using vector 
    vec.x = 2 #instead of reconstructing another object, 
    vec.y = 3 #we simply change values of the fields
\end{lstlisting}

In order to use as few memory as possible, we need to be able to deallocate, or \textit{free}, memory when it is not needed anymore. If memory was a shelf, it would mean
removing a box from the shelf to tell this space can be used by another box. Stack memory is trivial to deallocate : it is deallocated when corresponding
function call returns. For heap memory, two approach are possible. \textit{Automatic} memory management, as in Python, keeps a track of whether memory allocated is
still in use or not and deallocates it if needed. They are different algorithms and methods for that, depending on languages, compilers and interpreters.
\textit{Manual} memory management, as in C++, lets the programmer explicitly decide when he does not need memory anymore.
Manual memory management can be extremely powerful when done the right way, as it will keep memory usage minimal. Automatic memory management is most times
slower and leads to higher memory consumption, but offers a safety issue, as memory leaks aren't possible. A memory leak happens when an object is allocated, and
every pointer and reference to it are lost : it cannot be used anymore but continues to exist, taking space for nothing. A memory leak on our shelf would be having a
box with an unknown owner which we don't know whether we can remove it or not.

\begin{lstlisting}[language=C++, caption={C++ memory leak}]
int memoryLeak() {
    int* c = new int(0); //allocating an int     
    return 0; 
    //when the function returns, c pointer will be removed from stack, 
    //the int value it points to will be lost : memory leak occurs
}

int noMemoryLeak() {
    int* c = new int(0); //allocating an int     
    delete c; //we deallocate int pointer by c : no memory leak
    return 0; 
}
\end{lstlisting}

A lot of manual memory management languages gives way for programmer to avoid memory leak with mechanisms in between automatic and manual memory management.
For example, C++ uses \textit{smart pointers}, as \textit{unique pointers}. A unique pointer is a pointer that automatically deallocates memory it points to
when it is deallocated itself.

\begin{lstlisting}[language=C++, caption={C++ unique pointer}]

int unique_pointer() {
    unique_ptr<int> pointer; //we declare an unique pointer to int, allocated on stack 
    pointer = make_unique<int>(0); //we allocate memory on heap for int and give address to our pointer
    return 0; 
    //when the function returns, pointer will be deallocated, and will automatically
    //deallocate the memory it points to : no memory leak occurs.
}
\end{lstlisting}

While automatic memory management offers a bit lower performances regarding speed and memory consumption, they offer a much simpler and straightforward way to program than
manual memory management. As memory management differs from one language to another, it is wise to learn how the languages we use handle memory management, would it be
automatic or manual, to adapt our programming style in order to get the best performance possible. The choice between automatic or manual memory management, as well
as the level of optimization is a choice between performance versus clarity and simplicity, and will be discussed in the next chapter, \nameref{chap:code_quality}.

%tradeoff clear code vs performance + policy