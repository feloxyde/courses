\section{Types}
\label{sec:Typing}

We quickly mentioned types previously, in section \ref{subsec:variables}, \nameref{subsec:variables}. A type, or data type,
defines what data represents, what operations can be performed on it and how they shall be performed. It gives \textit{meaning} to raw data.

\begin{lstlisting}[language=Python, caption={Two types with same raw data and different meaning}]
#both these types are composed of two floating point number, yet they do not represent the same thing
class Vector2D : #this is a vector used for coordinates
    def __init__(self) : 
        self.x = 1.0
        self.y = 2.0

    def dot_product(self) : 
        # ...

class MeanMedian : #this is a pair of a mean and a median value
    def __init__(self) : 
        self.mean = 1.0
        self.median = 2.0
    
    def mean_over_median : 
        return mean > median 

#despite these two types being composed of the same raw data, a pair of floats, they do not have the same meaning.
\end{lstlisting}

In this section, we will put aside our zoo project and take time to study how the implementation of types can
vary from one language to another. We will illustrate that by comparing Python and C++. No strong knowledge of
C++ is needed.

\subsection{Typing parameters}

The first characteristic of typing to look at in a language is whether it is \textit{static} or \textit{dynamic}. In
static typing, the type of variables is processed and checked at compile time. In dynamic typing, the type of a variable
is decided when a value is assigned to the variable, at runtime.

\begin{lstlisting}[language=Python, caption={Python is a dynamically typed language}]
a = 10 #here, a is an integer
b = 20.0 #here, b is a float
a = "astring" #here, a is now a string
\end{lstlisting}

\begin{lstlisting}[language=C++, caption={C++ is a statically typed language}]
//this is a C++ comment
int a = 1; //a is an integer, to declare it we precise its type
           // (int) before its name (a)
float b = 1.0; //b is a float
b = 10; //b is still a float
\end{lstlisting}

The second characteristic to look at whether the typing is \textit{strong} or \textit{weak}. This depends on how the language
handles \textit{casting}, which is the action of converting one type into another. We already
performed a casting in the previous listing.

\begin{lstlisting}[language=C++, caption={C++ type implicit casting}]
float b = 1.0; //b is a float 
b = 10; //b is a float but 10 is an integer value 
        //therefore, C++ will cast 10 into a float (10.0)
        //before assigning it to b.
\end{lstlisting}

This casting is said to be \textit{implicit}, because we didn't tell C++ to cast \lstinline{10} from \lstinline{integer} to \lstinline{float} and yet
the compiler decided by himself to perform the casting. The more a language will allow implicit casts, the weaker it's typing will be. On the other
hand, the more a language will make \textit{explicit} casts mandatory, the stronger it's typing will be. Most of modern languages, including C++ and Python,
can be considered as strongly typed.

\begin{lstlisting}[language=C++, caption={C++ type explicit casting}]
float b = 1.0; //b is a float 
b = static_cast<float>(10); //this is a way to perform an explicit cast in C++
\end{lstlisting}

Those two characteristics have a heavy impact on the performances and use of a language. Statically typed languages tends to lead to higher execution speed
than dynamically typed ones. Strongly typed languages makes it easier to produce reliable software in comparison to weakly typed languages. However, dynamic
typing and weak typing both help to develop faster, since their syntax is usually lighter.

\subsection{Compile-time type safety}

We can resume compile-time type safety by the ability of a language to detect programming errors at compile time, by relying solely on types used by the programmer.
In other word, it is the ability of the language to forbid programmer from using operations on the wrong type \textit{before} running the program.
Python, being dynamically typed, isn't a good example for compile-time type safety.

\begin{lstlisting}[language=Python, caption={Python compile-time type unsafe code}]
class A :
    #... 

class B : 
    #... 
    def b_method(self) : #this method is not present in A definition
        #...

def do_something_on_b(b_object):
    b_object.b_method()

#main code
a = A()
do_something_on_b(a) 

#the execution of this code produces an error at runtime, since
#a does not has a method called b_method()
\end{lstlisting}

Compile-time type unsafe code can lead to extremely hard to track errors, as they may occur randomly at runtime, and thus be detected
months after having been introduced into the software.

\begin{lstlisting}[language=C++, caption={C++ compile-time type safe code}] 
class A { //this is how we define a class in C++
    //...
}; 

class B {
    //... 
    void bMethod(){
        //...
    }//this is how we declare a method in C++
};

//please note that below, as we define the function, we need to state the type of 
//the parameter "object"
void doSomethingOnB(B object){ 
    object.bMethod();
}

//main code,
A a = A();
doSomethingOnB(a); //this code does not compile, A type is incompatible with type of the parameter
                   //of the function (B), the error is therefore detected before runtime.

\end{lstlisting}

Types and/or variables can also have \textit{mutability} restrictions, which means we may forbid to change their value.

\begin{lstlisting}[language=C++, caption={C++ immutability}] 
//this function has an immutable parameter, wich means that in the body of the function,
//you won't be allowed to change it's value
void myFunction(const int a) //"const" means a is immutable
{
    a = 10; //this won't compile, as we are not allowed to change the value of a.
}
\end{lstlisting}

Compile time type safety helps to detect and fix a large amount of errors before even running the program. There are some manners to
enforce type safety in dynamically typed languages, but they are out of the scope of this course. Please note that Python is \textit{type safe} but not \textit{compile-time type safe}, it
simply enforces type safety during execution, raising an error if a type happens to be misused. Proving a language to be fully type-safe, either at
compile time or at runtime, is close to impossible.

\subsection{Pointers and references}
\label{subsec:ref_and_pointers}

An important notion in programming is the difference between a variable holding a \textit{value} and a variable holding a \textit{pointer}, or \textit{reference}.
Until now in this course, we only have seen every variable as holding a value : in this case,
as variable was a named box in which we put the value in. To understand pointers, we can approach the way computer memory works by a box-themed metaphor :
a shelf, shown in figure \ref{fig:memory_shelf}. The memory of our computer is like shelf on which there are boxes. Each box has a reference, representing the rows and column number. To make it simpler,
let's say that columns are represented by a letter, and rows by a number, so \lstinline{A2} represents first column, third row. It is called the memory \textit{address}.
When we create a variable, we associate a name with an address.

\begin{figure}
    \centering
    \includegraphics{resources/programming/memory_shelf.png}
    \caption{\label{fig:memory_shelf}Memory seen as a shelf}
\end{figure}

\begin{lstlisting}[language=Python, caption={A variable is a name for an address}]
#when we do this,
a = 10 
#the compiler finds an unused box on the shelf, (address B22 for example)
#puts 10 in it,
#and remembers that "a" now refers to the box at address (B22)
\end{lstlisting}

But actually, the address of a box is data as well. So how about we remember the address of a box in another box ?
It can't be done in Python, but it would look like that :

\begin{lstlisting}[language=Python, caption={Python storing address of a box (invalid code)}]
#when we do this,
a = 10  #the compiler finds an unused box on the shelf, (address B22 for example), ...
c = address(a) #the compiler puts address B22 in c
\end{lstlisting}

In this case, \lstinline{c} can be called a reference to \lstinline{a}, or more accurately, a pointer to \lstinline{a} : the value held by \lstinline{c} is the address of another box, \lstinline{a},
as shown in figure \ref{fig:memory_shelf_pointer}.
Why would we actually do that ? It can be useful to save time actually, because boxes are not equal. Let's imagine we have a really, really heavy box, containing a lot of data.
Passing it to a function would cost a lot of resources to copy it. Instead, we can tell the function the address of the box, which is light. This is called passing a variable by reference, instead of passing it
by value, and it can't be done explicitly in Python, but it would look like following code.

\begin{figure}
    \centering
    \includegraphics{resources/programming/memory_shelf_pointer.png}
    \caption{\label{fig:memory_shelf_pointer}Box address in a box}
\end{figure}

\begin{lstlisting}[language=Python, caption={Python passing by reference (invalid code)}]
def my_function(data_address) : 
    read_data(data_address, subdata) #we only access what we need using subdata

#main code 
a = HeavyData()
my_function(address(a))
\end{lstlisting}

When a variable is passed by value to a function, it is copied, and copying heavy data can take time. Sending a pointer or reference can save that time.
It is like if, instead of bringing this big box full of document to your colleague for him to read it, you would tell him that the box is at \lstinline{A22} on the shelf.
If it is faster, why don't Python allows us to do that ? Because actually, Python does that most of the time. In dynamically typed languages, most variables we create are
actually references or pointers, and Python is no exception to the rule. Then, why don't every language do that ? There are two reasons for this.

%#FIXME add examples for this !
First is side effects\cite{wiki_side_effect}. When copying a variable before passing it to a function, you ensure that changes happening inside the function will not affect value outside it. This
is corrected in Python by setting some arguments of function as immutable.

Second is about performances. References are extremely fast to be passed, but before accessing the value we need to first read the address and then access the value box. This operation
is called dereferencing. To be short, in a computer, looking at a tiny element in a box, no matter it's weight and content, takes always the same time.
Let's compare two codes performing the same task, in both Python and C++.

\begin{lstlisting}[language=Python, caption={Python heavy dereferencing}]
#we create classes encased in each other
class D : 
def __init__(self):
    self.e = #something

class C : 
def __init__(self):
    self.b = D()

class B : 
def __init__(self):
        self.b = C()

class A : 
    def __init__(self):
        self.b = B()

#main code 
a = A()

g = a.b.c.d.e #this results in massive performance drawback !
\end{lstlisting}

%#FIXME include a schematics
In this code, when we create an object of type \lstinline{A}, it's sub part \lstinline{b} is actually a reference to a value of type \lstinline{B}, having itself a sub part
\lstinline{c} and so on. So, each sub part is in a different box. Therefore, accessing \lstinline{a.b.c.d.e}, even if written in one line, implies to first dereference \lstinline{a.b}, then read the
corresponding value to dereference \lstinline{a.b.c} and so on and finally leads to three dereferencing. It is more or less equivalent to that code :

\begin{lstlisting}[language=Python, caption={Python heavy dereferencing step by step}]
#this
g = a.b.c.d.e 
#is somehow equivalent to doing this 
a_b = a.b #dereferencing #1
a_b_c = a_b.c #dereferencing #2
g = a_b_c.d #dereferencing #3
\end{lstlisting}

In a language like C++ that allows handling variables by value, such an access would be done in a single operation.

\begin{lstlisting}[language=C++, caption={C++ no dereferencing (invalid code)}]
//we create classes encased in each other
class D {
    int e; //that's how we declare a member field of the class 
};

class C {
    D d;
};

class B{
    C c;
};

class A {
    B b;
};

//main code 
A a = A();
int z = 10;
int g = a.b.c.d.e //this is exactly equivalent to 
int y = z         //to this
\end{lstlisting}

The difference of performance comes from the fact that when encased by value, all sub parts of \lstinline{A} object, \lstinline{a}, are stored in the same box
as \lstinline{a}.

Choosing to use values instead of references isn't always possible, and performance drawback of references is often only noticeable in data intensive operations.
To compensate that, Python offers really efficient libraries, usually implemented in C, to handle large amount of data, as NumPy and SciPy.


\subsection{Polymorphism and genericity}
\label{subsec:polymorphism}

In section \ref{subsec:inheritance}, we saw that two different classes can have common behavior that can be factorized using inheritance. Sometimes, we want to be
able to handle a set of variables with different types as if they were all the same type. This feature is called \textit{polymorphism}, and can take place either at compile time or at runtime.
Since python is dynamically typed, it only implements runtime polymorphism, \textit{dynamic polymorphism}. The approach Python uses for polymorphism is called \textit{duck typing}.
It states that as long as a type supports an operation, then it can be handled with this operation like any other type having this operation.

\begin{lstlisting}[language=Python, caption={Python duck typing}]
#class A and B are unrelated classes defining the same operation
class A :
    def some_operation(self) :
        print("A")

class B : 
    def some_operation(self) :
        print("B")

#main code
list = [A(), B()]
for elem in list : 
    elem.some_operation()

#this program produces following output :
#A
#B
\end{lstlisting}

Duck typing is implemented in a lot of modern languages, even statically typed like Golang, because it is a bit more flexible and simple than inheritance-based polymorphism that we can find in C++.
In C++, it is impossible\footnote{actually, it's possible, but that's really, really hard and most of the time not the best solution} to create a list of mixed types like
\lstinline{list = [A(), B()]}. To do that in C++, we would need to have classes \lstinline{A} and \lstinline{B} have a common ancestor defining the operation we want to perform,
and make a list of this said ancestor.

\begin{lstlisting}[language=C++, caption={C++ inheritance based polymorphism (invalid code)}]
//THIS CODE WOULD BE INVALID IN C++, IT IS SIMPLIFIED TO BE EASIER TO UNDERSTAND
class Ancestor {
    void do_something()
    {
        //...
    }
};

class A : Ancestor { //this is how we tell that A inherits from ancestor
    void do_something()
    {
        cout << "A" << endl; //this is equivalent to print("A") in Python 
    }
}

class B : Ancestor { 
    void do_something()
    {
        cout << "B" << endl;
    }
}

//main code 
list<Ancestor> list = {A(), B()}; //this is a list of object of type ancestor, containig A and B instances

for (elem : list){ //equivalent of Python for elem in list
    elem.do_something()
}

//this code produces the following output 
//A
//B
\end{lstlisting}

In the last C++ example, we manipulated a list of elements of type \lstinline{Ancestor}. However, as \lstinline{A} and \lstinline{B} inherit from \lstinline{Ancestor},
we can consider them as such : any operation supported by \lstinline{Ancestor} will also be supported by \lstinline{A} and \lstinline{B}.

But C++, as a lot of statically typed languages, has more to offer through static polymorphism, taking the form of \textit{overloading} and \textit{genericity}. Overloading
allows us to determine which operation to use regarding the type of the parameters we pass to it.

\begin{lstlisting}[language=C++, caption={C++ overloading}, label={lst:cpp_overloading}]
//two functions, same name, not same parameters type
void do_something(int a){
    cout << "do_something(int)" << endl;
}

void do_something(float a) {
    cout << "do_something(float)" << endl;
}

//main code 
int i = 1;
float f = 1.0;
do_something(i);
do_something(f);

//this code produces the following output : 
//do_something(int)
//do_something(float)
\end{lstlisting}

Python forbids to declare two functions with the same name. C++ allows it, as long as their parameter list isn't the same : different number of parameters or
at least one parameter with different type. At compile type, the compiler then selects the right function to use according to types of variables passed as arguments.
Selecting different functions at compile time based on handled types is nice, but we can also do the opposite through genericity : implementing a function once for multiple types.
In C++, such generic function is called a \textit{templated} function.

\begin{lstlisting}[language=C++, caption={C++ templated function}]
template<typename T> //we declare function as a template on a type, T
T add (T a, T b){
    return a + b
}

//main code
float a = 10;
float b = 11;
float c = add<float>(a, b); //here , "add" takes two floats, adds them 
                            //and returns the result 
int i = 10;
int j = 1;
int k = add(i, j); // The type to use can also be deduced by the compiler.
                   // here, "add" takes two ints, adds them and returns the result
\end{lstlisting}

This can also be done for classes, as we have seen in listing \ref{lst:cpp_overloading} : \lstinline[language=C++]{list<Ancestor>}. C++'s templates are by
far out of the scope of this course. It is only important to know that such thing exists in certain languages, as it allows
writing less code while keeping the benefits of compile-time type safety. Also, duck typing in Python replaces efficiently C++ templates if we don't consider
compile-time type safety.

\begin{lstlisting}[language=Python, caption={Python duck typing vs C++ templates}]
def add(a, b) : 
    return a + b

a = 10.0 
b = 11.0
c = add(a, b) //valid

i = 10
j = 1
k = add(i, j) //valid
\end{lstlisting}


