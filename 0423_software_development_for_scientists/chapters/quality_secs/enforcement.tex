\section{Enforcement}
\label{sec:code_quality_tools_and_practices}

Producing high quality code requires a lot of discipline to remain consistent as much as to follow rules. Evaluating
code isn't an easy task either, as complexity and other metrics can be fairly hard to compute by hand.
Fortunately, there are tools and methods to helps us in this task. In this section, we will discuss how guidelines before and
more generally a high quality of code can be enforced.


\subsection{Automation}
\label{subsec:code_quality_automation}
Setting up a good, tool-assisted environment makes you lose time at first when starting you project. On the long run it
saves you an \textit{insane} amount of time and effort.
The most obvious tools that can assist you are static analyzers. We can consider a compiler or interpreter as a static analyzer, as it
performs checks on your code without running it first. Static analyzers are often called \textit{linters}, and can be found as a standalone tool, integrated into
an editor or IDE, or even through and online service acting on a git repository. They will usually work by producing a report and point
out possible weaknesses of your code, as for example a lack of documentation or an inconsistency of your code with some guidelines.

\begin{lstlisting}[language=Python, caption={Python linting example}]
def function1():
    a = 10
    b = 20
    print(a)
    print(b)
    print(a + b)
\end{lstlisting}

A common linter for Python is Pylint\cite{pylint_page}, that we can run, after installing it, with command \lstinline{pylint [filetocheck]}. Running Pylint on the previous
code example produces the following output, telling us that docstrings are missing and that our variables do not follow Python standard for naming.

\begin{lstlisting}[caption={Python linting result}]
************* Module filetocheck
testpylint.py:1:0: C0114: Missing module docstring (missing-module-docstring)
testpylint.py:1:0: C0116: Missing function or method docstring (missing-function-docstring)
testpylint.py:2:4: C0103: Variable name "a" doesn't conform to snake_case naming style (invalid-name)
testpylint.py:3:4: C0103: Variable name "b" doesn't conform to snake_case naming style (invalid-name)

------------------------------------------------------------------
Your code has been rated at 3.33/10
\end{lstlisting}

As previously said, some linters and analyzers are online and analyze a git, GitHub or GitLab repository on demand, for example when pushing a commit.
A lot of them, like proprietary tool CodeClimate's Quality\cite{codeclimate_page}, provide free services for open source projects\cite{codeclimate_pricing}.

Some linters are more advanced than others, or more specialized, like focusing on security or performance. As linters usually don't edit your code, you can
use more than once without worrying about conflicts.

Another exciting code quality tool are code formatters, also called beautifiers. Code formatters will parse your code and rearrange it to match certain rules. They help maintain a
code with a visual strong coherence without having to pay too much attention to it while writing. C++ can be formatted, for example, with clang-format\cite{clangformat_page}, using a configuration file or
a default style.

\begin{lstlisting}[language=C++, caption={C++ clang-format formatting}]
//this code can be auto formated
int function(){int a=1;int b=2;int c=3;cout<<a+b+c<<endl;}

//into this code 
int function(int a ) {
    int a = 1;
    int b = 2;
    int c = 3;
    cout << a + b + c << endl;
}
\end{lstlisting}

%#FIXME possible repeating , maybe reformulate ?
Code formatters don't change the syntactical construct of the program, only it's appearance. It is not advised to use more than one formatter software at once for a
file since the file will get edited. Also, because of this edition, formatters can create a lot of unwanted conflicts in git versioning if they aren't used frequently, therefore
it is strongly advised to configure the editor or IDE to run the auto formatter on the code automatically when saving,
It is also not advised to run them on the full code base at once unless nobody else is working on it.

Using automation tool and keeping config files local to the project is a good practice, as described in chapter \ref{chap:devlopment_environment}, \nameref{chap:devlopment_environment} : it will ensure people working on the project always work with
the same tools as you.

Setting up such tools helps to solve part of the subjective aspects of code quality by delegating the decisions to the computer and objective rules. However, some rules
can hardly be automated. Those are meant to be mentioned into a set of guidelines such as shown in this chapter section \ref{sec:code_quality_guideline}, \nameref{sec:code_quality_guideline}.
Referring to an already existing and widely used guideline set is even better.

\subsection{Code reviews}

Code review is a static analysis of the code, but performed by humans. In this section we will discuss how to conduct code reviews.
The general idea behind a code review is to verify properties of the code that are too complicated or ambiguous to be enforced by automatic processes. Such reviews
can still be assisted by automated tools. For example, a static analyzer can look for possible non-terminating recursion and then reviewers will decide which are non-terminating
or not.

\begin{lstlisting}[language=Python, caption={Python non terminating recursion example}]
#this function is a non-terminating recursion that will indefinitely call for itself
#it can cause a stack overflow
def non_terminating(number)
    non_terminating(number + 1) # self calling here
    print(number) #this statement will never be executed

#this function is a terminating recursion, as it calls itself only certain conditions
#that are guaranteed to be met at some point
def terminating(number)
    print(number)
    if number > 0 : 
        terminating(number  -1) # at some point number will reach 0
\end{lstlisting}

Code review can take a lot of shapes. Let's see what parameters we can modulate to have the perfect one fitting for our needs.

\paragraph{Reviewer's awareness :}
Reviewer awareness is how much a reviewer knows about the code before starting to review it. A highly aware reviewer can for example
be the writer of the code itself, or a close by colleague. A highly unaware reviewer can be a random person found on the internet.
A highly aware reviewer is likely to be biased toward to code as his mind is already oriented toward a certain idea of what the code should be.
On the other side, a highly unaware reviewer may miss details related to the context of the code, as architecture flaws.

\paragraph{Temporality :}
Reviews can occur at various temporality. The three main possibilities are :
\begin{itemize}
    \item When writing the code, as for example with pair programming.
    \item When code changes, for example when a new commit is pushed on the git repository.
    \item At an arbitrary moment, for example each three weeks, or at the end of each cycle.
\end{itemize}

\paragraph{Rigorous or freestyle :}
A rigorous style review focuses on a certain set of points that will methodically be examined, while a
freestyle review will look at the code freely without looking for something in particular. Rigorous review offers reliability if conducted with discipline,
while a freestyle review is more entertaining and flexible,
as it can detect non-predicted errors. Of course, those two are archetypes and an in between is possible : ensuring a set
of point is always checked, while still allowing for freestyle remarks.

\paragraph{Support :}
A code review can be held with a meeting room and a computer, by displaying the code and discussing it. It can also
take place on internet, for example in the thread of a merge request, with all reviewers being remote, or use dedicated reviewing tools.
There are pros and cons to all configurations, but the most important actor in the decision is you and colleagues. If you need flexibility,
doing remove and deferred reviews is a nice idea. If you prefer discussion, then having a direct contact between reviewers and eventually devs
may be more interesting.

%#FIXME add references for efficience of code reviews
\paragraph{}Code reviews are much more important that they may seem, especially if they are done regularly and early. Regularity allows reviewing only small piece code each time,
reducing tiredness and boredom that may loosen your proficiency as the review advances.
Reviewing the code as quickly as possible after it is declared to be a valid version help detect defects before the code is widely used, and thus minimize the need
for changes later on.

Like in any other development practice, code reviews have to be adapted to specific needs of the team and software, and the chosen method shall be open
to changes if need be.

%#FIXME maybe add a minimal example of a review ?
\subsection{Design and Refactoring in the cycle}
\label{subsec:design_and_refactor}

When writing a software or working on technologies, sometimes one may \textit{voluntarily} ignore
flaws instead of fixing them, because at that time it costs less to adapt new code than to fix the existing code. It is understandable as some minor flaws
can require to edit the entire code base, as for example if it affects the core of the software. Let's say it explicitly, this is a horrible, evil practice.
Over the time, minor flaws accumulate and end up being a real handicap in the development of the software. This is a vicious circle : as
flaws fixing is avoided and code adapted to those flaws is added, the effort required to fix them increases, and thus the temptation to ignore those
flaws strengthens. This is the hell of the technical debt. *Dark choirs play in the background*.

Note that a really local flaw, for example within the body of a function with no side effect, may not create technical debt since it will
not affect the rest of the code.

\begin{lstlisting}[language=Python, caption={Python code flaw example}]
#this function has a local flaw, as the while loop could be replaced 
#by a foreach loop for elem in array :
def sum(array):
    i = 0
    sum = 0
    while i < len(array) :
        sum += array[i]
    return sum

#changing that will not change the behavior or the way to call the function at all, so 
# this flaw will not create technical debt
\end{lstlisting}

Fight against technical debt happens by two ways that are complementary. The first one is prevention. We want to have design phases in which technical
debt can not happen before getting into a new objective.
For that, we consider everything done in this phase to be fully disposable, and we mentally prepare ourselves for that. Restarting
the phase from zero shall not make you afraid. While doing so, we explore different designs, study problems we encounter and try to predict other problems we may encounter
in the future. We only get out of thinking phase and start implementing our solution when we have a clear and fairly detailed overview of what we need to do to complete our objective,
in other words, when the implementation will be trivial for us. It isn't forbidden to code during this phase, but we need to be prepared to fully delete our code and start again
if needed. If the design seems too complicated to achieve, divide your objective in smaller ones as independent as possible and work on them once at a time.
It may be tempting to skip this phase and get straight into coding. It can be partly explained because when we design it is harder to see progression in comparison to
when we code, as design doesn't produce a functionality when it is done. To address that, you can do \textit{design reviews} where you share thoughts on you design with other people
as in a code review. It will both give you a feeling of accomplishment and also help to fix flaws of your design.

%#FIXME provide an example, and also diagram of the conception process, especially how objectives and subs can affect each other !

The second way is to fix remaining flaws short after they are detected, through a refactoring. Its purpose is
only to enhance software and code quality, and thus it has to be differentiated from functionalities implementation and bug fixes. The behavior of the software do not change during
a refactoring, only it's appearance and expression. It is advised to have a dedicated time for refactoring, during which the target code is locked and no behavior change is allowed,
in order to avoid conflicts that can happen. You can even consider locking your entire code base while refactoring in case you may underestimate the extent of changes.
It is important for refactoring to happen as soon as possible upon detection of the flaw to minimize the amount of changes needed to fix the flaw.
A refactoring phase can be opened by a design phase if the solution to the flaw isn't trivial. Testing also plays a
critical role in refactoring as it reduce the risks that we break the code without noticing.
And as luck would have it, next chapter is related to testing. Code reviews can be part of the refactoring.

Some flaws will appear naturally in your software, as introducing new functionalities or changing behavior can require design changes withing already existing code. In these cases,
it is extremely important to fix those flaws before starting producing new code.

Finally, a development cycle will have two main phases, one of implementation, and one of refactoring, both starting by a design step. Usually, when detecting a flaw during an
implementation phase, we
simply wait for the next refactoring phase to fix it, unless it is critical and stops the current implementation phase, as shown in figure \ref{fig:dev_cycle_quality}.

\begin{figure}
    \centering
    \includegraphics[width=0.8\linewidth]{resources/enforcement/dev_cycle_quality.png}
    \caption{\label{fig:dev_cycle_quality}Example of a two-phases development cycle}
\end{figure}