
\section{Motivations}

Bringing or keeping a software at high code quality can feel like a hassle. Therefore, before discussing code quality, it is important
to discuss \textit{why} we want it to be as high as possible.

\subsection{Understandability}
%sharing on forums etc
The reason that rules them all is understandability. It is a combination between readability and clarity.
Readability is inversely proportional to the amount of time and energy a computer or a human has to spend to formulate hypothesis on
the meaning of the code. Clarity is inversely proportional the number of \textit{reasonable} hypothesis we
can formulate. Of course, we want both readability and clarity to be as high as possible.

\begin{lstlisting}[language=Python, caption={Python, high readability, high clarity}]
def divide_and_add(a, b):
    return a / b + a

#the code is fairly short and simple and therefore has a high readability
#only one hypothesis is reasonable (we divide a by b and then add a), so it is clear
\end{lstlisting}

\begin{lstlisting}[language=Python, caption={Python, low readability, low clarity}]
def special_operation(number1, number2_nonzero):
    return operation2(operation1(number1, number2_nonzero), number1)

#the code is hard to read, containing nested function calls 
#and long identifiers as number2_nonzero, readability is low 
#it is hard to understand straight what this code actually does without
#knowing what are operation2 and operation1, which are a bit abstract names,
#and therefore its clarity is low as well.
\end{lstlisting}

Understandability isn't exactly the same looking from the perspective of a computer or the one of a human.

\subsection{Computer's perspective}

The top reader of your code is likely to be a compiler or a static analysis tool. We summarize them by calling them a \textit{computer} or \textit{compiler}.
A computer usually only see your code as a collection of directives expressed using the language's syntax and semantics. They can understand \textit{what} you
are doing, not \textit{why}. Listing \ref{lst:computer_reading_example} shows what a computer sees. Please remember it is only a metaphor to
picture how a computer sees the code, not how it is exactly processed and analyzed.

\begin{lstlisting}[language=Python, caption={Python computer's reading example}, label={lst:computer_reading_example}]
#comments shows what computer sees 

students = ["Marc", "Remy", "Maxime", "Sophia", "Elodie"] 
# create a list L and add 5 string values to it : 
# "Marc", "Remy", "Maxime", "Sophia", and "Elodie"

# print a string whose value is "here are students"
print("here are students")
#iterate over the list L
for student in students : 
    #print the each element of the list L
    print(student)
\end{lstlisting}

We can notice that variables and functions names have no meaning for a computer. The computer understands that we create a list, that we fill it with some string values,
that we then print a string and that finally we display each element of the list. However, he doesn't understand we are working on
a list of \textit{students}. Any meaning passed through elements the compiler can not understand, as names or comments, are not taken in account by
the compiler. It is therefore unable to detect and warn if those ship an incoherent meaning.

\begin{lstlisting}[language=Python, caption={Python computer's incoherence acceptance}]
def paint(wall):
    #paint stuff 

#this triggers an error, the computer warns that "paint" only has one
#argument while you called it with two :
paint(Wall(), Wall())

#however, this does not triggers a warning during analysis : 
#the computer does not understand that painting a student is a bit strange
paint(Student())
\end{lstlisting}

Computers only analyze your code based on non-ambiguous rules. Those rules are more or less the language's syntax and semantics definition. It is normal
to proceed like this since we don't want the compiler to guess what to do, otherwise programming would be much harder and software probably less reliable.

Finally, computers have virtually infinite processing power and don't get tired. They can analyze syntactical constructions
that would be nearly impossible for us humans to understand.

\begin{lstlisting}[language=Python, caption={Python easy for computers to read}]
#a computer do not struggles more to read this 
a = funct1()
b = funct2()
c = funct3(a + b, b)
res = funct4(a + c)

#than this 
res = funct4(funct1()+funct3(funct1()+funct2(),funct2()))
\end{lstlisting}

\subsection{Human's perspective}
\label{subsec:how_humans_read_code}

While it is fine for a computer to only understand \textit{what} to do when reading code, humans need to understand \textit{why} doing it as well. Even
worse, we need to have the \textit{why} to better understand the \textit{what}. This may come from the way our natural languages work, heavily using
context to give meaning. The sentence \textit{``If you are nasty I'll eat you tonight !''} carries a completely different meaning whether you say
it with a malicious smile to your lover, or with appetite to an apple. Please note that if you talk to apples, how humans read code should
maybe not be your main concern. Back to original topic, it is faster to understand some code if you know why it is doing things before
wondering what it does.

\begin{lstlisting}[language=Python, caption={Python contextual why}]
def hypothenuse(triangle):
    return sqrt( pow(dist(triangle.a, triangle.b)) + pow(dist(triangle.a, triangle.c)) )

# when reading the name of the function "hypothenuse", and the parameter, "triangle",
# you already expect finding something of the form sqrt(AB + AC)
# when reading the body of the function, you only confirm or refute several hypothesis 
# you made on what the code does

def something(param1, param2):
    return param2 / dist(param1.a, param1.b) * dist(param1.a, param1.c)

# when reading neutral names like something, param1 and param2, 
# you have to decipher the formula and then try to understand what exactly it does 
# to finally get the why. With the why first it becomes much simpler

def thales_ac(triangle1, ab):
    return ab / dist(triangle1.a, triangle1.b) * dist(triangle1.a, triangle1.c)
\end{lstlisting}

Like for natural languages, human \textit{learn} how to read code. As a metaphor, we can consider that the syntax and semantics of a programming language is a natural
language, while how each developer organize their code and name things is a dialect of this natural language. In any cases,
This implies that the more a human reads code with a certain paradigm, language, and presentation, the faster he will read similar code.

In the end, humans have a limited processing power, they can only remember and analyze a tiny chunk of code and context at once.
The more processing power they use, the faster they get tired.
%subjectivity and learning aspect
%also, they are opposite of computer, they need to understand the why first to better understand the what

\begin{lstlisting}[language=Python, caption={Python hard for human to read}]
#a human do not have hard time reading this 
a = funct1()
b = funct2()
c = funct3(a + b, b)
res = funct4(a + c)
    
#but they will struggle a bit reading this
res = funct4(funct1()+funct3(funct1()+funct2(),funct2()))
\end{lstlisting}

\subsection{Efficiency}

Code efficiency is about solving a problem with code as small and simple as possible. Size of the code is fairly simple to measure, it is the number
of lines, keywords and identifiers, or instructions of our code. Code complexity, or more precisely code \textit{cognitive complexity}, opposite of simplicity, \textit{not to be confused with algorithmic complexity},
is much more complicated to evaluate. We could define it as being the minimum amount of processing power needed to analyze and understand the code.
Efficient code is faster to write and read.

\begin{lstlisting}[language=Python, caption={Python efficient code example}]
# this code is fairly efficient : 
list = [1,2,3,4,5] 
for elem in list : 
    print(elem) 
print("end") 

#this code does the same but is less efficient
endstr = "end"
list = [1,2,3,4,5] 
i = 0 #endstr, list
while i < len(list) : 
    print(list[i]) 
print(endstr) 
\end{lstlisting}

Looking at the previous listing, we can try to measure its complexity. Let's consider that we read the code following the flow of the program, and then let's try to see
how much we need to remember to understand the code. In order to do that, we read the code backward and each time something is used, we remember it until we find its definition.

\begin{lstlisting}[language=Python, caption={Python code complexity calculation example}]
# this code is fairly efficient : 
list = [1,2,3,4,5]      #4 : nothing (found list !) 
for elem in list :      #3 : list (found elem ! )
    print(elem)         #2 : elem 
print("end")            #1 : nothing
    
#this code does the same but is less efficient
endstr = "end"          #7 : nothing (found endstr !)
list = [1,2,3,4,5]      #6 : endstr, (found list !)
i = 0 #endstr, list     #5 : endstr, list (found i !)
while i < len(list) :   #4 : endstr, list, i     
    print(list[i])      #3 : endstr, list, i
    i += 1              #2 : endstr, i
print(endstr)           #1 : endstr
\end{lstlisting}

We can evaluate the complexity of the efficient code to be 1, and the one of the inefficient version to be 3. Of course, this is one way of measuring complexity.
We could take also in account the distance between usage and definition, and therefore \lstinline{endstr} would be counted as 6 or 7, or include functions calls, as they require
to have knowledge of what the function does. Finally, we could have excluded usage of things that are really expressive, as \lstinline{endstr} could be sufficiently self-explanatory
to not have to see its definition to understand it. There are plenty more ways of measuring cognitive code complexity \cite{climate_complexity}.

Code efficiency has a subjective aspect, and better than having a pure definition, the important matter is to understand the idea behind it and remember those complexity and size concerns.

\subsection{Maintainability}
\label{subsec:maintainability}

We don't want our code to simply be easy to write and read, we also want it to be easy to edit.
That's what maintainability is about. It depends on the quality of
the code, architecture of the software, and tests.
Edition can happen for a lot of reasons : enhancing
functionality, extending the software, fixing a bug, ...

We can evaluate maintainability by looking roughly at two things. First, how much edition I need to change or add something ? Second, when editing, to what extent I may introduce errors
in my software ?

%#FIXME add a reminder of what an AABB is !, add a figure for that
Let's have a quick look at two different Axis Aligned Bounding Box implementations, listings \ref{lst:maintainability_v1}, \ref{lst:maintainability_v2}
and how they react to getting from 2D to 3D modeling. An AABB example is shown figure \ref{fig:aabb_illustration}.
In both cases the AABB will be represented by an origin and an end.

\begin{figure}
    \centering
    \includegraphics[width=0.3\textwidth]{resources/quality_motivations/AABB.png}
    \caption{\label{fig:aabb_illustration} A 2D Axis Aligned Bounding Box is simply a rectangle with edges parallel to axes}
\end{figure}

\begin{lstlisting}[language=Python, caption={Python maintainability example version 1}, label={lst:maintainability_v1}]
#first version, remember AABB = axis aligned bounding box
class AABB():   
    def __init__(self, begin, end) :
        self.begin = begin
        self.end = end

    def center(self) :
        return (begin + end) / 2

    def translate(move)  :
        self.begin += move
        self.end += move

#creation of an AABB 
aabb = AABB(Vec2D(1,2), Vec2D(-2, 3))
\end{lstlisting}

As this first version represents its origin and end by a vector class and that all operations inside the class are related
to the vector class, we could switch from 2D to 3D simply by passing compatible 3D vectors to the constructor : \lstinline{AABB(Vec3D(1,2,3), Vec3D(-2, 3, -4))}.
On the other hand, if we need to edit other parts of our code to change where the \lstinline{AABB} class is used, like a \lstinline{RigidBody} we may forget
to change the vectors from 2D to 3D there and end up with bugs difficult to track. One possible fix would be to check that both vectors have the
same dimension when building the box, or even to check for a fixed dimension (2 or 3).

\begin{lstlisting}[language=Python, caption={Python maintainability example version 2}, label={lst:maintainability_v2}]
#second version remember AABB = axis aligned bounding box
class AABB():
    def __init(self, x0, y0, x1, y1) :
        self.x0 = x0
        self.y0 = y0
        self.x1 = x1
        self.y1 = y1

    def center_x(self) : 
        return (x0 + x1) / 2

    def center_y(self) : 
        return (y0 + y1) / 2

    def translate(x, y):
        self.x0 += x
        self.y0 += y
        self.x1 += x
        self.y1 += y

#creation of an AABB 
aabb = AABB(1, 2, -2, 3)
\end{lstlisting}

This second version is a more straightforward implementation and encodes origin and end directly by coordinates values. It takes a lot of editing to go from 2D to 3D, as nearly
all the methods need to be changed. However, because we will change the constructor as well, by adding \lstinline{x2} and \lstinline{y2} parameters, any code making usage of our
\lstinline{AABB} class and that we forget to convert from 2D to 3D will trigger an error at compile time. An in between solution would therefore be to mix both implementations,
constructing the class using raw coordinates but representing the AABB using vectors.

\begin{lstlisting}[language=Python, caption={Python maintainability example version 3}, label={lst:maintainability_v3}]
#last version, remember AABB = axis aligned bounding box
class AABB():   
    def __init(self, x0, y0, x1, y1) :
        self.begin = Vec2D(x9, y0)
        self.end = Vec2D(x1, y1)
    
    def center(self) :
        return (begin + end) / 2
    
     def translate(move)  :
        self.begin += move
        self.end += move
    
#creation of an AABB 
aabb = AABB(1, 2, -2, 3)
\end{lstlisting}

Maintainability has no perfect solution, as each case needs a specific study and depends on how you can anticipate changes. Code quality plays a really important role,
since you will often have to get back into code you wrote months or even years ago with a minimum of wasted time and effort. A common example for such a change is adapting your
code to new technologies or libraries, either to improve performances or to fix a newly occurring bug.

\subsection{Reusability}

Science advances because we don't have to rediscover and reinvent everything all the time. We reuse the work of other people as the foundations or our own. Software development
is no different. Two different software usually share a lot of common parts, even more if they belong to the same application field. A physics simulation library is likely
to need a constraint solver at some point. A software used for simulation and experimentation probably needs to plot data in a way or another. Better not implementing twice something
you could implement only once. There are several reasons for us to prefer reusing over implementing again.

\begin{itemize}
    \item Reusing the software piece as-is, or with few adaptations, saves a lot of conception and programming time.
    \item Reusing only algorithms or architecture is also an option, saving conception time.
    \item Code we reuse is, supposedly, already tested and functional.
    \item Code we reuse is, supposedly, already documented, and colleagues may be familiar with it.
\end{itemize}

Reusability simply measures how easy it is to take a part of a software into another software, and we can model this process by two steps.

\paragraph{In the prediction step}, we try to evaluate how much the software piece fits our needs, decide to use it or not, and how we will use it. \textit{Is there something interesting in this software for us to reuse ?
    How much we have to adapt the piece of code for it to fit in our software ? How much we have to adapt our software for the piece of code to fit into it ?} The prediction
step is easier if the piece of code and its documentation are simple to understand.

\paragraph{In the execution step}, we reuse the software in our code, given the prediction step produced a positive conclusion. This step heavily depends on how accurate and complete
our understanding of the piece of code was in the prediction step. If for some reason we missed an important point in our prediction step, we may lose a lot of time, either in adaptation or because we have to give up the reuse midway.

The best case for reusability is to simply take a piece of code from one software and drop it in another software for reuse. This is one of the numerous reasons why people split their code
into packages or modules and create libraries.