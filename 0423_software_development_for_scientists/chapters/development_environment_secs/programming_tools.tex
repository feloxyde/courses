\section{Programming tools}

\subsection{Editor}

Source code is usually written in a plain text format, and therefore can be edited with any text editor. However, some editors specialize
in code edition. They offer advanced features for coding. There are a lot of powerful open source code editors, as \textit{Vim}\cite{vim_page},
\textit{Emacs}\cite{emacs_page}, or \textit{VSCode}\cite{vscode_page}\cite{vscode_github}\cite{vscodium_github}. Those editors are highly customizable and offer a lot of possibilities to increase the speed at which you
write code.

Syntax highlighting colors keywords, names and other elements according to their role.

Autocompletion provides you with suggestions when you start writing a word, demonstrated figure \ref{fig:tools_autocompletion}. This is extremely useful since in programming you need to have names
being written always the same way : with autocompletion, you don't need to remember everything perfectly. It also saves you a lot of time when writing long names
by allowing you to write the first few characters and simply select the right choice. Finally, it can expose you what you can write in a certain context.

\begin{figure}
    \centering
    \includegraphics[width=0.7\linewidth]{resources/programming_tools/autocomplete.png}
    \caption{\label{fig:tools_autocompletion}Autocompletion prompt}
\end{figure}

Auto-formatting will rearrange your code to have it look nicer. It will be discussed in section \ref{subsec:code_quality_automation}. It helps to keep code homogenous and readable with low efforts.

Code snippet management allows writing complicated or widely used code constructs with only a few keystrokes. It can be triggered either with shortcuts or alongside
autocompletion, as in figure \ref{fig:tools_snippet}. A well configured code snippet manager can drastically increase the speed at which you write code. It is especially convenient when coding in a language you
don't often use by acting as a syntax reminder.


\begin{figure}
    \centering
    \includegraphics[width=0.7\linewidth]{resources/programming_tools/snippet_before.png}
    \includegraphics[width=0.7\linewidth]{resources/programming_tools/snippet_after.png}
    \caption{\label{fig:tools_snippet}Code snippet}
\end{figure}


Line wrapping allows to limit the width of a line of code on screen to a certain number of characters, while still writing on a single line in the file.

Block collapsing, or code collapsing, hides certain parts of the code and display only a one line summary, making it easier to read and explore. It is displayed figure \ref{fig:tools_collapse}.

\begin{figure}
    \centering
    \includegraphics[width=0.45\linewidth]{resources/programming_tools/collapse_before.png}
    \includegraphics[width=0.45\linewidth]{resources/programming_tools/collapse_after.png}
    \caption{\label{fig:tools_collapse}Code or block collapsing}
\end{figure}

Code navigation helps to find all mention of a certain names through multiple source files.

Code refactoring allows you to change names of entities according to the structure of the code. It can be seen as an advanced \textit{find and replace} feature.

Code editors pack in a lot more features, as file tree or integrated terminal. One really appreciable feature are \textit{workspaces} and configurations alike. A workspace
retains the configuration of your work environment, allowing you to quickly switch from a project to another and to define configuration local to a project.
%#FIXME add figure for workspaces ?

\subsection{Analyzers and debuggers}
% static, dynamic and debuggers
Getting information on a program, either during execution or by looking at code, can be extremely difficult for humans. A program of few lines of code can translate into
thousands of machine instructions at runtime and manipulate gigabytes of data. Fortunately, there are software to assist us in this complicated task.

A \textit{debugger} is a tool to execute a program step by step, as for example by stopping at each line of code, to observe the state of the program and its data, or even to change values on the fly
during execution.
It allows a developer to explore what happens in the program during runtime, and is really convenient for fixing bugs as it names implies. There exists at least one
debugger for each popular languages, as \textit{Javascript},
\textit{C}, \textit{C++}, or \textit{Python}.

There are also automated tools that can gather data about your software. \textit{Static analyzers} act by looking at your code without running it. They can for example detect security issues,
bad practices, badly presented code, and suggest you changes. A good example of such tool are \textit{linters}, discussed later in section \ref{subsec:code_quality_automation},
who ensures your code respects certain coding rules in addition to rules set by the language. \textit{Runtime analyzers} observe the software at runtime, sometime using a
modified version of it to improve observation abilities. For example, \textit{profilers} will report information about performance, either speed or memory consumption,
and \textit{coverage tools}, covered in section \ref{subsec:testing_coverage}, tells you which parts of the code ran during test.

\subsection{Automation}
\label{subsec:programming_tools_automation}

Programming and software development both involve working with fairly complicated workflows. For example, in order to compile a program written in C, we usually need to compile
each C file separately into \textit{object files} and then link them together to form a software. The command line to compile a C file looks like
\lstinline{gcc -c file -o object --some-flag --some-other-flag --more-flag}. A C software can easily be composed of tens of files. It makes it nearly impossible to compile them by hand each time we want
to run the program, because trust me, we want to run it often. Such problems are common in programming. Fortunately, there are a lot of \textit{automation} tools available for us
to never bother too much about that. They are a common practice and are both easy to use and widely used.

The first automation tools you can think of are \textit{bash scripts}. This course assume you already know a bit about them. We will not discuss them further right now,
as they are not the best suited tool for your everyday programming tasks.

Automation tools oriented on programming are often referred to as \textit{build automation tools}, as they are designed to ease building and installing software. A really common
build automation tool is \textit{Make}\cite{wiki_make}. Make allows running commands in a certain order by defining dependencies between commands. The \lstinline{make} command will look in the current directory for a file named \lstinline{Makefile} or \lstinline{makefile} containing rules. A rule is a named set of commands with eventually dependencies,
and is expressed as follows.

\begin{lstlisting}[language=Bash, caption={Make rule syntax}]
rulename : dependencyname1 dependencyname2 ... 
    command1 
    command2
    command3
    ...
\end{lstlisting}

The name of the rule is called a \textit{target}. You can supply a target to the make command to build this specific target and its dependencies only. Let's have a quick example
by manipulating text files using make. Let's say we have a directory containing two files, \lstinline{file1.txt} and \lstinline{file2.txt}, as well as a bash script, \lstinline{create_file.sh},
which creates one more file, \lstinline{created_file.txt}, when invoked. We want to be able to perform three actions. First is calling the script. Second is copying all files, including
the one created by the script, in a
newly made directory. Last is simply getting back to initial condition. We can write a tiny makefile for it.

\begin{lstlisting}
call : 
    ./create_file.sh

copy : call 
    mkdir newdir
    cp file1.txt file2.txt created_file.txt newdir

clean :
    - rm newdir 
    - rm created_file.txt
\end{lstlisting}

A \lstinline{-} before a command tells make to not report if the command fails. \lstinline{make call} will call the script. \lstinline{make copy} will call the script if it hasn't already
been done and then create \lstinline{newdir} directory and copy files into it. \lstinline{make clean} will get back into original state by deleting \lstinline{newdir} and \lstinline{created_file.txt}.
Please note that invoking make command without any argument will select a default target, which is usually the first target declared in the file. In our case \lstinline{make} is alike \lstinline{make call}.

Make is capable of much more. Keep in mind that this quick presentation was an inaccurate vulgarization, and that it is highly advised to learn more about Make or other generalist automation tools
as they are very useful, even outside pure programming projects.

Writing efficient makefiles can be fairly difficult when the project reaches large scale, or when a high portability is needed. In this case, using specialized tools may be better.
Specialized build automation tools are usually tied to a specific language. As an example, \textit{CMake} is specialized in the C family languages as C and C++. CMake actually generates
makefiles, so it isn't a build automation tool strictly speaking. Yet, for the developer it makes no difference. When using multiple languages, one can use specialized automation tools for each
and synchronize them using a generalist tool as Make.

Automation is a key factor to be efficient. Taking the time to set a fully automated development environment is extremely important and will save you a lot of time in the future.
This matter will be discussed a bit more in chapter \ref{chap:testing}, \nameref{chap:testing}.

\subsection{Version control}
\label{subsec:version_control}

A version control software, also referred to as \textit{source code manager} or \textit{revision control}, is meant to help us developers manage the progression of our software. They allow tracing of all changes made in the software in form of a history,
getting back on an older state, and coordinate the work of multiple people working on the same sources. There are plenty of version control software, as \textit{Subversion}\cite{svn_page},
\textit{Git}\cite{git_page}, or \textit{Mercurial}\cite{mercurial_page}.  \textit{In my opinion, working on
    any software development project, even the tiniest ones, without using version control is foolish. I won't simply recommend you to use version control, as I would recommend using
    a good editor : version control is primordial.}
We will use Git to introduce version control, as it is widely used and extremely flexible.

Git usually works with a server storing files online. This way we can access and share them anytime. Having a server can also give more guarantees against failures than a local
computer. Git works with local directory when it comes to editing code, and can also work perfectly fine without server. We will use this property to demonstrate its use.
A project under Git is called a \textit{repository}. Creating a repository is extremely simple. Just create a new directory, change into it and call \lstinline{git init} command.
You could also do that in an already created, non-empty, directory.

\begin{lstlisting}[language=Bash, caption={Creating a new git repository}]
$ mkdir git_test 
$ cd git_test 
$ git init
Initialized empty Git repository in /home/felix/git_test/.git/
$ ls -la
drwxr-xr-x  3 felix felix 4096 24 sept. 13:11 .
drwx------ 78 felix felix 4096 24 sept. 13:12 ..
drwxr-xr-x  7 felix felix 4096 24 sept. 13:11 .git
\end{lstlisting}

We can see that it created a new hidden directory, \lstinline{.git}, at the root of our repository. This directory is used by git to store information. Let's check the \textit{status}
of our repository to see if there are changes made, using \lstinline{git status} command. Then let's create a file to see status change.

\begin{lstlisting}[language=Bash, caption={Git status}]
$ git status
On branch master

No commits yet

nothing to commit (create/copy files and use "git add" to track)
$ touch file1.txt 
$ git status
On branch master

No commits yet

Untracked files:
  (use "git add <file>..." to include in what will be committed)
	file1.txt

nothing added to commit but untracked files present (use "git add" to track)
\end{lstlisting}

We have now an untracked file. We can add it to the history by making a \textit{commit}. A commit is simply a change in the repository. We first need to declare
files we want to add to the commit using \lstinline{git add} command. \lstinline{git add -u} adds all files git already knows of, or \textit{tracked files}, that had an update.
\lstinline{git add -A} adds all files, both tracked and untracked, that had an update. \lstinline{git add filename} adds the file named \lstinline{filename}. Please note
that \lstinline{git add -A} and \lstinline{git add .} commands shall be used with care, as they are adding to git new files and can therefore introduce files you
didn't want, as for example build files.

\begin{lstlisting}[language=Bash, caption={Git add}]
$ git add -A
$ git status
On branch master

No commits yet

Changes to be committed:
  (use "git rm --cached <file>..." to unstage)
	new file:   file1.txt
\end{lstlisting}

Then we can add changes to history with \lstinline{git commit} command. Without any argument, this command will open a text editor for you to write a commit message. You
can directly supply a message using \lstinline{git commit -m "message"} command. In a commit, try to only focus on a single subject. Add only relevant files and write
precise commit messages.

\begin{lstlisting}[language=Bash, caption={Git commit}]
$ git commit -m "added file1.txt"
[master (root-commit) d92e102] added file1.txt
1 file changed, 0 insertions(+), 0 deletions(-)
create mode 100644 file1.txt
\end{lstlisting}

We can now consult history with command \lstinline{git log}.

\begin{lstlisting}[language=Bash, caption={Git log result}]
commit d92e102e179e12857cc51195bf9b21c830601e7b (HEAD -> master)
Author: Felix Bertoni <felix.bertoni987@gmail.com>
Date:   Thu Sep 24 13:30:34 2020 +0200

    added file1.txt
(END)
\end{lstlisting}

We can press \lstinline{q} to exit log display. Our git commit has been registered and its message is also displayed.

You may have noticed that \lstinline{branch master} or \lstinline{master} is often mentioned in the result of previous git commands. This is because master is the default
name of the main \textit{branch} in Git. For simplicity, we will continue to call it master in this course, however, this term is a bit controversial\cite{linux_master}\cite{linux_master_git}\cite{github_master}\cite{stxe_master}, and for your projects it may be better to rename the branch as \textit{main} or \textit{stable}.

But wait, what is a branch ? We usually imagine a history as being a timeline. Git sees it as a graph, with multiple histories running on parallel, that can be joined
together later on, or become completely independent. It can be used, for example, to maintain two versions of a project at the same time : one \textit{stable}, guaranteed
to work, and one \textit{experimental}, where new changes are introduced and tested. Let's try branches on our example project.

We first need to create a new branch using \lstinline{git checkout -b branchname} command. It will make a new history starting at the current repository state we are in.

\begin{lstlisting}[language=Bash, caption={Git checkout to a new branch}]
$ git checkout -b new_branch
Switched to a new branch 'new_branch'
\end{lstlisting}

Using \lstinline{git log}, we can see that history is the same as the \lstinline{master} branch. We can add some changes into it. Let's add some changes.

\begin{lstlisting}[language=Bash, caption={Git add new changes to new branch}]
$ touch file2.txt
$ echo "something" >> file2.txt
$ git add -A
$ git commit -m "added file2.txt with something inside"
[new_branch e9a0670] added file2.txt with something inside
1 file changed, 1 insertions(+)
create mode 100644 file2.txt
$ ls
file1.txt file2.txt
\end{lstlisting}

Figure \ref{fig:git_history_one} gives an overview of our git history at this point.
We can navigate through branches using command \lstinline{git checkout branch_name}. Let's see how \lstinline{master} is going.

\begin{figure}
    \centering
    \includegraphics[width=0.2\linewidth]{resources/programming_tools/git_history_1.png}
    \caption{\label{fig:git_history_one}Git history after branching}
\end{figure}


\begin{lstlisting}[language=Bash, caption={Git getting back to master}]
$ git checkout master
Switched to branch 'master'
$ ls 
file1.txt
\end{lstlisting}

We can see that \lstinline{master} kept its old state. To simulate concurrent working of two people, we can simply add a new branch that will
create and work on the same \lstinline{file2.txt} file as \lstinline{new_branch}.

\begin{lstlisting}[language=Bash, caption={Git creating one more branch}]
$ git checkout -b additional_branch 
Switched to a new branch 'additional_branch'
$ echo "smthelse" > file2.txt
$ git add -A
$ git commit -m "added file2.txt with something else inside"
[additional_branch aa76d01] added file2.txt with something else inside
1 file changed, 1 insertion(+)
create mode 100644 file2.txt
\end{lstlisting}


Our history now looks like figure ref \ref{fig:git_history_two}. As we are done in \lstinline{new_branch}, we want to propagate the changes into \lstinline{master}. For this, we need to
get back into \lstinline{master} and initiate merging using \lstinline{git merge branchname} command. This command will merge \lstinline{branchname} with the
branch we are currently in, called \textit{active} branch.

\begin{figure}
    \centering
    \includegraphics[width=0.3\linewidth]{resources/programming_tools/git_history_2.png}
    \caption{\label{fig:git_history_two}Git history after branching a second time}
\end{figure}

\begin{lstlisting}[language=Bash, caption={Git merging new\_branch into master}]
$ git checkout master
Switched to branch 'master'
$ git merge new_branch
Updating d92e102..e9a0670
Fast-forward
 file2.txt | 1
 1 file changed, 1 insertions(+), 0 deletions(-)
 create mode 100644 file2.txt
\end{lstlisting}

Figure \ref{fig:git_history_three} shows how our history is at this point. We can also confirm with \lstinline{git log} that \lstinline{master} is now in the same state as \lstinline{new_branch}. This
happened because merging was \textit{fast-forward}, meaning that \lstinline{master} was simply a snapshot of the past of \lstinline{new_branch}. But what happens if
they actually differ ? For this, let's now try to merge \lstinline{additional_branch} with \lstinline{master}.

\begin{figure}
    \centering
    \includegraphics[width=0.4\linewidth]{resources/programming_tools/git_history_3.png}
    \caption{\label{fig:git_history_three}Git history after merging new\_branch}
\end{figure}

\begin{lstlisting}[language=Bash, caption={Git merging additional\_branch into master}]
$ git merge additional_branch
CONFLICT (add/add): Merge conflict in file2.txt
Auto-merging file2.txt
Automatic merge failed; fix conflicts and then commit the result.
\end{lstlisting}

Git is now signaling us that an unresolvable conflict appeared during merge. It comes from the fact that both our history from \lstinline{new_branch} and \lstinline{additional_branch}
had \lstinline{file2.txt} added with different content. Git is able to automatically solve most conflicts, but sometimes it can't and therefore asks us to do it manually. If
we look at \lstinline{file2.txt}, we can see how git signaled us the unresolvable conflict.

\begin{lstlisting}[language=Bash, caption={Git merge conflict in file2.txt}]
$ vim file2.txt
1 <<<<<<< HEAD
2 something
3 =======
4 smthelse
5 >>>>>>> additional_branch    
\end{lstlisting}

Git edited the file for signaling the conflict. Line 2 is current state, present in \lstinline{master} and line 4 is incoming changes from \lstinline{additional_branch}. We have
to edit the file as we like and then commit.

\begin{lstlisting}[language=Bash, caption={Git conflict solution}]
$vim file2.txt 
1 something, smthelse
\end{lstlisting}

\begin{lstlisting}[language=Bash, caption={Git finishing merge}]
$ git add -u
$ git status 
On branch master
All conflicts fixed but you are still merging.
  (use "git commit" to conclude merge)

Changes to be committed:
	modified:   file2.txt
git commit -m "merged additional_branch into master [add reason here]"
\end{lstlisting}

Our history now looks like figure \ref{fig:git_history_four}. There are two more commands to know that are interesting, and are used to communicate with the server. The first one is \lstinline{git pull},
merging local active branch with corresponding branch on the server. The second one is \lstinline{git push}, updating branch on the server, by
merging it with corresponding local branch. Please note that most git configurations will prevent you to push without first pulling and solving conflicts.

\begin{figure}
    \centering
    \includegraphics[width=0.6\linewidth]{resources/programming_tools/git_history_4.png}
    \caption{\label{fig:git_history_four}Git history after merging additional\_branch}
\end{figure}


This demonstration was only a picture of Git's most basic abilities, and you will learn more about them while working on projects and encountering problems.

A typical organization for a git repository is show on figure \ref{fig:git_history_typical}. The stable branch can only receive fast-forward commits and shall always
be in a state at which the software is functional. The development branch is meant to merge
and test working branches before pushing into stable branch. Working branches are created from development branch and are meant to edit code
or bring new functionalities.

\begin{figure}
    \centering
    \includegraphics[width=\linewidth]{resources/programming_tools/git_history_typical.png}
    \caption{\label{fig:git_history_typical}Typical look of a git history}
\end{figure}


\subsection{IDE : Integrated Development Environment}

When a development software features both an editor and several other tools, as for example a debugger, build automation management, and version control, we call it an \textit{Integrated
    Development Environment}, or \textit{IDE}. IDEs are often focused on a language or a set of languages, and even if they can often be adapted to other languages,
they aren't as convenient as custom environments when dealing with unusual language combination in a project. As example, we can cite \textit{CodeBlocks}\cite{codeblocks_page} for C and C++, or \textit{Pycharm}\cite{pycharm_page}
for Python.

The choice between a custom environment or an IDE is not easy, as they both have strengths and weaknesses. Please note that following comparison is a \textit{general} comparison
and there exists exceptions.

IDEs are much simpler than custom environments to set up,
as their default configuration is already ready for development. They also have better support for project-wide operations, for example refactoring that we will discuss later
in section \ref{subsec:design_and_refactor}. It is because they provide a way to define a \textit{project} and include files in it, which is rarely the
case for standalone code editors that need to guess which files are to be treated by looking at build automation scripts. Because of this, you can expect an IDE features, as
for example autocompletion, to be of higher quality compared to an editor.
IDEs consume more resources than standalone editors, as they need to handle more tools at once, however this drastically varies and is rarely a bother.

As each IDE has its own way to manage projects, it can be bothersome to work on a project made with a certain IDE using another IDE or a standalone editor. Some editors,
as VSCode, Emacs of Vim, can be customized to work alike an IDE and provide nearly all functionalities you can expect on one. A custom environment also has the advantage
to teach you tools that you can easily reuse in other projects, even ones unrelated to software development. Finally, custom environment is easier to automate without
using editor, as for example for building the project on a distant machine, or to allow someone else to work on the project with different tools.
%#FIXME maybe needs a bit of clarification !

%explanatory, discuss vs "manual" environment
%also, agains IDE is polyvalence : they can not handle all languages eventually
%Some editors can be as good as IDE once configured
\subsection{Concerns when setting up your development environment}

There are few points left to think of when setting up your development environment. They are listed in this section.

\subsubsection{Exporting customization}

Heavily customized environment, using an IDE or not, makes it difficult to set up again from scratch. Let's say for example you changed a lot of shortcuts
in your editor. If you need to change your computer and then reinstall your editor, you will need to reconfigure them again, because you are used
to them already. If you installed several plugins, you may also need to reinstall them.

There are some ways to export customization in both IDE and editors, for example in the form of configuration files. You can eventually include these config
files in your project or store them elsewhere, as for example in another git repository. Of course, all automation scripts shall be included in your project
as well. Finally, try to customize only what you can export or if the customization is easy to redo.

\subsubsection{Operating system and desktop manager}

Your operating system, as well as your computer setup are also part of your development environment, and when configured properly they can improve your
development performance. For example, a tiling windows manager as \textit{i3wm} is really convenient to display windows side by side, as
your code editor and a documentation page. Setting up shortcuts in order to have quick access to a terminal or switching windows in an instant can save you
a lot of effort later on.

\subsubsection{Open source or Free software for environment}

Open source and free development software have not much to envy to proprietary ones. A fully free or open source development environment
has the advantage to be always available for anyone to use, and you will never run into license issues. However, it is important to note that some proprietary tools,
as \textit{IDEA Intellij}\cite{intellij_github} IDE family have special discounts or free access for students\cite{pycharm_free_students}, teachers and researchers, or even open source
bases. Except when explicitly designated as proprietary, all tools presented in this course have open source
or free software versions.

When setting up a project, it is advised to make sure it isn't tied to a specific software, especially proprietary software, in order to allow as much people as possible
to use it and participate in the development.

\subsubsection{Internet search and documentation}

A lot of free resources are available online, as documentation, tutorials or programming and software development forums. When you encounter a problem, for example with
a language syntax or an error from a library, it is highly likely that someone already encountered it, and thus that it has been solved and documented.

In case you ask for help on a forum, remember to read their conditions and guideline beforehand. Writing a help request by following the rules of the forum you post will
help people understanding it better and answering quickly.

\subsubsection{Use your keyboard}

Each time you want to reach your mouse or pointing device, you loose some time and you reduce your focus. Most actions you can perform in a code editor, a terminal or
any development tool do not require to use your mouse. Keyboard is much faster and efficient that mouse in a lot of cases.

\begin{itemize}
    \item Keyboard keystrokes are binary, you press or you don't, while mouse needs a bit of precision. You can press keys extremely fast, however being both fast
          and precise with a mouse isn't really easy.
    \item Constantly reaching for your mouse is tiring on the long run. Left-handed people have an advantage to do this because if the mouse is on the left of the keyboard,
          it is closer to the position of the hand on the keyboard. Right-handed people can compensate by using a small factor keyboard or ten-key-less keyboard.
    \item A lot of important actions requiring multiple clicks and menu navigation can be done with a single shortcut in a well-configured software.
\end{itemize}

If you see that you perform an action often, remember to check if you can set a keyboard shortcut for it or simply perform it only with keyboard. Same goes for long to perform actions.

\subsubsection{Physical environment}
%#FIXME move that outside on the additional knowledge chapter ?
The last thing to remember is that your physical environment, your desktop, your screen, your mouse and keyboard, your chair, and the room you are in are
part of your development environment. Inform yourself on ergonomics practices for working on a computer, which will depend on the time you spend on it each day,
don't forget to take breaks fairly often.

