\section{About programming}

\subsection{Software, library, framework ?}

In a software development context, we usually differentiate three entities that one can code.

A software is a set of instructions that a computer can understand and that is aimed to fulfill
a specific need for a user. We often use software as a
shortcut for \textit{application software}. An application is a software meant for a user to
carry a specific task, as for example image manipulation. A simulation script can be considered
as an application as well. Figure \ref{fig:software_overview} shows and example of interactions between
a user and an application software.

\begin{figure}
    \centering
    \includegraphics[width=0.3\linewidth]{resources/about_programming/software.png}
    \caption{\label{fig:software_overview}An application software and its user}
\end{figure}

When creating software, a programmer usually uses really common and abstracts treatments. For example, for
a physics simulation, you may need vector operations or a numeric solver. Libraries\cite{wiki_library} are a way for a developer
to provide other developers with tool to develop software of a common field, as figure \ref{fig:library_overview} shows. It is used to not reinvent the wheel too often.
If a software was a house, libraries could provide bricks and cement.

\begin{figure}
    \centering
    \includegraphics[width=0.5\linewidth]{resources/about_programming/library.png}
    \caption{\label{fig:library_overview}Libraries, bricks to build software}
\end{figure}

A framework\cite{wiki_framework} is a special kind of library, or sometimes an aggregate of several libraries. The difference between
a framework and a library is how they influence the structure of the code using them. A library can be seen as
a collection of tools that the programmer can use as he wants. A framework is more like a pre-made software
that the developer will enhance and configure to build an actual software, see figure \ref{fig:framework_overview}. A physics simulation framework
would feature an already working simulation in which we can add new kind of physics objects, or alter physics
rules as for example by adding new constraints. If a software was a house, a framework would be a prefabricated
house in which you can add window by yourself and for which you can change walls materials.

\begin{figure}
    \centering
    \includegraphics[width=0.8\linewidth]{resources/about_programming/framework.png}
    \caption{\label{fig:framework_overview}Frameworks, foundations for software}
\end{figure}

Libraries can be extremely simple to develop because they often don't require much coherence in the way the developer interacts with
them, allowing to create all elements separately. Software is a bit harder to conceive, since they require some kind of coherence.
However, as software have a fixed and clear goal, being coherent isn't difficult. Frameworks, on another hand, are really tricky
to design, as they require both coherence and flexibility : you have to provide the developer as much as pre-made stuff as possible while
allowing extension and modification of the behavior of the framework. Of course, this estimation isn't absolute, and
some libraries or software can be as difficult as frameworks to implement and vice versa.

In the rest of the course, except if explicitly said, \textit{software} and \textit{program} will refer to both software, framework and library.

\subsection{Language}
Programming is done in most cases by writing \textit{code}, also called \textit{source code} or \textit{sources}. Code is text written
according a certain set of rules and describing what the computer shall do. Rules and meaning of code is expressed through a \textit{programming language},
defining a syntax and a semantic\cite{wiki_language}. Syntax defines what constructs are valid in the language. For example, we would imagine a language describing simple
arithmetic operations as \lstinline{number op number}, where \lstinline{op} is either \lstinline{+},  \lstinline{-}, \lstinline{*} or \lstinline{/}.
Then \lstinline{10 + 10} would be valid, while \lstinline{chat + 10} and \lstinline{10 ++ 10} would not. Semantic gives each valid construct a meaning. As an example,
we would decide that \lstinline{20 + 70} means \textit{add 20 and 70}.

There exists hundreds of programming languages, each having unique syntax and semantic. Usually, languages are designed with a goal in mind, and follow several \textit{paradigms}\cite{wiki_paradigm},
sort of philosophies on how to express what the computer should do. Some languages are much more specialized than other, as for example \textit{Javascript} is meant mostly for
web applications. We'll discuss further about languages in the next chapter, \ref{chap:programming} \nameref{chap:programming}.

\subsection{Compiler and interpreter}
Once the code is written, we want to have a computer execute the instructions described.

There are two archetypes of a program execution\cite{medium_compilation_interpretation}. The first one is to use a \textit{compiler}. A compiler will translate our source code into \textit{machine code}, forming an executable
that can be executed straight by the processor, or \textit{CPU} for \textit{Central Processing Unit}. Compiled software tend to have high performance regarding execution speed and resource consumption, however, executables have
a low portability. Production of machine code is dependent on hardware and operating system, and thus an executable made for a certain system is unlikely to be able to run
on other systems. If we want to run our software on different systems, we have to compile it for each system separately. In order to overcome this problem, we can use an \textit{interpreter}.
An interpreter is a software, usually compiled, that reads code of a certain language and executes its instructions. It acts as a medium between the code and the processor to help portability :
as long as the interpreter can run on a system, then our software will run on it as well. As the interpreter acts as a medium between software and processor, interpreted language usually have
lower performance regarding resource consumption and execution speed.
Those two modalities are shown figure \ref{fig:compilation_interpretation}. Please note that languages are not compiler nor interpreted, as their use is simply to express what the computer
should do. However, as languages are often associated with either compilation or interpretation, they are called either compiled or interpreted. Therefore, we will consider \textit{C++}
to be compiled language and \textit{Python} to be interpreted, even if it is possible to interpret C++ code\cite{cpp_interpreter} or compile Python code\cite{speedup_python}. This shortcut is acceptable, since it is much
simpler to interpret Python and compile C++ than the opposite.

\begin{figure}
    \centering
    \includegraphics[width=0.5\linewidth]{resources/about_programming/compiler_interpreter.png}
    \caption{\label{fig:compilation_interpretation}Compilation or interpretation}
\end{figure}


Some languages, as \textit{Java} and \textit{Golang} try to get the better of both compiler and interpretation, producing highly portable and performant executables. Java is both a compiled and interpreted language, as it is compiled in an intermediate
form in between processor instructions and code called \textit{bytecode}, and then run on a virtual machine offering much more performance than straightforward interpreters. Golang
is compiled but allows to ship a lot of resources alongside the executables it produces, making them more portable than a majority of other executable compiled with from other languages.

%#FIXME figures for java and golang ?
Interpreted language's performances are improving drastically and the performance gap between compilation and interpretation is tight now. Some languages even provide mechanisms
to allow enhancing critical parts of the software. One of them is \textit{just in time} compilation, abbreviated \textit{JIT}, where the interpreter will actually compile some
parts of the program on the fly if it considers them to be critical. Another is to allow calling compiled libraries from compiled code, often written in \textit{C}, to have
the developer handle critical parts in a high performance language.

Interpreted languages offer also a lot more flexibility when it comes to allow user to extend the program, but this will not be discussed in this course as it is advanced feature.
We will come back a bit on interpreted and compiled notions during chapter \ref{chap:programming}. Please note that in the rest of this course, we will not make any difference between
compiler and interpreter, and use both words for both meaning.