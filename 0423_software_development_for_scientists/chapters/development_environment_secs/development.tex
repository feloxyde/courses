
\section{Development}
%differentiate development vs programming as an intro
Programming is the activity of creating programs. Development is the activity of creating programs from need study to distribution, including project management.
Development methodologies choice is an extremely complex matter, especially because of the client-developer relation management. We
can assume that such relations are rare in a science and research context. Scientists are often their own customers when writing
software, and when they are not, their development product is still targeted at other scientists. Because of that,
requirements of the software are usually fairly clear.
This course will later introduce a development method, \lstinline{test-driven development}, in section \ref{sec:testing_tdd}. However,
rather than presenting a set of development methods, this section will give some hints to conduct software development projects.

\subsection{Incremental approach}
\label{subsec:incremental_approach}
In any project, software development or not, it is advised to divide large tasks and goals into tiny steps. However, some software development practices may seem a bit
non-intuitive at first sight.

Let's consider two scenarios in which we develop the same application software for data visualization. It does roughly three things. First, loads data from several possible
file format and convert them in an abstract representation. Second, analyze abstracted data with a set of algorithms selected by the user in a list. Finally, display results
or save them in an image.

In the first scenario, shown figure \ref{fig:linear_plan}, we plan the development of the software as we plan building a car or a furniture : one component after another. This approach is of course
suitable but may pose some problems. The software is only functional the very end, so if for some reason we have to stop development, either because a step is impossible
to achieve or because the project is dropped, we will leave an unusable software and time spent on it would have been wasted. Since project isn't functional during most of
the development, progression is only shown through code and eventually tests. It may make it hard to see for developers and result in a loss of motivation. It will surely
be even harder to show to people external to the project, as for example an administration that needs to decide on your funding.

\begin{figure}
    \centering
    \includegraphics[width=0.7\linewidth]{resources/development/linear_plan.png}
    \caption{\label{fig:linear_plan}Linear plan. \textit{Done} means software is functional.}
\end{figure}

In the second scenario, show figure \ref{fig:iterative_plan}, we chose our steps in order to always have a partially functional software at their end. This is called an \textit{iterative approach}. It involves implementing all components at the
same time, little by little. Functionality of the software increases with each step, and program can already be used for basic tasks at the end of the first step. Developers
can clearly see achievements, and it is easier to share them with external people. In case the project stops, only the efforts put in the current step will be thrown away.
Finally, it makes adding new functionalities during the project much more natural, because it is acknowledged from start that any component of the software can be modified
at any time.

\begin{figure}
    \centering
    \includegraphics[width=0.7\linewidth]{resources/development/iterative_plan.png}
    \caption{\label{fig:iterative_plan}Iterative plan. \textit{Done} means software is functional.}
\end{figure}


It isn't always possible nor good to achieve functionality at the end of each step, as sometimes some core components may require a lot of development before being usable.
For example, a constraint based physics simulation engine will probably need few steps before it is even able to perform any simulation, even basic ones : vector math, geometry,
numeric solver, and constraint solver. Even code using the engine may need few steps before actually making use of it and displaying functionality. In that case, it could be
advised to make such components, as the physics engine, a library or a framework. This way the notion of functionality is looked at directly from a code perspective.

We will, in the rest of this section, refer to steps as \textit{iterations}.

\subsection{Expressing and splitting goal}

First, let's fix ourselves upper and lower bounds for an iteration duration. \textit{I would propose one week to four weeks}, in order to both allow fairly wide atomic
tasks but still guarantee a continuous functional improvement. Then, let's fix our target duration, of course in between bounds. Why not two weeks. Please keep in
mind that those values examples are a bit arbitrary, and are meant to be adapted, we'll discuss that in section \ref{subsec:evaluate_and_improve_your_method}.

In order to split and express our goal, we can use a Kanban board, shown figure \ref{fig:kanban_board_example}. It is a board made of several columns, each representing a state of the task, as for example
\textit{to do}, \textit{in progress} and \textit{done}. Tasks are represented by stickers, or \textit{cards}. Columns are usually sorted chronologically, and most columns
have an internal sticker ordering, the highest task in the columns having the higher priority. For Kanban boards, we can use a real board and stickers
or a software tool for this.
We'll do 5 columns, you can do more or less depending on the granularity you want.

\begin{figure}
    \centering
    \includegraphics[width=0.9\linewidth]{resources/development/kanban_board.png}
    \caption{\label{fig:kanban_board_example}Kanban board example.}
\end{figure}

\begin{itemize}
    \item \textbf{Ideas}, for all ideas, expressed in a really general manner. These are simply reminders to think about stuff.
    \item \textbf{Features} where we will move interesting ideas after thinking on them and defining clear requirements for the feature to be considered as implemented
          and functional.
    \item \textbf{To do} where we register tasks corresponding each to an iteration. An item from Features can be moved here straight, or split in subtasks eventually.
    \item \textbf{In progress}, we move tasks here when we finally choose to implement them in the current iteration.
    \item \textbf{Done}, we move tasks here once completed.
\end{itemize}

Please note there can be more than one task per iteration, if there are more than one developer working on the project or if some tasks are paused. One could eventually
add a Paused column, or move paused tasks back in the To do column if it makes it clearer.

In the first part of a project, we will add a lot of stuff in both Ideas and Features as it is expressing the initial need and idea motivating our software, and
then as the project advances, we will sometime add a bit more in them. Let's consider data visualization software from previous section, \ref{subsec:incremental_approach}.
At some point in the development, we have an idea : why not allowing people to supply their own analysis algorithms ? We therefore add a \textit{Allow user to supply his
    own custom algorithms} card in the idea column to remember the idea. When we consider the development is sufficiently advanced for the idea to be implemented.
We study possibilities and move the card to Features, after changing its description to \textit{Allow user to supply custom analysis algorithms through python plugins, loading
    them dynamically on demand}. Sooner or later, it will become the top priority feature, and we will move it into To do, while splitting it into two tasks : \textit{Implement algorithm API interface and load custom
    algorithms on startup} and \textit{Implement dynamic custom algorithm loader}.
When one becomes the top priority, we can simply move it into In progress and then Done once done. Figure \ref{fig:card_manip} summarizes those manipulations.
\begin{figure}
    \centering
    \includegraphics[width=0.9\linewidth]{resources/development/card_transformation_example.png}
    \caption{\label{fig:card_manip}Example of card movement through the board.}
\end{figure}

Some tools allow extremely refined expression of relations between cards, as for example stating that two tasks are related to the same feature card or that
one task is dependent on another task to be completed or chosen.

%#FIXME add a figure summarizing cycles ?

\subsection{Forge software}
%#FIXME maybe add some figures ?

A software forge is a tool offering both code management and project management possibilities. We will show \textit{Gitlab}\cite{gitlab_page}\cite{gitlab_repos} as an example, which has two versions, one
free software licensed, \textit{Gitlab Community Edition}\cite{gitlab_foss_repos}, the other proprietary and built upon community edition, \textit{Gitlab Entreprise Edition}\cite{gitlab_ce_or_ee}. Please note
that there are plenty of software with similar features, either free, as \textit{Gitea}\cite{gitea_page}\cite{gitea_github}, or proprietary, as \textit{GitHub}\cite{github_page}. GitLab is a web service software and
allows hosting projects online for free on their instance, or self-hosting an instance.

GitLab uses Git for version control, presented section \ref{subsec:version_control}, and adds a lot of convenient features around it.

\subsubsection{Permission management}
GitLab permits a really refined management of permissions related to the project\cite{gitlab_doc_permissions}. For example, you can choose who can push to a given branch,
see or interact with elements of the rest of this list.

\subsubsection{Merge requests}
Merging two branches may be a complicated task, requiring time and communication between developers of each branch to fix conflicts. Merge requests
are a way to initiate and drive a merge\cite{gitlab_doc_merge_requests}. A developer initiates a merge request in GitLab interface, stating source and destination branches.
After that, a new discussion thread is opened on the page of the project, where developers can review code and changes, comment them, and ask for further
modifications before allowing the merge. If automated verifications are configured for the project, the thread will display whether those verifications
pass or fail. Finally, when merge request is accepted, a simple merge is performed.

Merge requests are useful if you want to have a protected branch, as for example the stable branch, where you want to disallow pushing and merging without
review first. Merge requests are also extremely convenient for open source projects, allowing external people to propose improvements.

\subsubsection{Issues and boards}
Continuing into communication tools, issues are a way for any person to report bugs and ask for improvements\cite{gitlab_doc_issues}. They can be seen as Kanban cards somehow.
For each issue submitted, a discussion thread is created, where developers and other people can exchange on the matter. Developers can choose
to assign someone to treat the issue. A branch can be associated to an issue, and when the branch is merged, the issue is automatically marked as resolved.

Issues can then be managed by creating a kanban board for the project\cite{gitlab_doc_board}. Boards associated
to the project can also be displayed publicly as a roadmap.

\subsubsection{Wiki}
There is one wiki per project\cite{gitlab_doc_wiki}. A wiki is really convenient to display information that are too wide to be put in a readme but too recent or special to be put directly
in the doc, as for example a temporary fix for a bug.

It is possible to use the wiki as support for documentation. However, I would not recommend doing that in GitLab as wiki are stored in a separate repository, and therefore
are not distributed with the code upon cloning a project.

\subsubsection{Web editor}

GitLab has an integrated web editor, and even a small web IDE\cite{gitlab_doc_ide}. It can be really useful to edit a file on the fly, in particular non-code files as readme. \textit{In my opinion,
    it isn't suited for development in comparison to a real IDE or custom environment, especially because running the code you write is harder with the web IDE. However, for
    minor changes you are really confident with, it is a powerful tool.}

\subsection{Evaluate and improve your method}
\label{subsec:evaluate_and_improve_your_method}

%final summary here of an iteration
%tell there are a lot of structured methods
Keep in mind that we didn't see a development method but only tips for development and project management. There are plenty of good and efficient development methods, iterative
or not,
and there is no consensus on the best way of conducting a software development project. To find a method that suits your needs better, \textit{experiment}
in several projects and systematically evaluate yourself. It is advised to start from an already existing development method,
as Test Driven Development we'll see in section \ref{sec:testing_tdd}.

Then, write down the rules of your method. Try to be as precise and objective as possible. An objective rule
could for example be : \textit{Perform one commit per iteration. A commit shall express in first paragraph what has been achieved during iteration and difficulties encountered, in second
    paragraphs what shall be done for next iteration}. Such a rule is simple to verify. When writing a rule, write the motivation for it as well. Example :
\textit{With one commit per iteration we can track progression through git history. Expressing both achievement and difficulties helps compare work done to predictions.
    Having goals for next iterations means we only need to look at last commit to know where we are going in an iteration.}

With rules written down, it is easier to decide if the method is flawed or if you simply didn't follow it properly. Table \ref{tab:method_evaluation_questions}
may help you start your evaluation. It can of course be applied to a team. Try to keep as much as possible history of your actions during a project, it can
drastically simplify evaluation.
Iterative approach is really convenient for method evaluation and improvement : after each iteration, or few iterations, you can perform a quick evaluation. Whatever method
you choose, be sure to perform a deep evaluation after completion of each important goals, and as well at the success or failure of a project.

\begin{table}
    \centering
    \begin{tabular}{c | l}
        \textbf{Q number} & \textbf{Question}                                      \\
        \hline
        1                 & Am I following method properly ?                       \\
        \hline
        2                 & Am I happy with my current (short term past) actions ? \\
        \hline
        3                 & Is the project's progression satisfying ?              \\
    \end{tabular}
    \begin{tabular}{c | c | c | p {0.7\textwidth}}
        \textbf{Q1} & \textbf{Q2} & \textbf{Q3} & \textbf{Direction}                                                                                                                                                                      \\
        \hline
        n           & n           & n           & Try to follow the method before more investigation. If you can't, then change the method.                                                                                               \\
        \hline
        n           & n           & y           & Your current actions are performant. Try to fix them as a method. However, beware of motivation loss.                                                                                   \\
        \hline
        n           & y           & n           & Try to follow the method before more investigation.                                                                                                                                     \\
        \hline
        n           & y           & y           & Fix your current actions as the method.                                                                                                                                                 \\
        \hline
        y           & n           & n           & You have to change your method.                                                                                                                                                         \\
        \hline
        y           & n           & y           & Your current actions are performant. See if you can change details in the method to enjoy working with it.                                                                              \\
        \hline
        y           & y           & n           & First, see if your unsatisfying progression feeling doesn't come from a bias, as for example a difficult problem you face during current iterations. If not, try to change your method. \\
        \hline
        y           & y           & y           & Everything is fine, keep up the good work !                                                                                                                                             \\
    \end{tabular}
    \caption{\label{tab:method_evaluation_questions} Questions to orient method evaluation}
\end{table}
