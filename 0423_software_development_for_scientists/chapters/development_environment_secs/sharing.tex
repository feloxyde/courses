\section{Sharing}

You are likely to give access to your software to other people at some point, for them to use
it or to help them criticize and reproduce your results. In case of a software solely attached
to a paper, as a simulation script, not all the following points will apply.

\subsection{Ease search}

Try to make your software as simple as possible to find.

It all starts by properly picking a name for your software. An ideal name is not already used and easy to remember.
If someone looks for it in a search engine, we want it to come in the firsts results. It will not forcibly help
a discovery of your software, but it may help someone find your software again, for example after having heard of it in a
conference.
Try to find a name fitting following criterions.

\begin{itemize}
    \item Not too short, not too long, 3-5 syllables is good. This way it is easy to remember
    \item Not solely related to applications field. For example, avoid \textit{Asteroids} for an astronomy software.
    \item Not solely related to software field. Your software is a ... software.
    \item Simple spelling with none or few silent letters. \textit{Asteroids} is better than \textit{Ashteeeroids}.
          However, having an unusual spelling can be beneficial as long as it is heard when we pronounce the name or if
          it is an intuitive transformation as a simple wordplay. For
          example, \textit{Azteroids} is simple to write and pronounced differently than \textit{Asteroids}.
    \item Not solely made of widely used terms.
    \item If possible, not already used in any field.
\end{itemize}

Fitting these criterions isn't mandatory, there are simply advices.
If you host your software on online resources, be sure that it is referenced by its name and not a derivative as an acronym.
For example, if you host a software called \textit{Dataminion Asteroids} on GitLab, you want the name of your repository to
be something like \textit{dataminion\_asteroids} rather than \textit{DA}.

If the mean of distribution you use can hold more detailed information about your software, as for example project description
or readme on GitLab, provide a quick summary of its use. If your software is accessible to anybody, even non-scientists, it can be
nice to have an accessible formulation. Remember that scientists are not the only ones who may be interested in using your software.

\subsection{Ease installation and use}
%#FIXME split that into dependencies ?

Finding a software is something, installing and using it is something else. Failing or struggling to install a software or a library
is extremely frustrating. Fortunately, we can save some sweat to the user.

Write an installation guide, telling step by step the user how to install the software. \textit{Step by step} is not to be taken
lightly here. Try to be as precise as possible in your steps, and provide visuals of manipulations you go through. If you want user to enter
a command line to install a package, show the command line, or even several versions if it is system dependent. If you want to skip
some steps, as for example installing an external library, be sure to provide a link to an external tutorial on how to install it if possible.

Provide a list of dependencies, as needed libraries. If some dependencies are optional, meaning they are not required, or required for some secondary functionalities,
state clearly what they are used for. Manually resolving dependencies can be extremely difficult depending on the context. For example,
if the missing dependency is detected only after 3 hours of compilation.

Try to automate the installation process as much as you can. For example, if your software needs
a configuration file somewhere in the system, its creation can be automated. Ideally, you can provide a fully automated installer that will
completely install the software, eventually asking some options to the user. On Linux, such installation would be done through package managers.
However, as there are a lot of different package managers, it can be tedious to provide packages to a large amount of Linux distributions.
Another solution can be to use package manager from a language to ship your application, as for example \textit{Pip} for Python.

Finally, document your software. This will be discussed in another chapter, section \ref{subsec:code_quality_documentation}.


\subsection{Contribution guides}

%talk about chosing aing a cool name and unique for the software in order to find it easily. 
In case your software is open to contributions, meaning other can submit modifications and improvements, or to discussion, as bug reporting
or improvement suggestion, it can be good to document such processes.

A typical contribution guide mentions what kinds of changes are welcome and what are the requirements for them
to be accepted.
\begin{itemize}
    \item What parts of the software are open to modification or enhancement ? Example : \textit{Physics code is not to be modified, however graphical interfaces improvements are welcome.}
    \item Are there contributions that are \textit{wanted} ? Example : \textit{We are looking for more data processing algorithms !}
    \item Are there technical rules to follow when contributing ? Example : \textit{All C++ code shall be formatted using \lstinline{.clang_format} configuration at the root of the project.}
    \item Are there soft rules to follow when contributing ? Example : \textit{Any contribution shall be published under MIT license !}
\end{itemize}
It could also be advised to provide a development guide. A development guide provides flow of actions in order to set up development environment
as well as performing important actions, as for example building the software or running tests.

A bug reporting or discussion guide is meant to help user to produce easy to process reports and suggestions. For example, if your software
produces logs, a reporting guide may explain how to retrieve and share such logs. A suggestion guide can give a specific format for suggestions,
or restrain suggestions to a certain set of functionalities. Example : \textit{We are not accepting any demand of modification of Physics code.}

\subsection{Licensing}

This section is meant to give an overview of families of license you can put on a software and its sources. Especially, license will
define under which conditions users can edit and share the software, and which warranty you offer to the user. We can roughly
define three philosophies of licenses. All licenses have variants, and what is shown here is an approximation. Be sure to read licenses
before using them with your software or using a software using them.

\subsubsection{Proprietary}

A license restraining software to use and forbidding modification and redistribution. Please note that a proprietary licensed software
can still expose its sources. It simply is forbidden for user to modify them.

\subsubsection{Permissive}

A permissive license allows use, modification and redistribution of the software with only minimal restrictions. Such licenses tend to include
a warranty disclaimer stating that developer can not be considered for bugs or faults of the software, neither damages it can cause.
Permissive licenses do not require derivative work or modifications to be published under a particular license. It means that the
modified version of a software under a permissive license, as \textit{Apache License}\cite{license_apache}, can be published under any license, even proprietary.


\subsubsection{Copyleft}

A copyleft license allows use, modification and redistribution of the software, but requires derivative work or modifications to be published
under a particular license. Such licenses tend to include a warranty disclaimer as permissive licenses do. If a software uses a library
under a copyleft license, it is likely that it has to be published under the same license as well. They ensure that all work done using
the library or software is free software as well. One example of such licenses is the \textit{GNU GPL}\cite{license_gpl_front} family.

\begin{itemize}
    \item \textbf{GNU GPL}\cite{license_gpl_front}\cite{license_gplv3_guide}\cite{license_gplv3_text} license requires any derivative work or modification, in case it is published, to be published under the same license.
    \item \textbf{GNU AGPL}\cite{license_gpl_front}\cite{license_agpl_why}\cite{license_agpl_text} license requires any derivative work or modification, in case it is published or accessible through internet, to be published under the same license.
          It is well suited for a web application for example.
    \item \textbf{GNU LGPL}\cite{license_gpl_front}\cite{license_lgpl_question}\cite{license_lgpl_question} Offers a bit more permissive definition, allowing a non LGPL-licensed work, as a library, to be used in a LGPL-licensed library under certain conditions. However,
          modifications of the library and derivative works still need to be published under LGPL or GPL licenses.
\end{itemize}
