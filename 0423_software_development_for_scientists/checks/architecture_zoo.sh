#!/usr/bin/expect -f

spawn python ../examples/programming/architecture_zoo/zoo.py 

expect {
    "^animals :  cat, tiger, whale, alpinechough, fishingcat" {}
    default { exit 1 }
}
send "y\r"
expect {
    "Available noises : crawing, ultrasonic_noises, roaring, meowing" {}
    default { exit 1 }
} 
send "crawing\r"
expect {
    "Available movements : agile_flight, powerful_swimming, powerful_run, agile_run" {}
    default { exit 1 }
}
send "agile_flight\r"
expect {
    "Available cutenesses : paws_licking, geysering, feather_shaking" {}
    default { exit 1 }
}
send "paws_licking\r"
expect {
    "please name you animal" {}
    default { exit 1 }
}
send "tigercraw\r"
expect {
    "Do you want to create another" {}
    default { exit 1}
}
send "n\r"
expect {
    "please list animals you want to see in order, separated by a comma" {}
    default { exit 1 }
}
send "tigercraw, cat\r"
expect {
    "This animal is a tigercraw" {}
    default { exit 1}
}