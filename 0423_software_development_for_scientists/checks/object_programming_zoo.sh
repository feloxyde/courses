#!/usr/bin/expect -f

spawn python ../examples/programming/object_programming_zoo/zoo.py 

expect {
    "^please list animals you want to see in order, separated by a comma" {}
    default { exit 1 }
}
send "cat, tiger, whale\r"
expect {
    "^This animal is a cat" 
    default { exit 1 }
} 