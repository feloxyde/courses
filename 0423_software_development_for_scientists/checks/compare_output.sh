#!/usr/bin/bash
#$1 is name of script/expected couple
src="./${1}.sh" 
res="${1}.res"
exp="${1}.exp"

run_cmd="${src} > ${res}"
check_cmd="diff ${res} ${exp}"
clean_cmd="rm ${res}"

eval $run_cmd
eval $check_cmd
r=$?
eval $clean_cmd 
exit $r

