# Composition over inheritance

## Introduction

## Contents

## Prerequisites

# Introduction

## Need for flexibility

## Class inheritance vs interface implementation

# Inheritance

## Strenghts

## Multiple inheritance problem

## inheritance fishnet problem

# Composition

## Principle

## Comparison to inheritance

## Limits

## ECS pattern