function enable_menu_actions() {
  var navtoggle = document.getElementById("main-nav-toggle");
  navtoggle.onclick = function () {
    if (document.getElementById("menu").classList.contains("show")) {
      document.getElementById("menu").classList.remove("show");
      document.getElementById("main-nav-toggle").innerHTML = "+"
    } else {
      document.getElementById("menu").classList.add("show");
      document.getElementById("main-nav-toggle").innerHTML = "&#215;"
    }
  }
}