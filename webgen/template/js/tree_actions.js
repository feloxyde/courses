function toggleDir (dir){
    if (dir.classList.contains("collapse")) {
        dir.classList.remove("collapse");
        var subs = dir.querySelectorAll(".dir");
        subs.forEach((sub) => {
            sub.classList.add("collapse");
        });
    } else {
        dir.classList.add("collapse");
    }
}

function enable_tree_actions() {
    var dirtitles = document.querySelectorAll(".tree .dir > h3");
    dirtitles.forEach((title) => {
        title.parentNode.classList.add("collapse");
        title.addEventListener("click", function (ev) {
            toggleDir(ev.target.parentNode);
        });
    });

    var dirfa  = document.querySelectorAll(".tree .dir > h3 > .fa");
    dirfa.forEach((fa) => {
        fa.addEventListener("click", function (ev) {
            toggleDir(ev.target.parentNode.parentNode);
        });
    });
}

