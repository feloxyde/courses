from yattag import Doc

doc, tag, text = Doc().tagtext()

giturl = "https://gitlab.com/feloxyde/courses/-/tree/master/"
gitlab_buildpath = "/builds/feloxyde/courses/public/"


def course_code(course):
    with tag("div", klass="code"):
        with tag("span"):
            text(course.code)


def course_tags(course):
    with tag("div", klass="tags"):
        for t in course.meta.tags:
            with tag("span"):
                text(t)


def course_formats(course):
    with tag("div", klass="formats"):
        for f in course.meta.formats:
            with tag("span"):
                text(f)


def file_icon():
    with tag("i", klass="fa fa-file-o"):
        pass


def file_element(file_data):
    with tag("div", klass="file"):
        file_icon()
        with tag("a", href=file_data.path.replace(gitlab_buildpath, "")):
            text(file_data.name)


def directory_icons():
    with tag("i", klass="fa fa-folder-open-o"):
        pass
    with tag("i", klass="fa fa-folder-o"):
        pass


def directory_element(dir_data):
    with tag("div", klass="dir"):
        with tag("h3"):
            directory_icons()
            text(dir_data.name)
        with tag("div", klass="content"):
            for fd in dir_data.subs:
                if fd.is_folder():
                    directory_element(fd)
                else:
                    file_element(fd)


def file_structure(course):
    with tag("div", klass="dir-root"):
        with tag("h3"):
            text("Contents")
        for fd in course.contents.subs:
            if fd.is_folder():
                directory_element(fd)
            else:
                file_element(fd)


def course_summary(course):
    with tag("div", klass="summary"):
        with tag("h1"):
            text(course.meta.name)
        with tag("div", klass="informations"):
            course_code(course)
            course_tags(course)
            course_formats(course)
        with tag("div", klass="description"):
            text(course.meta.shortd)
        with tag("div", klass="showmore"):
            with tag("a", href=course.code + ".html"):
                text("show more")


def course_details(course):
    with tag("div", klass="details"):
        with tag("h1"):
            text(course.meta.name)
        with tag("div", klass="informations"):
            course_code(course)
            course_tags(course)
            course_formats(course)
        with tag("div", klass="controls"):
            for l in course.meta.links:
                with tag("a", klass="download", href=(course.folder + "/" + l["link"])):
                    text(l["name"])
            with tag("a", klass="download", href=(course.folder + ".tar.gz")):
                text("download")
            with tag("a", klass="gitlab", href=(giturl+course.folder)):
                text("gitlab")
        with tag("div", klass="description"):
            text(course.meta.description)
        with tag("div", klass="tree"):
            file_structure(course)


def course_element(course):
    print("generating element :", course.folder)
    classes = "course status-"+course.meta.status
    with tag("article", klass=classes):
        course_summary(course)


def courses_list_start():
    doc.asis('<section id="available">')


def courses_list_wip():
    doc.asis('</section>')
    doc.asis('<h1>Work in progress</h1>')
    doc.asis('<section id="work-in-progress">')


def courses_list_end():
    doc.asis('</section>')


def doc_value():
    return doc.getvalue()


def reset_doc():
    global doc
    global tag
    global text
    doc, tag, text = Doc().tagtext()
