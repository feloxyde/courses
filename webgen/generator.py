import os
import json
import sys
import colorama
from colorama import Fore, Style
from course_html import course_element, doc_value, courses_list_end, courses_list_start, courses_list_wip, reset_doc, course_details


# FIXME check that all courses have a different code
# FIXME chack that all courses have a correctly named archive

# constants


# init libs

colorama.init()

# helper defs

indent = ""
indentcount = 0
failed = False


def gen_ident():
    global indent
    indent = ""
    for i in range(0, indentcount):
        indent += "\t"


def info(text):
    print(indent + Fore.BLUE + text + Style.RESET_ALL)


def passing(text):
    print(indent + Fore.GREEN + text + Style.RESET_ALL)


def error(text):
    global failed
    print(indent + Fore.RED + text + Style.RESET_ALL)
    failed = True


def pushi():
    global indentcount
    indentcount += 1
    gen_ident()


def popi():
    global indentcount
    indentcount -= 1
    gen_ident()

# END HELPER


class Meta():
    def __init__(self, status, name, tags, formats, description, shortd, links):
        self.status = status
        self.name = name
        self.tags = tags
        self.formats = formats
        self.description = description
        self.shortd = shortd
        self.links = links

    def display(self):
        print("meta {", self.status, ", ", self.name, ", ", self.tags, ", ",
              self.formats, ", ", self.description, ", ", self.shortd, ", ", self.links, "}")


class File():
    def __init__(self, path, filename):
        self.name = filename
        self.path = path

    def display(self, path):
        print(path + "/" + self.name + " (file)")

    def is_folder(self):
        return False


class Folder():
    def __init__(self, foldername, subs):
        self.name = foldername
        self.subs = subs

    def display(self, path):
        print(path + "/" + self.name + " (dir)")
        for sub in self.subs:
            sub.display(path+"/"+self.name)

    def is_folder(self):
        return True


class FilesTree():
    def __init__(self, subs):
        self.subs = subs

    def display(self):
        print("contents : ")
        for sub in self.subs:
            sub.display("")


class Course ():
    def __init__(self, folder, meta, contents):
        self.folder = folder
        self.meta = meta
        self.contents = contents
        self.code = folder.split("_")[0]
        # need to check for achive !


def meta_status(meta):
    if("status" not in meta):
        error("meta is missing status")
        return None
    else:
        if meta["status"] != "available" and meta["status"] != "work-in-progress":
            error("status meta shall be either available or work-in-progress")
            return None
        else:
            return meta["status"]


def folder_to_name(folder):
    return " ".join(folder.split("_").pop(0))


def meta_name(meta, folder):
    if("name" not in meta):
        return folder_to_name(folder)
    else:
        return meta["name"]


def meta_tags(meta):
    if("tags" not in meta):
        error("meta should have tags !")
        return None
    else:
        if not type(meta["tags"]) is list or len(meta["tags"]) < 1:
            error("meta tags should be an array of at least one value")
            return None
        else:
            return meta["tags"]


def meta_formats(meta):
    if("formats" not in meta):
        error("meta should have formats !")
        return None
    else:
        if not type(meta["formats"]) is list or len(meta["formats"]) < 1:
            error("meta formats should be an array of at least one value")
            return None
        else:
            return meta["formats"]


def meta_description_shortd(meta):
    if("shortd" not in meta):
        error("meta should have a shord field !")
        return None, None
    else:
        if("description" not in meta):
            return meta["shortd"], meta["shortd"]
        else:
            return meta["description"], meta["shortd"]


def parse_meta(folder):
    info("parsing meta ...")
    pushi()
    if(not os.path.exists(folder + ".json")):
        error(folder+".json not found !")
        return None
    if(not os.path.isfile(folder + ".json")):
        error(folder+"meta.json should be a file ! ")
        return None
    with open(folder + ".json", "r") as meta_json:
        meta = json.load(meta_json)
        status = meta_status(meta)
        name = meta_name(meta, folder)
        tags = meta_tags(meta)
        formats = meta_formats(meta)
        description, shortd = meta_description_shortd(meta)
        if not 'links' in meta:
            error(folder+"meta.json should define link list, even if empty ! ")
            return None
        links = meta["links"]

    if not failed:
        passing("done !")
    popi()
    return Meta(status, name, tags, formats, description, shortd, links)


def parse_content_item(directory, path):
    dpath = os.path.join(path, directory)
    items = os.listdir(dpath)
    items_list = []
    for item in items:
        if os.path.isdir(os.path.join(dpath, item)):
            items_list.append(parse_content_item(
                item, dpath))
        else:
            items_list.append(File(os.path.join(dpath, item), item))
    return Folder(directory, items_list)


def parse_contents(folder, root):
    info("parsing contents ...")
    pushi()
    path = os.path.join(root, folder)
    if(not os.path.isdir(path)):
        error("course should have another folder in for holding contents !")
        return None
    items = os.listdir(path)
    items_list = []
    for item in items:
        if os.path.isdir(os.path.join(path, item)):
            items_list.append(parse_content_item(
                item, path))
        else:
            items_list.append(File(os.path.join(path, item), item))
    if not failed:
        passing("done !")
    popi()
    return FilesTree(items_list)

# FIXME should check that course have LICENSES folder and files inside !


def parse_course(coursename, path):
    info("parsing course " + coursename)
    pushi()
    meta = parse_meta(os.path.join(path, coursename))
    tree = parse_contents(coursename, path)
    # FIXME check the presence of the .tar.gz file if course is available
    # FIXME check that code is effectively a code
    popi()
    return Course(coursename, meta, tree)


def parse_courses(root):
    dirs = os.listdir(root)
    courses = []
    for d in dirs:
        if(os.path.isdir(os.path.join(root, d))):
            courses.append(parse_course(d, root))
    if failed:
        error("course parsing failed !")
    return courses


    # script begins here
if len(sys.argv) < 2:
    info("generator should be invoked with a command as argument")
    info("available commands : check, gen")
    sys.exit(1)

command = sys.argv[1]

mode = ""
source = ""
dest = ""

if command == "check":
    if len(sys.argv) != 3:
        info("check shall be invoked with a secondary source directory argument !")
        sys.exit(1)
    else:
        source = sys.argv[2]
elif command == "gen":
    if len(sys.argv) != 5:
        info("gen shall be invoked with two argument : source dir template dir and destination dir")
        sys.exit(1)
    else:
        source = sys.argv[2]
        templ = sys.argv[3]
        dest = sys.argv[4]
else:
    info("command " + command + " not implemented yet !")
    sys.exit(1)


info("checking courses in " +
     os.path.join(os.getcwd(), source) + "\n")
courses = parse_courses(os.path.join(os.getcwd(), source))

if failed:
    error("some courses are invalid, exiting")
    sys.exit(1)

if command == "check":
    passing("All clear")
    sys.exit(0)

info("writing course js to " + os.path.join(os.getcwd(), dest))

if os.path.exists(os.path.join(os.getcwd(), dest)):
    error("file or dir already exists, aborting !")
    sys.exit(1)

# two sucessive parsing to do things right
# FIXME need to put stuff in a function, it becomes quite hard to read lol !
courses_list_start()
for course in courses:
    if course.meta.status == "available":
        course_element(course)

courses_list_wip()
for course in courses:
    if course.meta.status == "work-in-progress":
        course_element(course)
courses_list_end()

pj = os.path.join

start_index = ""
with open(pj(os.getcwd(), pj(templ, "index_start.html")), "r") as shfile:
    start_index = "".join(shfile.readlines())

end_index = ""
with open(pj(os.getcwd(), pj(templ, "index_end.html")), "r") as ehfile:
    end_index = "".join(ehfile.readlines())

os.mkdir(dest)

with open(pj(os.getcwd(), pj(dest, "index.html")), "w") as target:
    target.write(start_index + doc_value() + end_index)

start_course = ""
with open(pj(os.getcwd(), pj(templ, "course_start.html")), "r") as shfile:
    start_course = "".join(shfile.readlines())

end_course = ""
with open(pj(os.getcwd(), pj(templ, "course_end.html")), "r") as ehfile:
    end_course = "".join(ehfile.readlines())

reset_doc()
for course in courses:
    course_details(course)
    with open(pj(os.getcwd(), pj(dest, course.code+".html")), "w") as target:
        target.write(start_course + doc_value() + end_course)
    reset_doc()
