
webgen : gather_export
	python3 webgen/generator.py gen public webgen/template gen_site
	cp -r webgen/template/css webgen/template/js public
	cp gen_site/* public
	rm -r gen_site

webcheck : gather_export
	python3 webgen/generator.py check public

.PHONY : webgen

mk_public :
	- rm -r public/
	mkdir public/ 

gather_export : b0001 b0423 

clean : c0001 c0423
	- rm -r public

# defining targets for each course
# 0001
b0001 : mk_public
	#building 0423
	$(MAKE) -s -C 0001_introduction_to_linux
	cp -r 0001_introduction_to_linux/export/* public

c0001 : 
	#cleaning 0423
	$(MAKE) -s -C 0001_introduction_to_linux clean > /dev/null



# 0423
b0423 : mk_public
	#building 0423
	$(MAKE) -s -C 0423_software_development_for_scientists
	cp -r 0423_software_development_for_scientists/export/* public

c0423 : 
	#cleaning 0423
	$(MAKE) -s -C 0423_software_development_for_scientists clean > /dev/null
