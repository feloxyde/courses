# Templates, strenghts and limits

## Introduction

## Contents

## Prerequisites

# Advanced templates

## Templated templates

## Template specialization

## Template resotution

## Variadic templates

# Template forces

## Genericity

## Performance

## Static checking

## Clarity

# Template limits

## Testing

## Staticity

## Lisibility