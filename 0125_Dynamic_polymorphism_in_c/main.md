# Dynamic polymorphism in C

## Introduction

## Contents

## Prerequisites

# Why would I use C in 2020 ?

# With in-struct function pointers

## Implementation

## Flexibility

## Performance

# With function tables

## Implementation

## Inheritance

## Performance

# Warnings

## Testing

## Code complexity
