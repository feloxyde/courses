# Clear code : reviews, cleaning, refactoring and documenting

## Introduction 

## Contents

# What is clear code

## Line count

## Complexity

## Expressiveness

## Coherence

## Documentation

## Subjective aspects

# A continuous work

## Reviews

## Refactoring

## Cleaning

## Documenting

## Factorizing

# Addressing subjectivity

## Language choice

## Coding style

## Exceptions