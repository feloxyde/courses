# C++ Course

Below is a test content to see if images render properly.

![test image](resources/noodle_engine.png)

## introduction

## content 

[[_TOC_]]

## prerequisites

# Getting started

## Compilation flow and CMake

## Hello world

## C++ philosophy

# Imperative C++

## Variables and instructions flow

## Control structures

## Functions and procedures

## Pointers and references

## Data collections

# Object Oriented C++

## Classes, structs and scope

## Inheritance

## Polymorphism

# Templates C++

## Static polymorphism

## Template basics

## Type deduction

## Variadic templates

## Template specialization

# Advanced C++ project

## Libraries

## File structure

## Testing & Coverage

## Debugging
