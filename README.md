# courses ressources

Simple repository holding all my courses, talks and other teaching materials, for both reading and correction.

# Structure 

## Design chart

Design is better uniformized, but is not mandatory to follow. 

General colors for documents are ```BA4176``` and ```214F7F```. They shall be used in priority and any other color used shall respect the same tones and saturations etc. 

Diagrams and other graphic elements shall be expressed in SVG or other vector format, ideally using Inkscape. Their main shape has to be Hexagons, and default font 
shall be overpass mono.

Documents shall prevent themselves from including non-free code and non-CC art or alike. Any document, except extremely specific mention, shall be licensed under a free and open 
copyleft license permitting reuse, even for commercial matter, as GPLv3 and CC BY-SA.

It is advised to use templates and resources from (yet to come) ```common``` folder.

## Courses and automated build

At root level, each directory starting with a number, like ```0423_software_development_for_scientists``` is holding in a course.

When adding a new course, it is important to register them in root ```Makefile``` in order for them to be included in automated build. Rules ```bxxxx``` and 
```cxxxx``` are respectively to build and clear course of code ```xxxx```, so software development for scientists course has rules ```b0423``` and ```c0423```. 
These rules need to be added as dependencies to rules ```gather_export``` and ```clean``` respectively. 

Build rule shall depend on ```mk_public```, and at the end of the execution of the rule, following elements shall have been moved into ```public``` directory :

- A directory containing all generated materials of the course, and named after the course, so ```0423_software_development_for_scientists``` corresponding directory shall be called ```0423_software_development_for_scientists```. 
- A json file describing the course, called about name of the course. ```0423_software_development_for_scientists``` course would have ```0423_software_development_for_scientists.json``` file. Note the json shall be at the root of ```public``` directory, not corresponding course directory. Fields of the json are explained in next section.

## Course metadata json format

- ```status``` : either  ```available``` or ```work-in-progress```. Denotes whether the course is ready to be taken or if some major improvements and works are left on it.
- ```name``` : string. Name of the course to be displayed on website.
- ```tags``` : array of strings. Tags of the course, alike keywords, eventually later used to search courses in the list. Tags are displayed in course info and summary. 
- ```formats``` : array of strings. Formats that can be taken by the course, as for example *small course* or *talk*.
- ```shortd``` : string. Short description of the course, displayed in course list. Shall be kept fairly short.
- ```description``` : any string. Long description of the course, displayed in course details. 
- ```links``` : array of objects representing additional links to be displayed in course details :
  -  ```name``` : string. Name of the link to be displayed.
  -  ```link``` : string. URL of the course. Shall be **RELATIVE** to **PUBLIC** directory.


# Setup 

In order to build courses, you are likely to need Latex-full, Drawio and Inkscape, make and Rsync installed.

